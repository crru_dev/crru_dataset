var file = document.getElementById('file');
file.onchange = function(e) {
    var ext = this.value.match(/\.([^\.]+)$/)[1];
    switch (ext) {
        case 'jpg':
        case 'png':
        case 'JPG':
        case 'PNG':
        case 'gif':

            swal("อัพโหลดสำเร็จ", "", "success");
            break;
        default:

            new PNotify({
                title: 'ผิดพลาด',
                text: 'ไฟล์นี้ใช้ไม่ได้ อัพได้เฉพาะไฟล์ที่มีนามสกุล .jpg .png .gif',
                hide: true,
                animation: "fade",
                animate_speed: "slow",
                delay: 8000
            });

            swal("ผิดพลาด", "ไฟล์นี้ใช้ไม่ได้ อัพได้เฉพาะไฟล์ที่มีนามสกุล .jpg .png .gif", "error");

            this.value = '';
    }
};

var file2 = document.getElementById('file2');
file2.onchange = function(e) {
    var ext = this.value.match(/\.([^\.]+)$/)[1];
    switch (ext) {
        case 'jpg':
        case 'png':
        case 'JPG':
        case 'PNG':
        case 'gif':

            swal("อัพโหลดสำเร็จ", "", "success");
            break;
        default:

            new PNotify({
                title: 'ผิดพลาด',
                text: 'ไฟล์นี้ใช้ไม่ได้ อัพได้เฉพาะไฟล์ที่มีนามสกุล .jpg .png .gif',
                hide: true,
                animation: "fade",
                animate_speed: "slow",
                delay: 8000
            });

            swal("ผิดพลาด", "ไฟล์นี้ใช้ไม่ได้ อัพได้เฉพาะไฟล์ที่มีนามสกุล .jpg .png .gif", "error");

            this.value = '';
    }
};



$("#file").fileinput({
    showUpload: false,
    showCaption: false,
    browseClass: "btn btn-primary",
    fileType: "any",
    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
});


$("#file2").fileinput({
    showUpload: false,
    showCaption: false,
    browseClass: "btn btn-primary",
    fileType: "any",
    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
});