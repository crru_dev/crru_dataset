<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'fontend/login';
$route['login'] = 'fontend/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//FONTEND
$route['home'] = 'fontend/home';


//ส่วนที่ 2 ข้อมูลด้านสุขภาพ
$route['fontend/form/form_5'] = 'fontend/health_info/create';
$route['fontend/save/form_5'] = 'fontend/health_info/store';

//ส่วนที่ 4 สิ่งแวดล้อมและธรรมชาติ
$route['fontend/form/form_7'] = 'fontend/env_resource/create';
$route['fontend/save/form_7'] = 'fontend/env_resource/store';

//ส่วนที่ 5 ข้อมูลด้านการเมืองการปกครอง
$route['fontend/form/form_8'] = 'fontend/politica_info/create';
$route['fontend/save/form_8'] = 'fontend/politica_info/store';

//ส่วนที่ 6 ข้อมูลด้านสังคม วัฒนธรรม และการรวมกลุ่ม
$route['fontend/form/form_9'] = 'fontend/social_culture/create';
$route['fontend/save/form_9'] = 'fontend/social_culture/store';
