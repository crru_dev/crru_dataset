<?php
class Form_getdata_model extends CI_Model
{

    public function load_prefix_name()
    {
        $sql = "SELECT id, pre_name_th FROM tb_prefix_name  ORDER BY id ASC ";
        $prefix_query = $this->db->query($sql)->result_array();

        $array_p_name = [];
        foreach ($prefix_query as $key => $p_name) {
            $id = $p_name['id'];
            $array_p_name[$id] = $p_name['pre_name_th'];
        }

        return $array_p_name;
    }
    public function load_nationality()
    {
        $sql = "SELECT nationality_id, nationality_name FROM nationality  ORDER BY nationality_name  ASC";
        $nationality_query = $this->db->query($sql)->result_array();

        $array_nationality = [];
        foreach ($nationality_query as $key => $nationality) {
            $id = $nationality['nationality_id'];
            $array_nationality[$id] = $nationality['nationality_name'];
        }

        return $array_nationality;
    }
    public function load_religion()
    {
        $sql = "SELECT religion_id, religion_name FROM religion  ORDER BY religion_id  ASC";
        $religion_query = $this->db->query($sql)->result_array();

        $array_religion = [];
        foreach ($religion_query as $key => $religion) {
            $id = $religion['religion_id'];
            $array_religion[$id] = $religion['religion_name'];
        }

        return $array_religion;
    }
    public function load_marry_status()
    {
        $sql = "SELECT marry_status_id, marry_status_name FROM tb_marry_status  ORDER BY marry_status_id ";
        $marry_query = $this->db->query($sql)->result_array();

        $array_marry = [];

        foreach ($marry_query as $key => $marry) {
            $id = $marry['marry_status_id'];
            $array_marry[$id] = $marry['marry_status_name'];
        }

        return $array_marry;
    }
    public function load_edu_status()
    {
        $sql = "SELECT edu_status_id, edu_status_name FROM tb_edu_status  ORDER BY edu_status_id ";
        $edu_status_query = $this->db->query($sql)->result_array();

        $array_edu_status = [];

        foreach ($edu_status_query as $key => $edu_status) {
            $id = $edu_status['edu_status_id'];
            $array_edu_status[$id] = $edu_status['edu_status_name'];
        }

        return $array_edu_status;
    }
    public function load_edu_level()
    {
        $sql = "SELECT edu_level_id, edu_level_name FROM tb_edu_level  ORDER BY edu_level_id ";
        $edu_level_query = $this->db->query($sql)->result_array();

        $array_edu_level = [];

        foreach ($edu_level_query as $key => $edu_level) {
            $id = $edu_level['edu_level_id'];
            $array_edu_level[$id] = $edu_level['edu_level_name'];
        }

        return $array_edu_level;
    }

    public function load_cal_level()
    {
        $sql = "SELECT cal_level_id, cal_level_name FROM tb_cal_level  ORDER BY cal_level_id ";
        $cal_level_query = $this->db->query($sql)->result_array();

        $array_cal_level = [];

        foreach ($cal_level_query as $key => $cal_level) {
            $id = $cal_level['cal_level_id'];
            $array_cal_level[$id] = $cal_level['cal_level_name'];
        }

        return $array_cal_level;
    }

    public function load_writing_level()
    {
        $sql = "SELECT writing_level_id, writing_level_name FROM tb_writing_level  ORDER BY writing_level_id ";
        $writing_level_query = $this->db->query($sql)->result_array();

        $array_writing_level = [];

        foreach ($writing_level_query as $key => $writing_level) {
            $id = $writing_level['writing_level_id'];
            $array_writing_level[$id] = $writing_level['writing_level_name'];
        }

        return $array_writing_level;
    }

    public function load_reading_level()
    {
        $sql = "SELECT reading_level_id, reading_level_name FROM tb_reading_level  ORDER BY reading_level_id ";
        $reading_level_query = $this->db->query($sql)->result_array();

        $array_reading_level = [];

        foreach ($reading_level_query as $key => $reading_level) {
            $id = $reading_level['reading_level_id'];
            $array_reading_level[$id] = $reading_level['reading_level_name'];
        }

        return $array_reading_level;
    }
    public function load_relatin_head()
    {
        $sql = "SELECT relatin_head_id, relatin_head_name FROM tb_relatin_head  ORDER BY relatin_head_id ";
        $relatin_head_query = $this->db->query($sql)->result_array();

        $array_relatin_head = [];

        foreach ($relatin_head_query as $key => $relatin_head) {
            $id = $relatin_head['relatin_head_id'];
            $array_relatin_head[$id] = $relatin_head['relatin_head_name'];
        }

        return $array_relatin_head;
    }
    public function load_living_status()
    {
        $sql = "SELECT living_status_id, living_status_name FROM tb_living_status  ORDER BY living_status_id ";
        $living_status_query = $this->db->query($sql)->result_array();

        $array_living_status = [];

        foreach ($living_status_query as $key => $living_status) {
            $id = $living_status['living_status_id'];
            $array_living_status[$id] = $living_status['living_status_name'];
        }

        return $array_living_status;
    }

    public function load_skill()
    {
        $sql = "SELECT skill_id, skill_name FROM tb_skill  ORDER BY skill_id ";
        $skill_query = $this->db->query($sql)->result_array();
        return $skill_query;
    }

    // public function load_local_lang()
    // {
    //     $sql = "SELECT local_lang_id, local_lang_name FROM tb_local_lang  ORDER BY local_lang_id ";
    //     $local_lang_query = $this->db->query($sql)->result_array();
    //     return $local_lang_query;
    // }
    public function load_second_lang()
    {
        $sql = "SELECT second_lang_id, second_lang_name FROM tb_second_lang  ORDER BY second_lang_id ";
        $second_lang_query = $this->db->query($sql)->result_array();
        return $second_lang_query;
    }

    public function load_occup()
    {
        $sql = "SELECT occup_id, occup_name FROM tb_occup  ORDER BY occup_id ";
        $occup_query = $this->db->query($sql)->result_array();
        $array_occup = [];

        foreach ($occup_query as $key => $occup) {
            $id = $occup['occup_id'];
            $array_occup[$id] = $occup['occup_name'];
        }

        return $array_occup;
    }

    public function load_occup_sub()
    {
        $sql = "SELECT occup_sub_id, occup_sub_name FROM tb_occup_sub  ORDER BY occup_sub_id ";
        $occup_sub_query = $this->db->query($sql)->result_array();
        return $occup_sub_query;
    }

    public function load_area_occup()
    {
        $sql = "SELECT id, name FROM tb_occup_area  ORDER BY id ";
        $area_occup_query = $this->db->query($sql)->result_array();
        return $area_occup_query;
    }

    public function load_individual_risk_behavior()
    {
        $sql = "SELECT individual_risk_behavior_id, individual_risk_behavior_name FROM tb_individual_risk_behavior  ORDER BY individual_risk_behavior_id ";
        $individual_risk_behavior_query = $this->db->query($sql)->result_array();
        return $individual_risk_behavior_query;
    }

    public function load_income_type()
    {
        $sql = "SELECT id, name FROM tb_income_type  ORDER BY id ";
        $income_type_query = $this->db->query($sql)->result_array();
        return $income_type_query;
    }

    public function load_cost_type()
    {
        $sql = "SELECT id, name FROM tb_cost_type  ORDER BY id ";
        $cost_type_query = $this->db->query($sql)->result_array();
        return $cost_type_query;
    }
    public function load_tb_economic_problems()
    {
        $sql = "SELECT id, name FROM tb_economic_problems  ORDER BY id ";
        $data_query = $this->db->query($sql)->result_array();
        return $data_query;
    }

    
    public function load_group_type()
    {
        $sql = "SELECT id, name FROM tb_group_type  ORDER BY id ";
        $query = $this->db->query($sql)->result_array();
        $array = [];
        foreach ($query as $key => $data) {
            $id = $data['id'];
            $array[$id] = $data['name'];
        }
        return $array;
    }

    public function load_group_format()
    {
        $sql = "SELECT id, name FROM tb_group_format  ORDER BY id ";
        $query = $this->db->query($sql)->result_array();
        $array = [];
        foreach ($query as $key => $data) {
            $id = $data['id'];
            $array[$id] = $data['name'];
        }
        return $array;
    }

}