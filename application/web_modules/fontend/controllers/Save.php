<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Save extends CI_Controller
{

    public $fontend = 'fontend/';

    public function __construct()
    {
        parent::__construct();

        $user_id = $this->session->userdata('user_id');
        if ($user_id == "") {
            $this->session->sess_destroy();
            redirect('fontend/login/index');
            exit();

        }

    }
    public function index()
    {
        redirect('fontend/home');
    }

    // public function check_house_code()
    // { //รหัสบ้าน
    //     //เช็คข้อมูล ว่ามีรหัสประจำบ้าน นี้หรือยัง
    //     $house_code = $_POST['house_code'];
    //     $query = $this->db->query("SELECT house_code  FROM  tb_house_registration  WHERE  house_code  = '$house_code'");
    //     $num_rows = $query->num_rows();
    //     if ($num_rows > 0) {
    //         echo $num_rows;
    //     } else {
    //         echo $num_rows;
    //     }
    // }






    public function form_1()
    {
        //กรณีไม่มีรหัสประจำบ้าน ให้ผู้บันทึกเช็คจากข้อมูลสามชิกในครัวเรือนเอง
        // $community_id = $this->get_community_id();
        //เพิ่มไว้เผื่อหลุดเช็คจากหน้าแรก 
        $province_id = $this->input->post('province_id');
        $amphur_id = $this->input->post('amphur_id');
        $district_id = $this->input->post('district_id');
        $moo =  $this->input->post('moo');
        $village_name = $this->input->post('village_name');        
        $house_number = $this->input->post('house_number');        
      
            if($province_id=="" AND $amphur_id =="" AND $district_id  == ""  AND $moo =="" AND $house_number ="" AND  $village_name ="" ){
                header("location: " . base_url('fontend/form/form_1'));
                exit();
                }



        //ออโต้ community_id รหัสแบบสอบถาม
        $sql = "SELECT MAX(house_registration_id) as house_registration_id FROM tb_house_registration ";
        $q = $this->db->query($sql);
        $community_id = "1"; // รหัสแบบสอบถาม
        $house_registration_id = "1"; // รหัสลงทะเบียนข้อมูลบ้าน
        if ($q->num_rows() > 0) {

            $row = $q->row_array();
            if ($row['house_registration_id'] == "") {
                $community_id = "1";
            } else {
                $community_id = $row['house_registration_id'] + 1;
                $house_registration_id = $row['house_registration_id'] + 1;

            }
        }

        $house_code = $this->input->post('house_code');
       
        if(empty($house_code)){

            $sql = "SELECT MAX(house_registration_id)  as house_registration_id FROM tb_house_registration  WHERE house_code LIKE '%NO%'";
            $row = $this->db->query($sql)->row();
            //$house_code = "NO_".( $row->house_registration_id +1 );
           
        }

        $community_id = sprintf('%05d', $community_id);
        $family_member = $this->input->post('family_member');

        $data = array(
            'created_at' => date("Y-m-d H:i:s"),
            'created_by' => $this->session->userdata('user_id'), //user login
            'community_id' => $community_id,
            'province_id' => $this->input->post('province_id'),
            'amphur_id' => $this->input->post('amphur_id'),
            'district_id' => $this->input->post('district_id'),
            'moo' => $this->input->post('moo'),
            'village_name' => $this->input->post('village_name'),
            'community_name' => $this->input->post('community_name'),
            'house_number' => $this->input->post('house_number'),
            'house_code' => $house_code,
            'house_number_nearby' => $this->input->post('house_number_nearby'),
            'family_member' => $family_member,
            'Latitude' => $this->input->post('Latitude'),
            'Longitude' => $this->input->post('Longitude'),
            //'pic' => $this->input->post('pic')

        );
        if ($_FILES["pic"]['name'] != "") {
            if (isset($_FILES["pic"])) {

                $output_dir1 = FCPATH."asset/pic/";
                $ret = array();
                $error = $_FILES["pic"]["error"];
                $extention_file = pathinfo($_FILES["pic"]["name"], PATHINFO_EXTENSION);
                $result_explode = explode('.', trim($extention_file));
                $fileName = $community_id . '.' . strtolower($result_explode[0]);

                if (!is_array($_FILES["pic"]["name"])) { //single file

                    move_uploaded_file($_FILES["pic"]["tmp_name"], $output_dir1 . $fileName);
                    $ret[] = $fileName;
                    $data['pic'] = $fileName;
                }
            }
        }

        if ($_FILES["pic2"]['name'] != "") {
            if (isset($_FILES["pic2"])) {

                $output_dir1 = FCPATH."asset/pic2/";
                $ret = array();
                $error = $_FILES["pic2"]["error"];
                $extention_file = pathinfo($_FILES["pic2"]["name"], PATHINFO_EXTENSION);
                $result_explode = explode('.', trim($extention_file));
                $fileName = $community_id . '.' . strtolower($result_explode[0]);

                if (!is_array($_FILES["pic2"]["name"])) { //single file

                    move_uploaded_file($_FILES["pic2"]["tmp_name"], $output_dir1 . $fileName);
                    $ret[] = $fileName;
                    $data['pic2'] = $fileName;
                }
            }
        }

        //  $this->session->set_userdata('family_member', $data['family_member']); //จำนวนคนในครัวเรือน
        $this->session->set_userdata('house_registration_id', $house_registration_id); //รหัสลงทะเบียนข้อมูลบ้าน
        $this->session->set_userdata('community_id', $community_id); //รหัสแบบสอบถาม
        $this->db->insert('tb_house_registration', $data); //บันทึกข้อมูลลงทะเบียนบ้าน

        //***************************************************บันทึกข้อมูลจำนวนสมาชิกในครอบครัว tb_house_member เพื่อฟอร์ม 3 ชึ้นไปจะได้เขียนแค่ update  */

        for ($x = 1; $x <= $family_member; $x++) {

            $data_member = "";
            $data_member = array(
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => $this->session->userdata('user_id'), //user login
                'house_registration_id' => $house_registration_id,
                'house_member' => $x);

            $this->db->insert('tb_house_member', $data_member); //ตารางบันทึกข้อมูลสามาชิกในบ้าน

        }

        header("location: " . base_url('fontend/form/form_2'));
        

    }

    public function delete_house_registration(){

        $house_registration_id = $this->input->post('house_registration_id', true);

        $data = [
            'house_registration_id' => $house_registration_id
         ];

        $this->db->delete('tb_house_member', $data );
        $this->db->delete('tb_house_information', $data );
        $this->db->delete('tb_house_registration', $data );

         
        echo json_encode(array(
            'status' => true,
            'message' => '',
        ));
    }


    public function form_1_edit()
    {
        $community_id= $this->input->post('community_id');
        $house_code = $this->input->post('house_code');

        $house_registration_id= $this->session->userdata('house_registration_id'); //รหัสลงทะเบียนข้อมูลบ้าน
        $community_id= $this->input->post('community_id');       


            $data = array(
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->session->userdata('user_id'), //user login
                'community_id' => $community_id,
                'province_id' => $this->input->post('province_id'),
                'amphur_id' => $this->input->post('amphur_id'),
                'district_id' => $this->input->post('district_id'),
                'moo' => $this->input->post('moo'),
                'village_name' => $this->input->post('village_name'),
                'community_name' => $this->input->post('community_name'),
                'house_number' => $this->input->post('house_number'),
                'house_code' => $this->input->post('house_code'),
                'house_number_nearby' => $this->input->post('house_number_nearby'),
                'Latitude' => $this->input->post('Latitude'),
                'Longitude' => $this->input->post('Longitude'),

            );

        

        if ($_FILES["pic"]['name'] != "") {
            if (isset($_FILES["pic"])) {

                $output_dir1 = FCPATH . "asset/pic/";
                $ret = array();
                $error = $_FILES["pic"]["error"];
                $extention_file = pathinfo($_FILES["pic"]["name"], PATHINFO_EXTENSION);
                $result_explode = explode('.', trim($extention_file));
                $fileName = $community_id . '.' . strtolower($result_explode[0]);

                if (!is_array($_FILES["pic"]["name"])) { //single file

                    move_uploaded_file($_FILES["pic"]["tmp_name"], $output_dir1 . $fileName);
                    $ret[] = $fileName;
                    $data['pic'] = $fileName;
                }
            }
        }

        if ($_FILES["pic2"]['name'] != "") {
            if (isset($_FILES["pic2"])) {

                $output_dir1 = FCPATH . "asset/pic2/";
                $ret = array();
                $error = $_FILES["pic2"]["error"];
                $extention_file = pathinfo($_FILES["pic2"]["name"], PATHINFO_EXTENSION);
                $result_explode = explode('.', trim($extention_file));
                $fileName = $community_id . '.' . strtolower($result_explode[0]);

                if (!is_array($_FILES["pic2"]["name"])) { //single file

                    move_uploaded_file($_FILES["pic2"]["tmp_name"], $output_dir1 . $fileName);
                    $ret[] = $fileName;
                }
            }
        }
        $house_registration_id =   $this->session->userdata('house_registration_id'); //รหัสลงทะเบียนข้อมูลบ้าน
        $this->session->set_userdata('community_id', $community_id); //รหัสแบบสอบถาม

        $this->db->update('tb_house_registration', $data, array('house_registration_id' => $house_registration_id));
        //print_r($this->db->last_query());


       redirect('fontend/form/form_3');

    }

    public function delete_member($id=""){
        if($id == ""):
            redirect('fontend/home');
            exit();
        endif;

           $is_success = $this->db->delete('tb_house_member', array('house_member_id' => $id));
            $msg = ($is_success) ? 'ลบผู้ใช้งานเรียบร้อย' : 'ผิดพลาดไม่สามารถลบผู้ใช้งานได้';
           //นับจำนวนสมาชิกที่เหลือกแล้ว update tb_house_registration family_member
           $house_registration_id = $this->session->userdata('house_registration_id');
           $sql = "SELECT * FROM   tb_house_member WHERE house_registration_id  = '$house_registration_id' ";
           $q = $this->db->query($sql);
          $count_member =  $q->num_rows();


          $family_member = array(
              'family_member' => $count_member);
          $this->db->update('tb_house_registration', $family_member, array('house_registration_id' => $house_registration_id));
           // echo $count_member;

            echo json_encode(array(
                'status' => true,
                'message' => $msg,
            ));

    }
    public function add_member($id=""){
        if($id == ""):
            redirect('fontend/home');
            exit();
        endif;
        $data_member = array(
            'created_at' => date("Y-m-d H:i:s"),
            'created_by' => $this->session->userdata('user_id'), //user login
            'house_registration_id' => $id,
            'house_member' => "99");
            $is_success= $this->db->insert('tb_house_member', $data_member); //ตารางบันทึกข้อมูลสามาชิกในบ้าน
           // $msg = ($is_success) ? 'เพิ่มสมาชิกแล้ว' : 'ผิดพลาดไม่สามารถเพิ่มสมาชิกได้';
           //นับจำนวนสมาชิกที่เหลือกแล้ว update tb_house_registration family_member
        $sql = "SELECT * FROM   tb_house_member WHERE house_registration_id  = '$id' ";
         $q = $this->db->query($sql);
        $count_member =  $q->num_rows();


        $family_member = array(
            'family_member' => $count_member);
        $this->db->update('tb_house_registration', $family_member, array('house_registration_id' => $id));

        redirect('fontend/form/form_3/');


    }
    public function form_1_delete($id = "")
    {
        if($id == ""):
            redirect('fontend/home');
            exit();
        endif;

      //  $this->session->set_userdata('house_registration_id', $id); //รหัสลงทะเบียนข้อมูลบ้าน
        redirect('fontend/form/form_1');
      //echo $id;
         //tb_house_registration
        //tb_house_member
        //tb_house_information
        //answer_area_occup
        //answer_caretaker
        //answer_chronic_illness
        //answer_chronic_illness_problems
        //answer_emergency_assistance
        //answer_environment
        //answer_family_knowledge
        //answer_guidelines_for_care
        //answer_individual_risk_behavior
        //answer_member_second_lang
        //answer_member_skill
        //answer_monthly_activity
        //answer_natural_resources
        //answer_need_help
        //answer_occup_sub
        //answer_politica_information
        //answer_welfare
        //answer_work_risk_behavior
      //  exit();
        exit();


    }
    public function form_2()
    {

        $house_information_recorded = $this->input->post('house_information_recorded'); //ครั้งที่บันทึก
        $house_registration_id = $this->session->userdata('house_registration_id');
        $house_code = $this->input->post('house_code');
        $query = $this->db->query("SELECT house_information_id  FROM  tb_house_information  WHERE    house_registration_id = '$house_registration_id'");
        $num_rows = $query->num_rows();
        //บ้านหนึ่งหลังจะบันทึกข้อมูลได้ 1 ครั้ง แก้ใข 1 ก พ  2564

        if ($num_rows > 0) {
            // echo "มีข้อมูลแล้ว";
            // update  tb_house_information

            $data = array(
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->session->userdata('user_id'), //user login
                'house_information_recorded' => $house_information_recorded,
                'house_information_record_date' => $this->input->post('house_information_record_date'),
                'house_information_recorder' => $this->input->post('house_information_recorder'),
                'house_information_recorder_tel' => $this->input->post('house_information_recorder_tel'),
                'recorder_name' => $this->input->post('recorder_name'),

            );

            $this->db->update('tb_house_information', $data, array('house_registration_id' => $house_registration_id));

            header("location: " . base_url('fontend/form/form_3'));
        } else {
            $data = array(
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => $this->session->userdata('user_id'), //user login
                'house_registration_id' => $house_registration_id,
                'house_information_recorded' => $house_information_recorded,
                'house_information_record_date' => $this->input->post('house_information_record_date'),
                'house_information_recorder' => $this->input->post('house_information_recorder'),
                'house_information_recorder_tel' => $this->input->post('house_information_recorder_tel'),
                'recorder_name' => $this->input->post('recorder_name'),


            );

            $this->session->set_userdata('house_information_recorded', $data['house_information_recorded']); //ครั้งที่บันทึก
            $this->db->insert('tb_house_information', $data);
            redirect('fontend/form/form_3');
        }
    }

    public function form_3()
    {
        //******************แก้ใขการบันทึกข้อมูลใหม่ 2 กพ 2564 */
        //เพิ่มบันทึกตาราง answer_member_skill ความสามารถพิเศสของสมาชิกในครัวเรื่อน
        //เพิ่มบันทึกตาราง answer_member_second_lang ภาษาที่ 2 ของสมาชิกในครัวเรื่อน
        //ลบข้อมูลก่อนทุกครั้ง
        $house_registration_id = $this->session->userdata('house_registration_id'); //รหัสลงทะเบียนข้อมูลบ้าน
        $tables = array('answer_member_skill', 'answer_member_second_lang');
        $this->db->where('house_registration_id', $house_registration_id);
        $this->db->delete($tables);

        ///update tb_house_member
        $post_list = $this->input->post(null, true);
        $updated_at = date("Y-m-d H:m:s");

        $count_post = count($this->input->post('house_member_id'));

        // exit();
        for ($x = 0; $x < $count_post; $x++) {

            $data = array(
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => $this->session->userdata('user_id'), //user login
                'prefix_id' => $this->input->post('prefix_id')[$x],
                'f_name' => $this->input->post('f_name')[$x],
                'l_name' => $this->input->post('l_name')[$x],
                'aeg' => $this->input->post('aeg')[$x],
                'height' => $this->input->post('height')[$x],
                'weight' => $this->input->post('weight')[$x],
                'nationality_id' => $this->input->post('nationality_id')[$x],
                'birthday' => $this->input->post('birthday')[$x], ///ดึงข้อมูลมาแสดงในหน้า form ยังไม่ได้
                'gender' => $this->input->post('gender')[$x],

                'religion_id' => $this->input->post('religion_id')[$x],
                'marry_status_id' => $this->input->post('marry_status_id')[$x],
                //'edu_status_id' => $this->input->post('edu_status_id')[$x],
                'edu_level_id' => $this->input->post('edu_level_id')[$x],
                'reading_level_id' => $this->input->post('reading_level_id')[$x],
                'writing_level_id' => $this->input->post('writing_level_id')[$x],
                'cal_level_id' => $this->input->post('cal_level_id')[$x],
                // 'relatin_head_id' => $this->input->post('relatin_head_id')[$x],
                // 'living_status_id' => $this->input->post('living_status_id')[$x],

            );
            $house_member_id = $this->input->post('house_member_id')[$x];
            $this->db->update('tb_house_member', $data, array('house_member_id' => $house_member_id));
        }

        foreach ($post_list as $key_topic => $topic) {

            if ($key_topic == "skill_id") {
                foreach ($topic as $member_id => $answer) {
                    foreach ($answer as $skill_id) {
                        $array = array(
                            'updated_at' => $updated_at,
                            'created_by' => $this->session->userdata('user_id'),
                            'house_registration_id' => $house_registration_id,
                            'member_id' => $member_id,
                            'skill_id' => $skill_id,
                        );

                        $this->db->set($array);
                        $this->db->insert('answer_member_skill');
                    }

                }
            }

            if ($key_topic == "second_lang_id") {
                foreach ($topic as $member_id => $answer) {
                    foreach ($answer as $second_lang_id) {
                        $array = array(
                            'updated_at' => $updated_at,
                            'created_by' => $this->session->userdata('user_id'),
                            'house_registration_id' => $house_registration_id,
                            'member_id' => $member_id,
                            'second_lang_id' => $second_lang_id,
                        );
                        $this->db->set($array);
                        $this->db->insert('answer_member_second_lang');
                    }
                }
            }
        }
        redirect('fontend/form/form_4');
    }



    public function form_4()
    {

       $house_registration_id = $this->session->userdata('house_registration_id'); //รหัสลงทะเบียนข้อมูลบ้าน

        $count_post = count($this->input->post('house_member_id')); //รอบอับเดทข้อมูล
        $occup_sub_id = "";
        $area_occup_id = "";
        $updated_at = date("Y-m-d H:m:s");

        for ($x = 0; $x < $count_post; $x++) {

            $data = array(
                'created_by' => $this->session->userdata('user_id'), //user login
                'occup_id' => $this->input->post('occup_id')[$x],
                'occup_detail' => $this->input->post('occup_detail')[$x],

            );

            $house_member_id = $this->input->post('house_member_id')[$x];

            $this->db->update('tb_house_member', $data, array('house_member_id' => $house_member_id));
        }

        $this->db->delete('answer_area_occup', array('house_registration_id' => $house_registration_id));
        $this->db->delete('answer_occup_sub', array('house_registration_id' => $house_registration_id));

        foreach ($_POST['house_member_id'] as $key => $value) {

            if(!empty($_POST['area_occup_id'][$value])){

                $area_occup_id_array = $_POST['area_occup_id'][$value];

                foreach ($area_occup_id_array ?? [] as $key_2 => $value2) {

                    $data = array(
                        'updated_at' => $updated_at,
                        'created_by' => $this->session->userdata('user_id'),
                        'house_registration_id' => $house_registration_id,
                        'house_member_id' => $value,
                        'area_occup_id' => $value2,
                    );

                    $this->db->insert('answer_area_occup', $data);
                }
            }

        }

        foreach ($_POST['house_member_id'] as $key => $value) {

            if(!empty($_POST['occup_sub_id'][$value])){
                $occup_sub_id_array = $_POST['occup_sub_id'][$value];
                foreach ($occup_sub_id_array ?? [] as $key_2 => $value2) {

                    $data = array(
                        'updated_at' => $updated_at,
                        'created_by' => $this->session->userdata('user_id'),
                        'house_registration_id' => $house_registration_id,
                        'house_member_id' => $value,
                        'occup_sub_id' => $value2,
                    );

                    $this->db->insert('answer_occup_sub', $data);
                }
            }
        }

        redirect('fontend/form/form_5');
    }

    public function get_province()
    { //อำเภอ
        if (isset($_POST['function']) && $_POST['function'] == 'provinces') {
            $id = $_POST['id'];
            $sql = "SELECT * FROM amphures WHERE province_id='$id'";
            $query = $this->db->query($sql)->result_array();
            echo '<option value="" selected disabled>-กรุณาเลือกอำเภอ-</option>';
            foreach ($query as $value) {
                echo '<option value="' . $value['id'] . '">' . $value['name_th'] . '</option>';

            }
        }

        if (isset($_POST['function']) && $_POST['function'] == 'amphures') {
            $id = $_POST['id'];
            $sql = "SELECT * FROM districts WHERE amphure_id='$id'";
            $query = $this->db->query($sql)->result_array();
            echo '<option value="" selected disabled>-กรุณาเลือกตำบล-</option>';
            foreach ($query as $value2) {
                echo '<option value="' . $value2['id'] . '">' . $value2['name_th'] . '</option>';

            }
        }

    }

    public function form_6()
    {
        $post = $this->input->post(null, true);
        $post_list = $post;
      
        $house_registration_id = $this->session->userdata('house_registration_id'); //รหัสลงทะเบียนข้อมูลบ้าน
        $house_information_recorded = $this->session->userdata('house_information_recorded'); //รหัสลงทะเบียนข้อมูลบ้าน
        $this->db->delete('answer_cost', array('house_registration_id' => $house_registration_id));
        $this->db->delete('answer_income', array('house_registration_id' => $house_registration_id));
        $updated_at = date("Y-m-d H:m:s");

        $sess = $this->session->userdata();

        $table = 'answer_economic_problems';

        $economic_problems = $post['economic_problems'];

        $cond = [
                'house_registration_id' => $house_registration_id
        ];

        $num_rows = $this->db->get_where($table, $cond)->num_rows();

        if( $num_rows > 0) {//กรณี Update คำตอบ
            $this->db->delete($table, $cond);
        }
       
        //INSERT POLITICA INFOMATION ANWSER
        $table = 'answer_economic_problems';
        $economic_problems_name = $economic_problems['economic_problems_name'];
        
        if(isset($economic_problems['answer'])) {

            foreach ($economic_problems['answer'] as $key => $answer) {
            
                $data_insert = [
                    'created_by'=> $sess['user_id'],
                    'house_registration_id'=> $house_registration_id,
                    'economic_problems_id' => $answer,
                    'economic_problems_name' => $economic_problems_name[$answer] ?? "" 
                ];

                $status = $this->db->insert($table, $data_insert);
            
            }//end foreach
        }
        $data_update = [
                        'monthly_income' => $this->input->post('monthly_income'),
                        'hourse_debt'=>$this->input->post('hourse_debt'),
                        'hourse_saveing'=>$this->input->post('hourse_saveing'),
                    ];

        $update= $this->db->update('tb_house_information', $data_update, ['house_registration_id' => $house_registration_id]); 
        //update รายได้เฉลี่ยนต่อเดือน

        foreach ($post_list as $key_topic => $topic) {
         
        
            if ($key_topic == "income") {
                foreach ($topic AS $value2) {
                    $data_answer_income = array(
                        'updated_at' => $updated_at,
                        'created_by' => $this->session->userdata('user_id'),
                        'house_registration_id' => $house_registration_id,
                    );
                    $data_answer_income = $data_answer_income+$value2;
                    $this->db->insert('answer_income', $data_answer_income);
                }
            }

            if ($key_topic == "cost") {
                foreach ($topic AS $value2) {
                    $data_answer_cost = array(
                        'updated_at' => $updated_at,
                        'created_by' => $this->session->userdata('user_id'),
                        'house_registration_id' => $house_registration_id,
                    );
                    $data_answer_cost = $data_answer_cost+$value2;
                    $this->db->insert('answer_cost', $data_answer_cost);
                }
            }



        }
        redirect('fontend/form/form_7');

    }
//การรวมกลุ่ม/องค์กรในชุมชนที่ท่านมีส่วนร่วมหรือเป็นสมาชิก
    public function form_10()
    {
        $data = $this->input->post(null, true);
        $status = $this->db->insert('tb_community_integration', $data); //บันทึกข้อมูลลงทะเบียนบ้าน
        redirect('fontend/form/form_10');

    }
    public function form_covid()
    {

        $post = $this->input->post(null, true);
        $sess = $this->session->userdata();
        $table = 'answer_covid_defense';

        // echo "<pre>";
        // print_r($post);
        // exit();
        $house_registration_id = $sess['house_registration_id'];
        // $house_registration_id = 8;

        $covid_defense = $post['covid_defense'];

        $cond = [
                'house_registration_id' => $house_registration_id
            ];
        $num_rows = $this->db->get_where($table, $cond)->num_rows();
        if( $num_rows > 0) {//กรณี Update คำตอบ
            $this->db->delete($table, $cond);
        }

        //INSERT POLITICA INFOMATION ANWSER
        $table = 'answer_covid_defense';
        $covid_defense_name = $covid_defense['covid_defense_name'];
        if(isset($covid_defense['answer'])) {
            foreach ($covid_defense['answer'] as $key => $answer) {
                $data_insert = [
                    'created_by'=> $sess['user_id'],
                    'house_registration_id'=> $house_registration_id,
                    'covid_defense_id' => $answer,
                    'covid_defense_name'=>$covid_defense_name[$answer]
                ];
                $status = $this->db->insert($table, $data_insert);
            }//end foreach
        }
       
       redirect('fontend/form/form_10');

    }

    public function delete_form_10($id)
    {
        $is_success = $this->db->delete('tb_community_integration', array('id' => $id));
        $msg = ($is_success) ? 'ลบผู้ใช้งานเรียบร้อย' : 'ผิดพลาดไม่สามารถลบผู้ใช้งานได้';

        echo json_encode(array(
            'status' => true,
            'message' => $msg,
        ));
    }

} //END CLASS