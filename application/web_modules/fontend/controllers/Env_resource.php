<?php
//-------------[Controller File name : Env_resource.php ]---------------------//
//---------------------[ Create by: @SEK At 28-01-2021 ]----------------------//
// ----------------------NOTE: แบบสอบถามส่วนที่ 4--------------------------------//
defined('BASEPATH') OR exit('No direct script access allowed');

class Env_resource extends CI_Controller {

    public $fontend = 'fontend/';
	public function index()
	{
		// $this->load->view($this->fontend.$this->fontend.'home/index');
        $this->load->view($this->fontend.'theme/header');
        $this->load->view($this->fontend.'home/index');
        $this->load->view($this->fontend.'theme/footer');
	}

    // CREATE create() BY: @SEK At 28-01-2021
    // IDEA: -
    // NOTE: -
    public function create()
    {
        $house_registration_id = $this->session->userdata('house_registration_id');
        $information_recorded= $this->session->userdata('house_information_recorded');
        // $house_registration_id = 35;
        // $information_recorded = 2;
        //@PLUGIN & @APPJS
		$data['plugin'] = array(
            'asset/theme/eliteadmin/assets/node_modules/icheck/skins/all.css',
            'asset/theme/eliteadmin/assets/node_modules/icheck/icheck.min.js',
        );
		$data['appjs'] = array(
            'appjs/home/form_7.js'
        );
        $table = 'tb_environment';
        $data['q_environment'] = $this->db->get($table)->result();

        $table = 'tb_natural_resources';
        $data['q_resource'] = $this->db->get($table)->result();

        $sql = "SELECT h.*, p.province_name, d.name_th AS district_name
                FROM tb_house_registration h
                LEFT JOIN province p
                    ON h.province_id = p.province_id
                LEFT JOIN districts d
                    ON h.district_id = d.id
                WHERE h.house_registration_id = {$house_registration_id}
                ";
        $data['house_registration'] = $this->db->query($sql)->row();


        
        //LOAD VIEW
        $this->load->view($this->fontend.'theme/header', $data);
        $this->load->view($this->fontend.'home/form_7');
        $this->load->view($this->fontend.'theme/footer');
    }

    // CREATE store() BY: @SEK At 28-01-2021
    // IDEA: -
    // NOTE: -
    public function store()
    {
        $post = $this->input->post(null, true);
        $sess = $this->session->userdata();
        $house_registration_id = $sess['house_registration_id'];
        // $house_registration_id = 35;
        // echo "<pre>";
        // print_r($post);
        // exit();

        //INSER ANSWER ENVIRONMENT
        $update_status = false;
        $cond = ['house_registration_id'=> $house_registration_id];
        $num_rows = $this->db->get_where('answer_environment', $cond)->num_rows();
        if($num_rows > 0){//กรณีมีการอัพเดท
            $update_status = true;
            $this->db->delete('answer_environment', $cond);
        }

        $env_answer = $post['environment'];
        $updated_at = ($update_status) ? date('Y-m-d h:s:i') : null;
        $updated_by = ($update_status) ? $sess['user_id']: null;
        foreach ($env_answer  as $key => $env) {
            $data_insert = [
                'created_by'=>$sess['user_id'],
                'updated_at'=>$updated_at,
                'updated_by'=>$updated_by ,
                'house_registration_id ' => $house_registration_id,
                'environment_id' => $env['environment_id'],
                'environment_name' =>$env['text'],
                'answer' =>$env['answer'],
            ];
            $this->db->insert('answer_environment', $data_insert);
        }



        //INSERT NATURAL RESOURCE
        $update_status = false;
        $cond = ['house_registration_id'=> $house_registration_id];
        $num_rows = $this->db->get_where('answer_natural_resources', $cond)->num_rows();
        if($num_rows > 0){//กรณีมีการอัพเดท
            $update_status = true;
            $this->db->delete('answer_natural_resources', $cond);
        }

        $natural_resources = $post['natural_resources'];
        $updated_at = ($update_status) ? date('Y-m-d h:s:i') : null;
        $updated_by = ($update_status) ? $sess['user_id']: null;
        foreach ($natural_resources as $key => $nature) {
            $data_insert = [
                'created_by'=>$sess['user_id'],
                'updated_at'=>$updated_at,
                'updated_by'=>$updated_by ,
                'house_registration_id ' => $house_registration_id,
                'natural_resources_id' => $nature['natural_resources_id'],
                'natural_resources_name' => $nature['note'],
                'answer' =>$nature['answer'],
            ];
            $this->db->insert('answer_natural_resources', $data_insert);
        }

        redirect('fontend/form/form_8');

    }


}//END CLASS
