<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public $fontend = 'fontend/';
	public function index()
	{
		// $this->load->view($this->fontend.$this->fontend.'home/index');
        $this->load->view($this->fontend.'theme/header');
        $this->load->view($this->fontend.'home/index');
        $this->load->view($this->fontend.'theme/footer');
	}
}//END CLASS
