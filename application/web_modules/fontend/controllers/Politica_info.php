<?php
//-------------[Controller File name : Politica_info.php ]---------------------//
//---------------------[ Create by: @SEK At 02-02-2021 ]----------------------//
// ----------------------NOTE: แบบสอบถามส่วนที่ 5--------------------------------//
defined('BASEPATH') OR exit('No direct script access allowed');

class Politica_info extends CI_Controller {

    public $fontend = 'fontend/';
	public function index()
	{
		// $this->load->view($this->fontend.$this->fontend.'home/index');
        $this->load->view($this->fontend.'theme/header');
        $this->load->view($this->fontend.'home/index');
        $this->load->view($this->fontend.'theme/footer');
	}

    // CREATE create() BY: @SEK At 28-01-2021
    // IDEA: -
    // NOTE: -
    public function create()
    {
        $sess = $this->session->userdata();
        $house_registration_id = $sess['house_registration_id'];
        // $information_recorded= $sess['house_information_recorded'];
        // $house_registration_id = 8;
        // $information_recorded = 2;

        //@PLUGIN & @APPJS
		$data['plugin'] = array(
            'asset/theme/eliteadmin/assets/node_modules/icheck/skins/all.css',
            'asset/theme/eliteadmin/assets/node_modules/icheck/icheck.min.js',
        );
		$data['appjs'] = array(
            'appjs/home/form_8.js'
        );
        $table = 'tb_politica_information';
        $data['q_politica_info'] = $this->db->get($table)->result();

        $table = 'answer_politica_information';
        $cond = ['house_registration_id' => $house_registration_id];
        $this->db->select('politica_infomation_id');
        $data['answer_list'] = $this->db->get_where($table, $cond)->result();


        //LOAD VIEW
        $this->load->view($this->fontend.'theme/header', $data);
        $this->load->view($this->fontend.'home/form_8');
        $this->load->view($this->fontend.'theme/footer');
    }

    // CREATE store() BY: @SEK At 28-01-2021
    // IDEA: -
    // NOTE: -
    public function store()
    {
        $post = $this->input->post(null, true);
        $sess = $this->session->userdata();
        $table = 'answer_politica_information';

        // echo "<pre>";
        // print_r($post);
        // exit();
        $house_registration_id = $sess['house_registration_id'];
        // $house_registration_id = 8;

        $politica_infomation = $post['politica_infomation'];

        $cond = [
                'house_registration_id' => $house_registration_id
            ];
        $num_rows = $this->db->get_where($table, $cond)->num_rows();
        if( $num_rows > 0) {//กรณี Update คำตอบ
            $this->db->delete($table, $cond);
        }

        //INSERT POLITICA INFOMATION ANWSER
        $table = 'answer_politica_information';
        $politica_infomation_name = $politica_infomation['politica_infomation_name'];
        if(isset($politica_infomation['answer'])) {
            foreach ($politica_infomation['answer'] as $key => $answer) {
                $data_insert = [
                    'created_by'=> $sess['user_id'],
                    'house_registration_id'=> $house_registration_id,
                    'politica_infomation_id' => $answer,
                    'politica_infomation_name'=>$politica_infomation_name[$answer]
                ];
                $status = $this->db->insert($table, $data_insert);
            }//end foreach
        }
        

        redirect('fontend/form/form_9');

    }


}//END CLASS
