<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public $fontend = 'fontend/';
	
	public function __construct(){

		parent::__construct();
		$this->load->helper('Function');
  }//end __construct
  

	public function index()
	{
		$response = [];

		$response['title'] = "ข้อมูลผู้ใช้";

		$user_id = $this->session->userdata('user_id');

		$sql 	= "SELECT * FROM tb_login WHERE delete_at IS NULL AND id <> $user_id  ";
		$response['users'] = $this->db->query($sql)->result();

		$data['plugin'] = array(
			'asset/node_modules/select2/dist/css/select2.min.css',
			'asset/plugins/bootstrap-fileinput-master/css/fileinput.css',
			'asset/plugins/bootstrap-fileinput-master/js/fileinput.js'
		);

		$data['appjs'] = array(
			'asset/node_modules/select2/dist/js/select2.full.min.js',
			'appjs/select2.js',
			'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js',
			'asset/theme/eliteadmin/assets/node_modules/datatables.net/js/jquery.dataTables.min.js',
			'asset/theme/eliteadmin/assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js"'
		);

		//LOAD VIEW
		$this->load->view($this->fontend.'theme/header', $data);
		$this->load->view('fontend/users/index.php', $response);
		$this->load->view($this->fontend.'theme/footer');	
	}

	public function profile()
	{

		$response = [];
		$response['title'] = 'ข้อมูลส่วนตัว';
		
		$user_id = $this->session->userdata('user_id');

		$sql = "SELECT * FROM tb_login WHERE id = '$user_id' ";
		$response['user'] = $this->db->query($sql)->row();

		if($this->input->post()){

			$data = array(
				'name'=>$this->input->post('name'),
				'surname'=>$this->input->post('surname'),
			);

			$this->db->update('tb_login', $data, array('id'=>$user_id));

			redirect('fontend/users/profile', $response);
		}

		$data['plugin'] = array(
			'asset/node_modules/select2/dist/css/select2.min.css',
			'asset/plugins/bootstrap-fileinput-master/css/fileinput.css',
			'asset/plugins/bootstrap-fileinput-master/js/fileinput.js'
		);

		$data['appjs'] = array(
			'asset/node_modules/select2/dist/js/select2.full.min.js',
			'appjs/select2.js',
			'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js'
		);

		//LOAD VIEW
		$this->load->view($this->fontend . 'theme/header', $data);
		$this->load->view('fontend/users/profile.php', $response);
		$this->load->view($this->fontend . 'theme/footer');	
}

	public function create()
	{
		$response = [];
		$response['title'] = 'ข้อมูลผู้ใช้';
	
		$data['plugin'] = array(
			'asset/node_modules/select2/dist/css/select2.min.css',
			'asset/plugins/bootstrap-fileinput-master/css/fileinput.css',
			'asset/plugins/bootstrap-fileinput-master/js/fileinput.js',
			'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.min.css'
		);

		$data['appjs'] = array(
			'asset/node_modules/select2/dist/js/select2.full.min.js',
			'appjs/select2.js',
			'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js'
		);

		//LOAD VIEW
		$this->load->view($this->fontend . 'theme/header', $data);
		$this->load->view('fontend/users/create.php', $response);
		$this->load->view($this->fontend . 'theme/footer');	
	}

	public function save_user()
	{
		$response = [];
	
		if($this->input->post()){
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'กรอก username', 'required');
			$this->form_validation->set_rules('password', 'กรอก รหัสผ่านใหม่', 'required');
			$this->form_validation->set_rules('name', 'กรอก ชื่อ', 'required');
			$this->form_validation->set_rules('surname', 'กรอก นามสกุล', 'required');
			$this->form_validation->set_rules('role', 'ประเภทผู้ใช้', 'required');
		
			if($this->form_validation->run())
			{
				$username = $this->input->post('username');
				$sql = "SELECT * FROM tb_login WHERE username = '$username'  AND delete_at IS NULL";
				$user = $this->db->query($sql)->row();
				if($user){

					$response['status'] = false;
					$response['massage'] = 'Username นี้มีในระบบแล้ว';
				}else{

					$data = array(
						'username' => $this->input->post('username'),
						'password' => md5($this->input->post('password')),
						'name' => $this->input->post('name'),
						'surname' => $this->input->post('surname'),
						'role' => $this->input->post('role'),
					);

					$this->db->insert('tb_login', $data);
					$response['status'] = true;
				}

			}else{

				$response['massage'] = validation_errors();
			}

			echo json_encode($response);
		}
	}

	

	public function update_password()
	{

		$user_id = $this->session->userdata('user_id');

		$sql = "SELECT * FROM tb_login WHERE id = '$user_id' ";
		$login = $this->db->query($sql)->row();

		$this->load->library('form_validation');
		if($this->input->post()){

			$this->form_validation->set_rules('password', 'กรอก รหัสปัจจุบันด้วย', 'required|alpha_numeric');
			$this->form_validation->set_rules('newpass', 'กรอก รหัสผ่านใหม่', 'required|alpha_numeric');
			$this->form_validation->set_rules('confpassword', 'กรอก ยืนยันรหัสผ่าน', 'required|alpha_numeric');
			
			if($this->form_validation->run())
			{
				$cur_password 	= $this->input->post('password');
				$new_password 	= $this->input->post('newpass');
				$conf_password 	= $this->input->post('confpassword');
				
				try
				{
					if ($login->password != md5($cur_password))   throw new Exception('ขออภัย! รหัสผ่านปัจจุบันไม่ตรงกัน');
					if ($new_password != $conf_password)    throw new Exception('รหัสผ่านใหม่และยืนยันรหัสผ่านไม่ตรงกัน');
					
					$data = array(
						'password' => md5($this->input->post('newpass'))
					);
		
					$this->db->update('tb_login', $data, array('id'=>$user_id));
		
					$response['status'] = true;

				}
				catch (Exception $e)
				{
					$response['status'] = false;
					$response['massage'] = $e->getMessage();	
				}
			}
			else
			{
				$response['status'] 	= false;
				$response['massage'] = validation_errors();
			}
		}

		echo json_encode($response);
	}

	public function change_password()
	{
		$response = [];
		$response['title'] = 'เปลี่ยนรหัสผ่าน';
		$user_id = $this->session->userdata('user_id');

		$sql = "SELECT * FROM tb_login WHERE id = '$user_id' ";
		$response['user'] = $this->db->query($sql)->row();


		$data['plugin'] = array(
			'asset/node_modules/select2/dist/css/select2.min.css',
			'asset/plugins/bootstrap-fileinput-master/css/fileinput.css',
			'asset/plugins/bootstrap-fileinput-master/js/fileinput.js',
			'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.min.css'
		);

		$data['appjs'] = array(
			'asset/node_modules/select2/dist/js/select2.full.min.js',
			'appjs/select2.js',
			'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js'
		);

		//LOAD VIEW
		$this->load->view($this->fontend . 'theme/header', $data);
		$this->load->view('fontend/users/change_password.php', $response);
		$this->load->view($this->fontend . 'theme/footer');	

	}
	
	public function edit($id)
	{
		$response = [];
		$response['title'] = 'แก้ไข ข้อมูลผู้ใช้';
	
		$sql = "SELECT * FROM tb_login WHERE id = '$id' ";
		$response['user'] = $this->db->query($sql)->row();

		$data['plugin'] = array(
			'asset/node_modules/select2/dist/css/select2.min.css',
			'asset/plugins/bootstrap-fileinput-master/css/fileinput.css',
			'asset/plugins/bootstrap-fileinput-master/js/fileinput.js',
			'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.min.css'
		);

		$data['appjs'] = array(
			'asset/node_modules/select2/dist/js/select2.full.min.js',
			'appjs/select2.js',
			'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js'
		);

		//LOAD VIEW
		$this->load->view($this->fontend . 'theme/header', $data);
		$this->load->view('fontend/users/edit.php', $response);
		$this->load->view($this->fontend . 'theme/footer');	
	}

	public function update()
	{
		$response = [];
	
		if($this->input->post()){

			$id = $this->input->post('id');
			$sql = "SELECT * FROM tb_login WHERE id = '$id'  AND delete_at IS NULL";
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'กรอก username', 'required');
			$this->form_validation->set_rules('name', 'กรอก ชื่อ', 'required');
			$this->form_validation->set_rules('surname', 'กรอก นามสกุล', 'required');
			$this->form_validation->set_rules('role', 'ประเภทผู้ใช้', 'required');
		
			if($this->form_validation->run())
			{
					$data = array(
						'name' => $this->input->post('name'),
						'surname' => $this->input->post('surname'),
						'role' => $this->input->post('role'),
					);

					( $data['password'] = md5($this->input->post('password')) ?? "");

					$this->db->update('tb_login', $data, array('id' => $id));
					
					$response['status'] = true;

			}else{

				$response['massage'] = validation_errors();
			}

			echo json_encode($response);
		}
	}

	public function delete($id){

		$is_success = $this->db->update('tb_login', array( 'delete_at' => date("Y-m-d H:i:s")  ),  array( 'id' => $id ));
	
		$msg = ($is_success)?'ลบผู้ใช้งานเรียบร้อย':'ผิดพลาดไม่สามารถลบผู้ใช้งานได้';

		echo json_encode(array(
			'status'=> true,
			'message'=>$msg
		));
	}


}//End Class
