<?php
//-------------[Controller File name : Social_culture.php ]---------------------//
//---------------------[ Create by: @SEK At 02-02-2021 ]----------------------//
// ----------------------NOTE: แบบสอบถามส่วนที่ 6--------------------------------//
defined('BASEPATH') OR exit('No direct script access allowed');

class Social_culture extends CI_Controller {

    public $fontend = 'fontend/';
	public function index()
	{
		// $this->load->view($this->fontend.$this->fontend.'home/index');
        $this->load->view($this->fontend.'theme/header');
        $this->load->view($this->fontend.'home/index');
        $this->load->view($this->fontend.'theme/footer');
	}

    // CREATE create() BY: @SEK At 2-02-2021
    // IDEA: -
    // NOTE: -
    public function create()
    {
        //@PLUGIN & @APPJS
		$data['plugin'] = array(
            'asset/theme/eliteadmin/assets/node_modules/icheck/skins/all.css',
            'asset/theme/eliteadmin/assets/node_modules/icheck/icheck.min.js',
        );
		$data['appjs'] = array(
            'appjs/home/form_9.js'
        );
        $table = 'tb_month';
        $data['month_list'] = $this->db->get($table)->result();

        //LOAD VIEW
        $this->load->view($this->fontend.'theme/header', $data);
        $this->load->view($this->fontend.'home/form_9');
        $this->load->view($this->fontend.'theme/footer');
    }

    // CREATE store() BY: @SEK At 2-02-2021
    // IDEA: -
    // NOTE: -
    public function store()
    {
        $post = $this->input->post(null, true);
        $sess = $this->session->userdata();
        $table = 'answer_monthly_activity';

        $house_registration_id = $sess['house_registration_id'];
        // $house_registration_id = 8;
        // $information_recorded = 2;

        $activity = $post['activity'];

        $cond = [
                'house_registration_id' => $house_registration_id
            ];
        $num_rows = $this->db->get_where($table, $cond)->num_rows();
        if( $num_rows > 0) {//กรณี Update คำตอบ
            $this->db->delete($table, $cond);
        }

        //INSERT SOCIAL CILTURE ANWSER
        foreach ($activity as $key => $answer) {
            $data_insert = [
                'created_by' => $sess['user_id'],
                'house_registration_id' => $house_registration_id,
                'month_id' => $answer['month_id'],
                'month_name' =>$answer['month_name'],
                'activity' => $answer['activity']
            ];
            $status = $this->db->insert($table, $data_insert);
        }//end foreach

        //INSERT KNOWLEDGE FAMILY
        $table = 'answer_family_knowledge';
        $cond = [
                'house_registration_id' => $house_registration_id
            ];
        $num_rows = $this->db->get_where($table, $cond)->num_rows();
        if( $num_rows > 0) {//กรณี Update คำตอบ
            $this->db->delete($table, $cond);
        }

        $knowledge = $post['knowledge'];
        $knowledge_text = $post['knowledge_text'];
        $data_insert = [
            'created_by' => $sess['user_id'],
            'house_registration_id' => $house_registration_id,
            'choice' =>$knowledge,
            'answer' => $knowledge_text
        ];
        $status = $this->db->insert($table, $data_insert);

        if ($status) {
            redirect('fontend/form/form_covid');
        } else {
            redirect('fontend/form/form_9');

        }

    }


}//END CLASS
