<?php
//-------------[Controller File name : Health_info.php ]---------------------//
//---------------------[ Create by: @SEK At 02-02-2021 ]----------------------//
// ----------------------NOTE: แบบสอบถามส่วนที่ 2 --------------------------------//
// ----------------------NOTE: DELETE field tb_work_risk_behavior, tb_emergency_assistance --------------------------------//
defined('BASEPATH') OR exit('No direct script access allowed');

class Health_info extends CI_Controller {

    public $fontend = 'fontend/';
	public function index()
	{
		// $this->load->view($this->fontend.$this->fontend.'home/index');
        $this->load->view($this->fontend.'theme/header');
        $this->load->view($this->fontend.'home/index');
        $this->load->view($this->fontend.'theme/footer');
	}

    // CREATE create() BY: @SEK At 02-02-2021
    // IDEA: -
    // NOTE: -
    public function create()
    {
        $sess = $this->session->userdata();
        $house_registration_id = $sess['house_registration_id'];
        // $house_registration_id = 8;
        //Get tb_house_registration
        $cond = ['house_registration_id' => $house_registration_id];
        $data['house_info'] = $this->db->get_where('tb_house_registration', $cond)->row();

        //Get tb_house_member
        $data['house_member_list'] = $this->db->get_where('tb_house_member', $cond)->result();

        //@PLUGIN & @APPJS
        $data['plugin'] = array(
            'asset/plugins/sweetalert/sweetalert.min.js',
            'asset/theme/eliteadmin/assets/node_modules/icheck/skins/all.css',
            'asset/theme/eliteadmin/assets/node_modules/icheck/icheck.min.js',
        );
        $data['appjs'] = array(
            'appjs/home/form_5.js',

        );
        //2.1
        $table = 'tb_individual_risk_behavior';
        $data['irb_list'] = $this->db->get($table)->result();


        //2.2
        $table = 'tb_work_risk_behavior';
        $data['wrb_list'] = $this->db->get($table)->result();

        $table = 'answer_work_risk_behavior';
        $cond = ['house_registration_id' => $house_registration_id];
        $this->db->select('work_risk_behavior_id');
        $data['answer_wrb_list'] = $this->db->get_where($table, $cond)->result();


        //2.3
        // $table = 'tb_need_help';
        // $data['nh_list'] = $this->db->get($table)->result();

        // $table = 'answer_need_help';
        // $cond = ['house_registration_id' => $house_registration_id];
        // $this->db->select('need_help_id');
        // $data['answer_nh_list'] = $this->db->get_where($table, $cond)->result();


        //2.4
        // $table = 'tb_emergency_assistance';
        // $data['amas_list'] = $this->db->get($table)->result();

        // $table = 'answer_emergency_assistance';
        // $cond = ['house_registration_id' => $house_registration_id];
        // $this->db->select('emergency_assistance_id');
        // $data['answer_emergency_assistance'] = $this->db->get_where($table, $cond)->result();


        //2.5
        //ข้อมูลการเจ็บป่วยเรื้อรัง
        $table = 'tb_chronic_illness';
        $data['chronic_illness_list'] = $this->db->get($table)->result();
        //Answer list
        $table = 'answer_chronic_illness';
        $cond = ['house_registration_id' => $house_registration_id];
        $this->db->select('chronic_illness_id');
        $data['answer_chronic_illness'] = $this->db->get_where($table, $cond)->result();

        //ข้อมูลผู้ให้การดูแล
        $table = 'tb_caretaker';
        $data['caretaker_list'] = $this->db->get($table)->result();
        //Answer list
        $table = 'answer_caretaker';
        $cond = ['house_registration_id' => $house_registration_id];
        $this->db->select('caretaker_id');
        $data['answer_caretaker'] = $this->db->get_where($table, $cond)->result();

        //ข้อมูลแนวทางการดูแลรักษาเมื่อมีภาวะเจ็บป่วย
        $table = 'tb_guidelines_for_care';
        $data['guidelines_for_care_list'] = $this->db->get($table)->result();
        //Answer list
        $table = 'answer_guidelines_for_care';
        $cond = ['house_registration_id' => $house_registration_id];
        $this->db->select('guidelines_for_care_id');
        $data['answer_guidelines_for_care'] = $this->db->get_where($table, $cond)->result();

        //ข้อมูลสิทธิและสวัสดิการในการรักษาพยาบาล
        $table = 'tb_welfare_type';
        $data['welfare_type_list'] = $this->db->get($table)->result();
        //Answer list
        $table = 'answer_welfare';
        $cond = ['house_registration_id' => $house_registration_id];
        $this->db->select('welfare_type_id');
        $data['answer_welfare'] = $this->db->get_where($table, $cond)->result();


        //LOAD VIEW
        $this->load->view($this->fontend . 'theme/header', $data);
        $this->load->view($this->fontend . 'home/form_5');
        $this->load->view($this->fontend . 'theme/footer');
    }

    // CREATE store() BY: @SEK At 02-02-2021
    // IDEA: -
    // NOTE: -
    public function store()
    {
        $post = $this->input->post(null, true);
        $sess = $this->session->userdata();
        $house_registration_id = $sess['house_registration_id'];
        // $house_registration_id = 8;

        //INSER answer_individual_risk_behavior
        $update_status = false;
        $cond = ['house_registration_id'=> $house_registration_id];
        $num_rows = $this->db->get_where('answer_individual_risk_behavior', $cond)->num_rows();
        if($num_rows > 0){//กรณีมีการอัพเดท
            $update_status = true;
            $this->db->delete('answer_individual_risk_behavior', $cond);
        }
        $individual_risk_behavior = $post['individual_risk_behavior'];
        $individual_risk_behavior_list = $individual_risk_behavior['individual_risk_behavior_list'];
        $table = 'answer_individual_risk_behavior';
        $updated_at = ($update_status) ? date('Y-m-d h:s:i') : null;
        $updated_by = ($update_status) ? $sess['user_id']: null;
        
        if(!empty($individual_risk_behavior['member'])){

            foreach ($individual_risk_behavior['member'] ?? "" as $key_member=> $member) {
                foreach ($member as $key => $answer_list) {
                    foreach ($answer_list as $key => $answer) {
                        $data_insert = [
                            'updated_at'=>$updated_at,
                            'created_by'=>$sess['user_id'],
                            'updated_by'=>$updated_by,
                            'house_registration_id'=>$house_registration_id,
                            'house_member_id'=>$key_member,
                            'individual_risk_behavior_name'=>$individual_risk_behavior_list[$answer],
                            'individual_risk_behavior_id'=>$answer,
                        ];
                        $this->db->insert($table, $data_insert);

                    }//END FOREACH 03

                }//END FOREACH 02
            } //END FOREACH 01
        }

      

        //INSER answer_work_risk_behavior
        $update_status = false;
        $cond = ['house_registration_id'=> $house_registration_id];
        $num_rows = $this->db->get_where('answer_work_risk_behavior', $cond)->num_rows();
        if($num_rows > 0){//กรณีมีการอัพเดท
            $update_status = true;
            $this->db->delete('answer_work_risk_behavior', $cond);
        }
        $work_risk_behavior = $post['work_risk_behavior'];
        $work_risk_behavior_list = $work_risk_behavior['work_risk_behavior_list'];
        $table = 'answer_work_risk_behavior';
        $updated_at = ($update_status) ? date('Y-m-d h:s:i') : null;
        $updated_by = ($update_status) ? $sess['user_id']: null;

        if(!empty($work_risk_behavior['member'])){
            foreach ($work_risk_behavior['member'] as $key_member=> $member) {
                foreach ($member as $key => $answer_list) {
                    foreach ($answer_list as $key => $answer) {
                        $data_insert = [
                            'updated_at'=>$updated_at,
                            'created_by'=>$sess['user_id'],
                            'updated_by'=>$updated_by,
                            'house_registration_id'=>$house_registration_id,
                            'house_member_id'=>$key_member,
                            'work_risk_behavior_name'=>$work_risk_behavior_list[$answer],
                            'work_risk_behavior_id'=>$answer,
                        ];
                        $this->db->insert($table, $data_insert);
                    }

                }//END FOREACH 02
            } //END FOREACH 01
        }


        //INSER answer_need_help
        // $update_status = false;
        // $cond = ['house_registration_id'=> $house_registration_id];
        // $num_rows = $this->db->get_where('answer_need_help', $cond)->num_rows();
        // if($num_rows > 0){//กรณีมีการอัพเดท
        //     $update_status = true;
        //     $this->db->delete('answer_need_help', $cond);
        // }
        // $need_help = $post['need_help'];
        // $need_help_list = $need_help['need_help_list'];
        // $table = 'answer_need_help';
        // $updated_at = ($update_status) ? date('Y-m-d h:s:i') : null;
        // $updated_by = ($update_status) ? $sess['user_id']: null;
        // foreach ($need_help['member'] as $key_member=> $member) {
        //     foreach ($member as $key => $answer_list) {
        //         foreach ($answer_list as $key => $answer) {
        //             $data_insert = [
        //                 'updated_at'=>$updated_at,
        //                 'created_by'=>$sess['user_id'],
        //                 'updated_by'=>$updated_by,
        //                 'house_registration_id'=>$house_registration_id,
        //                 'house_member_id'=>$key_member,
        //                 'need_help_name'=>$need_help_list[$answer],
        //                 'need_help_id'=>$answer,
        //             ];
        //             $this->db->insert($table, $data_insert);
        //         }

        //     }//END FOREACH 02
        // } //END FOREACH 01


        //INSER answer_emergency_assistance
        // $update_status = false;
        // $cond = ['house_registration_id'=> $house_registration_id];
        // $num_rows = $this->db->get_where('answer_emergency_assistance', $cond)->num_rows();
        // if($num_rows > 0){//กรณีมีการอัพเดท
        //     $update_status = true;
        //     $this->db->delete('answer_emergency_assistance', $cond);
        // }
        // $emergency_assistance = $post['emergency_assistance'];
        // $emergency_assistance_list = $emergency_assistance['emergency_assistance_list'];
        // $table = 'answer_emergency_assistance';
        // $updated_at = ($update_status) ? date('Y-m-d h:s:i') : null;
        // $updated_by = ($update_status) ? $sess['user_id']: null;
        // foreach ($emergency_assistance['member'] as $key_member=> $member) {
        //     foreach ($member as $key => $answer_list) {
        //         foreach ($answer_list as $key => $answer) {
        //             $data_insert = [
        //                 'updated_at'=>$updated_at,
        //                 'created_by'=>$sess['user_id'],
        //                 'updated_by'=>$updated_by,
        //                 'house_registration_id'=>$house_registration_id,
        //                 'house_member_id'=>$key_member,
        //                 'emergency_assistance_name'=>$emergency_assistance_list[$answer],
        //                 'emergency_assistance_id'=>$answer,
        //             ];
        //             $this->db->insert($table, $data_insert);
        //         }

        //     }//END FOREACH 02
        // } //END FOREACH 01


        //INSER answer_chronic_illness
        $update_status = false;
        $cond = ['house_registration_id'=> $house_registration_id];
        $num_rows = $this->db->get_where('answer_chronic_illness', $cond)->num_rows();
        if($num_rows > 0){//กรณีมีการอัพเดท
            $update_status = true;
            $this->db->delete('answer_chronic_illness', $cond);
        }
        $chronic_illness = $post['chronic_illness'];
        $chronic_illness_list = $chronic_illness['chronic_illness_list'];
        $table = 'answer_chronic_illness';
        $updated_at = ($update_status) ? date('Y-m-d h:s:i') : null;
        $updated_by = ($update_status) ? $sess['user_id']: null;

        if(!empty($chronic_illness['member'])){
            foreach ($chronic_illness['member'] as $key_member=> $member) {
                foreach ($member as $key => $answer_list) {
                    foreach ($answer_list as $key => $answer) {
                        $data_insert = [
                            'updated_at'=>$updated_at,
                            'created_by'=>$sess['user_id'],
                            'updated_by'=>$updated_by,
                            'house_registration_id'=>$house_registration_id,
                            // 'house_member_id'=>$key_member,
                            'chronic_illness_name'=>$chronic_illness_list[$answer],
                            'chronic_illness_id'=>$answer,
                        ];
                        $this->db->insert($table, $data_insert);
                    }

                }//END FOREACH 02
            } //END FOREACH 01
        }


        //INSER answer_caretaker
        $update_status = false;
        $cond = ['house_registration_id'=> $house_registration_id];
        $num_rows = $this->db->get_where('answer_caretaker', $cond)->num_rows();
        if($num_rows > 0){//กรณีมีการอัพเดท
            $update_status = true;
            $this->db->delete('answer_caretaker', $cond);
        }
        $caretaker = $post['caretaker'];
        $caretaker_list = $caretaker['caretaker_list'];
        $table = 'answer_caretaker';
        $updated_at = ($update_status) ? date('Y-m-d h:s:i') : null;
        $updated_by = ($update_status) ? $sess['user_id']: null;
        if(!empty($caretaker['member'])){
            foreach ($caretaker['member'] as $key_member=> $member) {
                foreach ($member as $key => $answer_list) {
                    foreach ($answer_list as $key => $answer) {
                        $data_insert = [
                            'updated_at'=>$updated_at,
                            'created_by'=>$sess['user_id'],
                            'updated_by'=>$updated_by,
                            'house_registration_id'=>$house_registration_id,
                            // 'house_member_id'=>$key_member,
                            'caretaker_name'=>$caretaker_list[$answer],
                            'caretaker_id'=>$answer,
                        ];
                        $this->db->insert($table, $data_insert);
                    }

                }//END FOREACH 02
            } //END FOREACH 01
        }

        //INSER answer_guidelines_for_care
        $update_status = false;
        $cond = ['house_registration_id'=> $house_registration_id];
        $num_rows = $this->db->get_where('answer_guidelines_for_care', $cond)->num_rows();
        if($num_rows > 0){//กรณีมีการอัพเดท
            $update_status = true;
            $this->db->delete('answer_guidelines_for_care', $cond);
        }
        $guidelines_for_care = $post['guidelines_for_care'];
        $guidelines_for_care_list = $guidelines_for_care['guidelines_for_care_list'];
        $table = 'answer_guidelines_for_care';
        $updated_at = ($update_status) ? date('Y-m-d h:s:i') : null;
        $updated_by = ($update_status) ? $sess['user_id']: null;

        if(!empty($guidelines_for_care['member'])){
            foreach ($guidelines_for_care['member'] as $key_member=> $member) {
                foreach ($member as $key => $answer_list) {
                    foreach ($answer_list as $key => $answer) {
                        $data_insert = [
                            'updated_at'=>$updated_at,
                            'created_by'=>$sess['user_id'],
                            'updated_by'=>$updated_by,
                            'house_registration_id'=>$house_registration_id,
                        // 'house_member_id'=>$key_member,
                            'guidelines_for_care_name'=>$guidelines_for_care_list[$answer],
                            'guidelines_for_care_id'=>$answer,
                        ];
                        $this->db->insert($table, $data_insert);
                    }

                }//END FOREACH 02
            } //END FOREACH 01
        }


        //INSER answer_chronic_illness_problems
        // $update_status = false;
        // $cond = ['house_registration_id'=> $house_registration_id];
        // $num_rows = $this->db->get_where('answer_chronic_illness_problems', $cond)->num_rows();
        // if($num_rows > 0){//กรณีมีการอัพเดท
        //     $update_status = true;
        //     $this->db->delete('answer_chronic_illness_problems', $cond);
        // }
        // $chronic_illness_problems = $post['chronic_illness_problems'];
        // $table = 'answer_chronic_illness_problems';
        // $updated_at = ($update_status) ? date('Y-m-d h:s:i') : null;
        // $updated_by = ($update_status) ? $sess['user_id']: null;
        // foreach ($chronic_illness_problems as $key_member=> $answer) {
        //     $data_insert = [
        //         'updated_at'=>$updated_at,
        //         'created_by'=>$sess['user_id'],
        //         'updated_by'=>$updated_by,
        //         'house_registration_id'=>$house_registration_id,
        //         'house_member_id'=>$key_member,
        //         'answer'=>$answer,
        //     ];
        //     $this->db->insert($table, $data_insert);
        // } //END FOREACH 01


        //INSER answer_welfare
        $update_status = false;
        $cond = ['house_registration_id'=> $house_registration_id];
        $num_rows = $this->db->get_where('answer_welfare', $cond)->num_rows();
        if($num_rows > 0){//กรณีมีการอัพเดท
            $update_status = true;
            $this->db->delete('answer_welfare', $cond);
        }


        $welfare = $post['welfare_type'];
        $welfare_type_list = $welfare['welfare_type_list'];
        $table = 'answer_welfare';
      
        
        $updated_at = ($update_status) ? date('Y-m-d h:s:i') : null;
        $updated_by = ($update_status) ? $sess['user_id']: null;

        if(!empty($welfare['member'])){

            foreach ($welfare['member'] as $key_member=> $member) {
                foreach ($member as $key => $answer_list) {
                    foreach ($answer_list as $key => $answer) {
                        $data_insert = [
                            'updated_at'=>$updated_at,
                            'created_by'=>$sess['user_id'],
                            'updated_by'=>$updated_by,
                            'house_registration_id'=>$house_registration_id,
                        // 'house_member_id'=>$key_member,
                            'welfare_type_name'=>$welfare_type_list[$answer],
                            
                            'welfare_type_id'=>$answer,
                            'answer'=>$post['welfare_detail'],
                        ];
                        
                        $this->db->insert($table, $data_insert);
                    }

                }//END FOREACH 02
            } //END FOREACH 01
        }

        redirect('fontend/form/form_6');

    }


}//END CLASS
