<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Login extends CI_Controller
{

	public $fontend = 'fontend/';
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$response = [];
		$response['msg_error'] = "";

		$data['plugin'] = array(
			'asset/node_modules/select2/dist/css/select2.min.css',
			'asset/plugins/bootstrap-fileinput-master/css/fileinput.css',
			'asset/plugins/bootstrap-fileinput-master/js/fileinput.js'
		);

		$data['appjs'] = array(
			'asset/node_modules/select2/dist/js/select2.full.min.js',
			'appjs/select2.js',
			'asset/plugins/sweetalert/sweetalert.min.js'
		);

		//LOAD VIEW
		$this->load->view($this->fontend . 'theme/header_login', $data);
		$this->load->view($this->fontend . 'login/index.php', $response);
		$this->load->view($this->fontend . 'theme/footer');
	}



	public function page_login_failed()
	{
		$response = [];
		$response['msg_error'] = "";

		$data['plugin'] = array(
			'asset/node_modules/select2/dist/css/select2.min.css',
			'asset/plugins/bootstrap-fileinput-master/css/fileinput.css',
			'asset/plugins/bootstrap-fileinput-master/js/fileinput.js'
		);

		$data['appjs'] = array(
			'asset/node_modules/select2/dist/js/select2.full.min.js',
			'appjs/select2.js',
			'asset/plugins/sweetalert/sweetalert.min.js'
		);

		//LOAD VIEW
		$this->load->view($this->fontend . 'theme/header_login', $data);
		$this->load->view('fontend/login/index.php', array('msg_error' => 'username หรือ password ไม่ถูกต้อง'));
		$this->load->view($this->fontend . 'theme/footer');
	}

	public function signin()
	{

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$sql = "SELECT *
          FROM
              tb_login
          WHERE
              username  = " . $this->db->escape(trim($username));

		$q = $this->db->query($sql);
		$row = $q->row_array();

		if ($q->num_rows() > 0) {
			$row = $q->row_array();

			if (trim($row['password']) === md5(trim($password))) {

				$sdata = array(
					'role'		 => $row['role'],
					'fullname' 	=> $row['name'].' '.$row['surname'],
					'user_id' 			=> $row['id'],
					'breadcrumb' => ['หน้าหลัก']
				);

				$this->session->set_userdata($sdata);

				redirect('fontend/home');
			} else {
				redirect('fontend/login/page_login_failed');
			}
		} else {
			redirect('fontend/login/page_login_failed');
		}
	} //end function


	public function logout()
	{
		$this->session->sess_destroy();
		redirect('fontend/login/index');
	}
}
