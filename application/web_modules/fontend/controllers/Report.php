
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public $fontend = 'fontend/';
	
	public function __construct(){

		parent::__construct();
		$user_id = $this->session->userdata('user_id');
        if ($user_id == "") {
            $this->session->sess_destroy();
            redirect('fontend/login/index');
            exit();

        }

        $this->load->model('Province_model', 'Province_model');
        $this->load->model('Form_getdata_model', 'Form_getdata_model');
		
  }//end __construct
  



	public function house_information()
	{
		$data['plugin'] = array(
			'asset/node_modules/select2/dist/css/select2.min.css',
			'asset/node_modules/Chart.js/Chart.min.js',	
		);
		$data['appjs'] = array(
			'asset/node_modules/select2/dist/js/select2.full.min.js',			
			'appjs/select2.js',				
		);
				//โหลด Edit Form 1
			$form_data['provices'] = $this->Province_model->load_province();
			
			$sql = "SELECT
						tb_house_member.gender,
						Count(*) AS count_gender
					FROM
						tb_house_member
					INNER JOIN tb_house_registration ON tb_house_member.house_registration_id = tb_house_registration.house_registration_id
					GROUP BY gender "; 
					$query = $this->db->query($sql)->result_array();

						$array_name = [];
						$array_count= [];
						$array_rgbColor=[];
						foreach ($query as $key => $query_data) :
							switch ($query_data['gender']) :
								case 1:
									$array_name[]= "ชาย";
								break;
								case 2:
									$array_name[]= "หญิง";
								break;       
								default:
									$array_name[]= "ไม่ได้กรอกข้อมูล";
							endswitch;							
							$array_count[]= $query_data['count_gender'];
							$array_rgbColor[] ="rgb(".mt_rand(0, 255).", ".mt_rand(0, 255).", ".mt_rand(0, 255).")";											
						endforeach;

						$form_data['gender_JSON']=json_encode($array_name, \JSON_UNESCAPED_UNICODE);
						$form_data['count_gender_JSON']=json_encode($array_count);
						$form_data['rgb_gender_JSON']=json_encode($array_rgbColor);
						
			
		    	$sql = "SELECT nationality_id,Count(*)AS count_nationality
					FROM
						tb_house_member
					INNER JOIN tb_house_registration ON tb_house_member.house_registration_id = tb_house_registration.house_registration_id
					GROUP BY nationality_id "; 

				$query = $this->db->query($sql)->result_array();
				$array_name = [];
				$array_count= [];
				$array_rgbColor=[];
				foreach ($query as $key => $query_data) :
					$array_name[]= $query_data['nationality_id'];	
					$array_count[]= $query_data['count_nationality'];	
					$array_rgbColor[] ="rgb(".mt_rand(0, 255).", ".mt_rand(0, 255).", ".mt_rand(0, 255).")";										
				endforeach;

				$form_data['nationality_JSON']=json_encode($array_name, \JSON_UNESCAPED_UNICODE);
				$form_data['count_nationality_JSON']=json_encode($array_count);
				$form_data['rgb_nationality_JSON']=json_encode($array_rgbColor);

			
			$sql = "SELECT
						tb_house_member.religion_id,
						Count(*) AS count_religion,
						religion.religion_name
					FROM
						tb_house_member
					LEFT JOIN religion ON tb_house_member.religion_id = religion.religion_id
					INNER JOIN tb_house_registration ON tb_house_member.house_registration_id = tb_house_registration.house_registration_id
					group by religion_id "; 
							$query = $this->db->query($sql)->result_array();
							$array_name = [];
							$array_count= [];
							$array_rgbColor=[];
							foreach ($query as $key => $query_data) :
								$array_name[]= $query_data['religion_name'];	
								$array_count[]= $query_data['count_religion'];	
								$array_rgbColor[] ="rgb(".mt_rand(0, 255).", ".mt_rand(0, 255).", ".mt_rand(0, 255).")";							
							endforeach;
			
							$form_data['religion_JSON']=json_encode($array_name, \JSON_UNESCAPED_UNICODE);
							$form_data['count_religion_JSON']=json_encode($array_count);
							$form_data['rgb_religion_JSON']=json_encode($array_rgbColor);	
		
	
			$this->load->view($this->fontend . 'theme/header', $data);
			$this->load->view($this->fontend . 'report/report_information', $form_data);
			$this->load->view($this->fontend . 'theme/footer');
	
	}

	public function report_information()
	{
		//print_r($_POST);
		$province_id = $_POST['province_id'];
		
		$WHERE ="";

		if($province_id != ""){
			$WHERE =" WHERE  tb_house_registration.province_id = '$province_id'  ";
		}
	


		if (isset($_POST["district_id"])) {
			$district_id = $_POST['district_id'];
			if($district_id!=""){
				$amphur_id = $_POST['amphur_id'];
				$WHERE = $WHERE." AND  tb_house_registration.amphur_id = '$amphur_id'  AND  tb_house_registration.district_id = '$district_id'  ";
			}		
		} 
		if (isset($_POST["amphur_id"])) {
			$amphur_id = $_POST['amphur_id'];
			if($amphur_id != ""){
				$WHERE = $WHERE." AND  tb_house_registration.amphur_id = '$amphur_id'  ";
			}
	
		} 
	
	

		$data['plugin'] = array(
			'asset/node_modules/select2/dist/css/select2.min.css',
			'asset/node_modules/Chart.js/Chart.min.js',	
		);
		$data['appjs'] = array(
			'asset/node_modules/select2/dist/js/select2.full.min.js',			
			'appjs/select2.js',				
		);
				//โหลด Edit Form 1
			$form_data['provices'] = $this->Province_model->load_province();
			
			$sql = "SELECT
							tb_house_member.gender,
							Count(tb_house_member.gender) AS count_gender
					FROM
						tb_house_member
					INNER JOIN tb_house_registration ON tb_house_member.house_registration_id = tb_house_registration.house_registration_id ";			
			$group_by = " GROUP BY gender "; 

			$query = $this->db->query($sql.$WHERE.$group_by)->result_array();
			//echo $sql.$WHERE.$group_by;
			//exit();


						$array_name = [];
						$array_count= [];
						$array_rgbColor=[];
						foreach ($query as $key => $query_data) :
							switch ($query_data['gender']) :
								case 1:
									$array_name[]= "ชาย";
								break;
								case 2:
									$array_name[]= "หญิง";
								break;       
								default:
									$array_name[]= "ไม่ได้กรอกข้อมูล";
							endswitch;							
							$array_count[]= $query_data['count_gender'];
							$array_rgbColor[] ="rgb(".mt_rand(0, 255).", ".mt_rand(0, 255).", ".mt_rand(0, 255).")";											
						endforeach;

						$form_data['gender_JSON']=json_encode($array_name, \JSON_UNESCAPED_UNICODE);
						$form_data['count_gender_JSON']=json_encode($array_count);
						$form_data['rgb_gender_JSON']=json_encode($array_rgbColor);
						
			
		    	$sql = "SELECT nationality_id,Count(*)AS count_nationality
					FROM
						tb_house_member
					INNER JOIN tb_house_registration ON tb_house_member.house_registration_id = tb_house_registration.house_registration_id
					GROUP BY nationality_id "; 

				$query = $this->db->query($sql)->result_array();


				$sql = "SELECT tb_house_member.nationality_id,Count(tb_house_member.nationality_id) AS count_nationality 
				FROM
					tb_house_member	
					INNER JOIN tb_house_registration ON tb_house_member.house_registration_id = tb_house_registration.house_registration_id ";			
				$group_by = " GROUP BY nationality_id "; 
				$query = $this->db->query($sql.$WHERE.$group_by)->result_array();


				$array_name = [];
				$array_count= [];
				$array_rgbColor=[];
				foreach ($query as $key => $query_data) :
					$array_name[]= $query_data['nationality_id'];	
					$array_count[]= $query_data['count_nationality'];	
					$array_rgbColor[] ="rgb(".mt_rand(0, 255).", ".mt_rand(0, 255).", ".mt_rand(0, 255).")";										
				endforeach;

				$form_data['nationality_JSON']=json_encode($array_name, \JSON_UNESCAPED_UNICODE);
				$form_data['count_nationality_JSON']=json_encode($array_count);
				$form_data['rgb_nationality_JSON']=json_encode($array_rgbColor);

			
			$sql = "SELECT
						tb_house_member.religion_id,
						Count(*) AS count_religion,
						religion.religion_name
					FROM
						tb_house_member
					LEFT JOIN religion ON tb_house_member.religion_id = religion.religion_id
					INNER JOIN tb_house_registration ON tb_house_member.house_registration_id = tb_house_registration.house_registration_id
					group by religion_id "; 
							$query = $this->db->query($sql)->result_array();

							
							$array_name = [];
							$array_count= [];
							$array_rgbColor=[];
							foreach ($query as $key => $query_data) :
								$array_name[]= $query_data['religion_name'];	
								$array_count[]= $query_data['count_religion'];	
								$array_rgbColor[] ="rgb(".mt_rand(0, 255).", ".mt_rand(0, 255).", ".mt_rand(0, 255).")";							
							endforeach;
			
							$form_data['religion_JSON']=json_encode($array_name, \JSON_UNESCAPED_UNICODE);
							$form_data['count_religion_JSON']=json_encode($array_count);
							$form_data['rgb_religion_JSON']=json_encode($array_rgbColor);	
		
	
			$this->load->view($this->fontend . 'theme/header', $data);
			$this->load->view($this->fontend . 'report/report_information', $form_data);
			$this->load->view($this->fontend . 'theme/footer');
	}

	

}//End Class
