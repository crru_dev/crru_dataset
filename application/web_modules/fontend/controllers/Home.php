<?php
//------------[Controller File name : Home.php ]----------------------//
//----------------[ Create by: @SEK At 13-01-2021 ]----------------------//
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
		$this->load->helper('Function');
		$this->load->model('Province_model', 'Province_model');
        $user_id = $this->session->userdata('user_id');
        if ($user_id == "") {
            $this->session->sess_destroy();
            redirect('fontend/login/index');
            exit();

        }

    }

    public $fontend = 'fontend/';
    public function index()
    {
        $this->main();
    }

    // CREATE main() BY: @SEK At 13-01-2021
    // IDEA: -
    // NOTE: -
    public function main()
    {

        //@PLUGIN & @APPJS
        $data['plugin'] = array(
        );
        $data['appjs'] = array(
            'appjs/home/app.js',
        );

		//ดึงข้อมูลบ้านที่บันทึก

        //LOAD VIEW

		$session = $this->session->userdata('role');
		$user_id = $this->session->userdata('user_id');

		$role = $this->session->userdata('role');
		$where = "";
		
		if ($role == "member"){

			$where .= "AND tb_house_registration.created_by = '$user_id' ";
		}

		if($this->input->get('key')){

			$key = $this->input->get('key');
			$where .= "AND ( tb_house_registration.community_id LIKE '%$key%' OR tb_house_registration.village_name LIKE '%$key%' OR tb_house_registration.house_number LIKE '%$key%' 
			OR tb_house_registration.house_code LIKE '%$key%' ) ";
		}

		if($this->input->get('province_id')){

			$province_id = $this->input->get('province_id');
			$where .= "AND tb_house_registration.province_id = '$province_id' ";
		}

		if($this->input->get('amphur_id')){

			$amphur_id = $this->input->get('amphur_id');
			$where .= "AND tb_house_registration.amphur_id = '$amphur_id' ";
		}

		if($this->input->get('district_id')){

			$district_id = $this->input->get('district_id');
			$where .= "AND tb_house_registration.district_id = '$district_id' ";
		}

    	$sql = "SELECT 
			tb_house_registration.house_registration_id,
			tb_house_registration.community_id,
			tb_house_registration.province_id,
			tb_house_registration.amphur_id,
			tb_house_registration.district_id,
			tb_house_registration.moo,
			tb_house_registration.village_name,
			tb_house_registration.community_name,
			tb_house_registration.house_number,
			tb_house_registration.house_code,
			tb_house_registration.house_number_nearby,
			tb_house_registration.family_member,
			province.province_name,
			amphures.name_th AS amphures_name,
			districts.name_th AS districts_name,
			tb_house_registration.created_at,
			tb_house_registration.created_by,
			tb_login.id,
			tb_login.surname,
			tb_login.username,
			tb_login.`name`
			FROM
			tb_house_registration
			LEFT JOIN districts ON tb_house_registration.district_id = districts.id 
			LEFT JOIN province ON tb_house_registration.province_id = province.province_id
			LEFT JOIN amphures ON tb_house_registration.amphur_id = amphures.id
			LEFT JOIN tb_login ON tb_house_registration.created_by = tb_login.id
			WHERE 1=1  $where ";  

		$reponse['total_row'] = $this->db->query($sql)->num_rows();
		$reponse['total_all'] = $this->db->query($sql)->num_rows();
		$reponse['start_row'] = $this->uri->segment(4, '0');

		$this->load->library('pagination');
		$config['base_url'] = site_url('fontend/home/main');
		$config['total_rows'] = $reponse['total_row'];
		$config['per_page'] = 50;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;
		$config['full_tag_open']   = '<div class="pagging text-center"><nav><ul class="pagination">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']   = '</span></li>';
		$config['cur_tag_open']   = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']   = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']   = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']   = '</span></li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']   = '</span></li>';
		
		$config['reuse_query_string'] = true;
		$this->pagination->initialize($config);

		$start = $reponse['start_row'];
		$limit = $config['per_page'];

		$sql .= " ORDER BY tb_house_registration.community_id DESC  LIMIT $start, $limit ";

		$form_data['recData'] = $this->db->query($sql)->result_array();

   		$form_data['provices'] = $this->Province_model->load_province();
 
        $this->load->view($this->fontend . 'theme/header', $data);
        $this->load->view($this->fontend . 'home/index', $form_data);
        $this->load->view($this->fontend . 'theme/footer');
    }

} //END CLASS