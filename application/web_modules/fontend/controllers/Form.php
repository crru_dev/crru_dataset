<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Form extends CI_Controller
{

    public $fontend = 'fontend/';

    public function __construct()
    {
        parent::__construct();

        $user_id = $this->session->userdata('user_id');
        if ($user_id == "") {
            $this->session->sess_destroy();
            redirect('fontend/login/index');
            exit();

        }

        $this->load->model('Province_model', 'Province_model');
        $this->load->model('Form_getdata_model', 'Form_getdata_model');

    }
    public function index()
    {

        redirect('fontend/home');

    }

    public function checkHouse_code()
    {
        $house_code = $this->input->get('house_code');
        $sql = " SELECT * FROM tb_house_registration WHERE house_code = '$house_code' ";
        $tb_house_registration = $this->db->query($sql)->num_rows();

        if($tb_house_registration >=1){

            echo json_encode(array(
                'status' => false,
                'message' => 'รหัสบ้านนี้มีการใช้งานไปเรียบร้อยแล้ว ไม่สามาถใช้เลขที่บ้านนี้ได้แล้ว',
            ));
        }else{
            echo json_encode(array(
                'status' => true,
                'message' => 'รหัสบ้านนี้ใช้ได้',
            ));
        }
    }

    public function form_1()
    {
        //@PLUGIN & @APPJS

        $data['plugin'] = array(
            'asset/node_modules/select2/dist/css/select2.min.css',
            'asset/plugins/bootstrap-fileinput-master/css/fileinput.css',
            'asset/plugins/bootstrap-fileinput-master/js/fileinput.js',
            'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.min.css'
        );
        $data['appjs'] = array(
            'asset/node_modules/select2/dist/js/select2.full.min.js',
            'appjs/select2.js',
            'asset/plugins/sweetalert/sweetalert.min.js',
            '/appjs/home/form_1.js'
        );



        //เช็คข้อมูล  edit
        $house_registration_id = $this->session->userdata('house_registration_id'); //รหัสลงทะเบียนข้อมูลบ้าน
        $edit=$this->session->userdata('edit');
        if($edit!=""){
            //โหลด Edit Form 1
            $form_data['provices'] = $this->Province_model->load_province();
            //LOAD VIEW
            $sql = " SELECT * FROM tb_house_registration WHERE house_registration_id = '$house_registration_id' ";
            $form_data['row'] = $this->db->query($sql)->row_array();

            $sql = " SELECT
            tb_house_member.house_member_id,
            tb_house_member.f_name,
            tb_house_member.l_name,
            tb_relatin_head.relatin_head_name,
            tb_living_status.living_status_name,
            tb_marry_status.marry_status_name
            FROM
            tb_house_member
            LEFT JOIN tb_relatin_head ON tb_house_member.relatin_head_id = tb_relatin_head.relatin_head_id
            LEFT JOIN tb_living_status ON tb_house_member.living_status_id = tb_living_status.living_status_id
            LEFT  JOIN tb_marry_status ON tb_house_member.marry_status_id = tb_marry_status.marry_status_id
             WHERE tb_house_member.house_registration_id = '$house_registration_id' ";
            $form_data['member_data'] = $this->db->query($sql)->result_array();

            $this->load->view($this->fontend . 'theme/header', $data);
            $this->load->view($this->fontend . 'home/form_1_edit', $form_data);
            $this->load->view($this->fontend . 'theme/footer');
            $this->session->set_userdata('edit', ""); //รหัสลงทะเบียนข้อมูลบ้าน


        }else{

            $form_data['provices'] = $this->Province_model->load_province();
            //LOAD VIEW
            $this->load->view($this->fontend . 'theme/header', $data);
            $this->load->view($this->fontend . 'home/form_1', $form_data);
            $this->load->view($this->fontend . 'theme/footer');

        }

    }


    public function form_2()
    {
        //@PLUGIN & @APPJS
        $data['plugin'] = array(
            'asset/node_modules/select2/dist/css/select2.min.css',
        );
        $data['appjs'] = array(
            'asset/node_modules/select2/dist/js/select2.full.min.js',
            'appjs/select2.js',
            'asset/plugins/sweetalert/sweetalert.min.js',
        );
        $house_registration_id = $this->session->userdata('house_registration_id');
        $sql = " SELECT * FROM tb_house_information WHERE house_registration_id = '$house_registration_id' ";
        $form_data['row'] = $this->db->query($sql)->row_array();

     // print_r($this->db->last_query());
    //  exit();
        $this->load->view($this->fontend . 'theme/header', $data);
        $this->load->view($this->fontend . 'home/form_2', $form_data);
        $this->load->view($this->fontend . 'theme/footer');


    }
    public function form_3()
    {
        $house_registration_id = $this->session->userdata('house_registration_id');
        if ($house_registration_id == "") {
            header("location: " . base_url('fontend/form/form_1'));
            exit(0);

        }

        $sql = " SELECT * FROM tb_house_member WHERE house_registration_id = '$house_registration_id' ";
        $form_data['member_data'] = $this->db->query($sql)->result_array();

        //@PLUGIN & @APPJS
        $data['plugin'] = array(
			'asset/node_modules/select2/dist/css/select2.min.css',
			'asset/plugins/bootstrap-fileinput-master/css/fileinput.css',
			'asset/plugins/bootstrap-fileinput-master/js/fileinput.js',
			'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.min.css'
		);

        $data['appjs'] = array(
			'asset/node_modules/select2/dist/js/select2.full.min.js',
			'appjs/select2.js',
			'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js'
		);

        $form_data['p_name'] = $this->Form_getdata_model->load_prefix_name();
        $form_data['nationality'] = $this->Form_getdata_model->load_nationality();
        $form_data['religion'] = $this->Form_getdata_model->load_religion();
        $form_data['marry'] = $this->Form_getdata_model->load_marry_status();
        $form_data['edu_status'] = $this->Form_getdata_model->load_edu_status();
        $form_data['edu_level'] = $this->Form_getdata_model->load_edu_level();
        $form_data['cal_level'] = $this->Form_getdata_model->load_cal_level();
        $form_data['writing_level'] = $this->Form_getdata_model->load_writing_level();
        $form_data['reading_level'] = $this->Form_getdata_model->load_reading_level();
        $form_data['relatin_head'] = $this->Form_getdata_model->load_relatin_head();
        $form_data['living_status'] = $this->Form_getdata_model->load_living_status();
        $form_data['skill'] = $this->Form_getdata_model->load_skill();
        //ปรับภาษาให้เหลือแค่ภาษาที่ 2
        $form_data['second_lang'] = $this->Form_getdata_model->load_second_lang();
        // $form_data['local_lang'] = $this->Form_getdata_model->load_local_lang();

// ดึงข้อมูลสมาชิกในครัวเรื่อน tb_house_member

        //   print_r($form_data['nationality']);
        //exit();

        //LOAD VIEW
        $this->load->view($this->fontend . 'theme/header', $data);
        $this->load->view($this->fontend . 'home/form_3', $form_data);
        $this->load->view($this->fontend . 'theme/footer');

    }

    public function form_4()
    {

        $house_registration_id = $this->session->userdata('house_registration_id');
        if ($house_registration_id == "") {
            header("location: " . base_url('fontend/form/form_1'));
            exit(0);

        }
        //@PLUGIN & @APPJS
        $data['plugin'] = array(
            'asset/node_modules/select2/dist/css/select2.min.css',
        );
        $data['appjs'] = array(
            'asset/node_modules/select2/dist/js/select2.full.min.js',
            'appjs/select2.js',
        );

        $sql = " SELECT * FROM tb_house_member WHERE house_registration_id = '$house_registration_id' ";
        $form_data['member_data'] = $this->db->query($sql)->result_array();
        $form_data['occup_data'] = $this->Form_getdata_model->load_occup();

        $form_data['occup_sub_data'] = $this->Form_getdata_model->load_occup_sub();
        $form_data['area_occup_data'] = $this->Form_getdata_model->load_area_occup();
        //LOAD VIEW
        $this->load->view($this->fontend . 'theme/header', $data);
        $this->load->view($this->fontend . 'home/form_4', $form_data);
        $this->load->view($this->fontend . 'theme/footer');


    }

    public function get_province()
    { //อำเภอ
        if (isset($_POST['function']) && $_POST['function'] == 'provinces') {
            $id = $_POST['id'];
            $sql = "SELECT * FROM amphures WHERE province_id='$id'";
            $query = $this->db->query($sql)->result_array();
            echo '<option value="" selected disabled>-กรุณาเลือกอำเภอ-</option>';
            foreach ($query as $value) {
                echo '<option value="' . $value['id'] . '">' . $value['name_th'] . '</option>';

            }
        }

        if (isset($_POST['function']) && $_POST['function'] == 'amphures') {
            $id = $_POST['id'];
            $sql = "SELECT * FROM districts WHERE amphure_id='$id'";
            $query = $this->db->query($sql)->result_array();
            echo '<option value="" selected disabled>-กรุณาเลือกตำบล-</option>';
            foreach ($query as $value2) {
                echo '<option value="' . $value2['id'] . '">' . $value2['name_th'] . '</option>';

            }
        }

    }


    public function form_6()
    {
        //ส่วนที่ 3 ข้อมูลด้านเศรษฐกิจ

        $house_registration_id = $this->session->userdata('house_registration_id');
        $house_information_recorded = $this->session->userdata('house_information_recorded');

        if ($house_registration_id == "") {
            header("location: " . base_url('fontend/form/form_1'));
            exit(0);
        }

        //@PLUGIN & @APPJS
        $data['plugin'] = array(
            'asset/node_modules/select2/dist/css/select2.min.css',
            'asset/node_modules/select2/dist/js/select2.full.min.js',
            'appjs/select2.js',
            'asset/plugins/sweetalert/sweetalert.min.js',
            'asset/theme/eliteadmin/assets/node_modules/icheck/skins/all.css',
            'asset/theme/eliteadmin/assets/node_modules/icheck/icheck.min.js',
        );
        $data['appjs'] = array(
            'appjs/home/form_5.js',

        );
        $form_data['income_type_data'] = $this->Form_getdata_model->load_income_type();
        $form_data['cost_type_data'] = $this->Form_getdata_model->load_cost_type();

         // tb_economic_problems //แก้ไขรอบสอง
       // $form_data['econ_type_data'] = $this->Form_getdata_model->load_tb_economic_problems();

        $table = 'tb_economic_problems';
        $data['q_economic_problems'] = $this->db->get($table)->result();

        $table = 'answer_economic_problems';
        $cond = ['house_registration_id' => $house_registration_id];
        $this->db->select('economic_problems_id');
        $data['answer_list'] = $this->db->get_where($table, $cond)->result();
        
//// --------- end แก้ใขรอบสอง
       

        $sql_house_income = "SELECT *  FROM  tb_house_information  WHERE   house_registration_id = '$house_registration_id' ";
        $form_data['house_information'] = $this->db->query($sql_house_income)->row_array();

        
        //LOAD VIEW
        $this->load->view($this->fontend . 'theme/header', $data);
        $this->load->view($this->fontend . 'home/form_6', $form_data);
        $this->load->view($this->fontend . 'theme/footer');
    }
//ส่วนที่ 6 ประเภทของการรวมกลุ่ม/องค์กรในชุมชนที่ท่านมีส่วนร่วมหรือเป็นสมาชิก
    public function form_10()
    {
        $house_registration_id = $this->session->userdata('house_registration_id');

        if ($house_registration_id == "") {
            header("location: " . base_url('fontend/form/form_1'));
            exit(0);
        }

       	$data['plugin'] = array(
			'asset/node_modules/select2/dist/css/select2.min.css',
			'asset/plugins/bootstrap-fileinput-master/css/fileinput.css',
			'asset/plugins/bootstrap-fileinput-master/js/fileinput.js',
			'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.min.css'
		);

        $data['appjs'] = array(
			'asset/node_modules/select2/dist/js/select2.full.min.js',
			'appjs/select2.js',
			'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js'
		);

        $form_data['group_format']  = $this->Form_getdata_model->load_group_format();
        $form_data['group_type']    = $this->Form_getdata_model->load_group_type();

        $this->load->view($this->fontend . 'theme/header', $data);
        $this->load->view($this->fontend . 'home/form_10', $form_data);
        $this->load->view($this->fontend . 'theme/footer');
    }
    public function form_10_edit()
    {
        $house_registration_id = $this->session->userdata('house_registration_id');

        if ($house_registration_id == "") {
            header("location: " . base_url('fontend/form/form_1'));
            exit(0);
        }
        $data['plugin'] = array(
            'asset/node_modules/select2/dist/css/select2.min.css',
        );
        $data['appjs'] = array(
            'asset/node_modules/select2/dist/js/select2.full.min.js',
            'appjs/select2.js',
        );

        $form_data['group_format'] = $this->Form_getdata_model->load_group_format();
        $form_data['group_type'] = $this->Form_getdata_model->load_group_type();

        $this->load->view($this->fontend . 'theme/header', $data);
        $this->load->view($this->fontend . 'home/form_10', $form_data);
        $this->load->view($this->fontend . 'theme/footer');
    }

    public function form_1_edit()
    {

        $data['plugin'] = array(
            'asset/node_modules/select2/dist/css/select2.min.css',
            'asset/plugins/bootstrap-fileinput-master/css/fileinput.css',
            'asset/plugins/bootstrap-fileinput-master/js/fileinput.js',
            'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.min.css'
        );
        $data['appjs'] = array(
            'asset/node_modules/select2/dist/js/select2.full.min.js',
            'appjs/select2.js',
            'asset/plugins/sweetalert/sweetalert.min.js',
            '/appjs/home/form_1.js',
            'asset/theme/eliteadmin/assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js'
        );

        $house_registration_id = $this->session->userdata('house_registration_id'); //รหัสลงทะเบียนข้อมูลบ้าน

            //โหลด Edit Form 1
            $form_data['provices'] = $this->Province_model->load_province();
            //LOAD VIEW
            $sql = " SELECT * FROM tb_house_registration WHERE house_registration_id = '$house_registration_id' ";
            $form_data['row'] = $this->db->query($sql)->row_array();

            $sql = " SELECT
            tb_house_member.house_member_id,
            tb_house_member.f_name,
            tb_house_member.l_name,
            tb_relatin_head.relatin_head_name,
            tb_living_status.living_status_name,
            tb_marry_status.marry_status_name
            FROM
            tb_house_member
            LEFT JOIN tb_relatin_head ON tb_house_member.relatin_head_id = tb_relatin_head.relatin_head_id
            LEFT JOIN tb_living_status ON tb_house_member.living_status_id = tb_living_status.living_status_id
            LEFT  JOIN tb_marry_status ON tb_house_member.marry_status_id = tb_marry_status.marry_status_id
             WHERE tb_house_member.house_registration_id = '$house_registration_id' ";
            $form_data['member_data'] = $this->db->query($sql)->result_array();

            $this->load->view($this->fontend . 'theme/header', $data);
            $this->load->view($this->fontend . 'home/form_1_edit', $form_data);
            $this->load->view($this->fontend . 'theme/footer');
            $this->session->set_userdata('edit', ""); //รหัสลงทะเบียนข้อมูลบ้าน




    }

   public function form_edit_list()
   {
       $get = $this->input->get(null, true);

       $cond = ['house_registration_id' => $get['house_registration_id']];
       $table = 'tb_house_registration';
       $data['house_info'] = $this->db->get_where($table, $cond)->row_array();
       // exit();
       $this->session->set_userdata('house_registration_id', $get['house_registration_id']); //รหัสลงทะเบียนข้อมูลบ้าน

       $this->load->view('fontend/home/form_list_edit', $data);


   }
   public function form_covid()
   {

    $sess = $this->session->userdata();
        $house_registration_id = $sess['house_registration_id'];
       

        //@PLUGIN & @APPJS
		$data['plugin'] = array(
            'asset/theme/eliteadmin/assets/node_modules/icheck/skins/all.css',
            'asset/theme/eliteadmin/assets/node_modules/icheck/icheck.min.js',
        );
		$data['appjs'] = array(
            'appjs/home/form_8.js'
        );
        $table = 'tb_covid_defense';
        $data['q_covid_defense'] = $this->db->get($table)->result();

        $table = 'answer_covid_defense';
        $cond = ['house_registration_id' => $house_registration_id];
        $this->db->select('covid_defense_id');
        $data['answer_list'] = $this->db->get_where($table, $cond)->result();

        
    $this->load->view($this->fontend . 'theme/header');
    $this->load->view($this->fontend . 'home/form_covid', $data);
    $this->load->view($this->fontend . 'theme/footer');


   }
   

} //END CLASS
