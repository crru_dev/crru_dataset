<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h4 class="text-themecolor">ข้อมูลผู้ใช้</h4>
			</div>
			<div class="col-md-7 align-self-center text-right">
				<div class="d-flex justify-content-end align-items-center">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active">ข้อมูลผู้ใช้</li>
					</ol>
					<a href="<?php echo site_url('fontend/users/create')?>" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> เพิ่มข้อมูลผู้ใช้</a>
				</div>
			</div>
		</div>

		<!-- Row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header bg-info">
						<h4 class="m-b-0 text-white">ข้อมูลผู้ใช้</h4>
					</div>
					<div class="card-body">
					<table class='table table-bordered table-hover table-condensed data-table'>
							<thead>
								<tr class="">
									<th>username</th>
									<th>ชื่อ สกุล</th>
									<th>ประเภทสิทธิ์</th>
									<th>จัดการ</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($users as $user) : ?>
									<tr>
										<td><?php echo $user->username ?></td>
										<td><?php echo $user->name . ' ' . $user->surname ?></td>
										<td><?php echo getTypeUser($user->role) ?></td>
										<td>
											<a href="<?php echo site_url('fontend/users/edit/'.$user->id) ?>" class="btn btn-sm btn-warning">
												<span class="ti-pencil-alt"></span>
												EDIT
											</a>
											<button type="button" href="<?php echo site_url('fontend/users/delete/'.$user->id) ?>" class="btn btn-sm btn-danger btn-delete" >
												<span class="ti-trash"></span>
												DELETE
											</button>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Row -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- script -->

<script type="text/javascript">
 $(function () {
	$('.data-table').DataTable({
		responsive: true
	});

});

</script>

<!-- End script -->