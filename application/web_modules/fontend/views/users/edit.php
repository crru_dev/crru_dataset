<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h4 class="text-themecolor">แก้ไข ข้อมูลผู้ใช้</h4>
			</div>
			<div class="col-md-7 align-self-center text-right">
				<div class="d-flex justify-content-end align-items-center">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active">แก้ไข ข้อมูลผู้ใช้</li>
					</ol>
				</div>
			</div>
		</div>

		<!-- Row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header bg-info">
						<h4 class="m-b-0 text-white">แก้ไข ข้อมูลผู้ใช้</h4>
					</div>
					<div class="card-body">
						<form method="post" action="<?php echo site_url('fontend/users/update')?>" name="form" id="form" enctype="multipart/form-data">
							<div class="form-body">
								<div class="form-group row">
									<label for="example-text-input" class="col-2 col-form-label">username</label>
									<div class="col-10">
									<input type="hidden" name="id" id ="" value="<?php echo $user->id?>" >
									<?php echo form_input('username', $user->username, ['placeholder' => 'username', 'class' => 'form-control', 'required' => 'required', 'readonly' => 'readonly' ]); ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-2 col-form-label">Password</label>
									<div class="col-10">(  * ถ้ากรอก รหัสจะถูกเปลี่ยนแปลงทันที )
									<?php echo form_input('password', '', ['placeholder' => 'รหัสผ่าน', 'class' => 'form-control', 'required' => 'required']); ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-2 col-form-label">ชื่อ</label>
									<div class="col-10">
									<?php echo form_input('name', $user->name, ['placeholder' => 'ชื่อ', 'class' => 'form-control', 'required' => 'required' ]); ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-2 col-form-label">นามสกุล</label>
									<div class="col-10">
									<?php echo form_input('surname', $user->surname, ['placeholder' => 'นามสกุล', 'class' => 'form-control', 'required' => 'required' ]); ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-2 col-form-label">ประเภทผู้ใช้</label>
									<div class="col-10">
									<?php echo  form_dropdown('role', ['' => 'เลือก', 'admin' =>'admin', 'member'=>' ผู้ใช้ทั่วไป' ], $user->role, ['class' => 'form-control', 'required' => 'required']) ?>
									</div>
								</div>

								<div class="form-group row">
									<div class="form-actions">
										<button type="submit" id="btn-form" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
										<button type="button" onclick="save_data2()" class="btn btn-inverse">Cancel</button>
									</div>
								</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- Row -->

	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- script -->

<script type="text/javascript">
$("#btn-form").click(function(e) { // เมื่อกดปุ่มบันทึกข้อมูล

		e.preventDefault();
        $('#btn-form').button('loading');
        $('#form').ajaxSubmit({ //ฟอร์มบันทึกข้อมูล
            dataType: 'json',
            success: reponseData // เมื่อทำงานเสร็จให้ไปทำ fucntion นี้ต่อ
        });
    });

    function reponseData(response) { // function เมื่อ ส่งค่าไปทำงานเสร็จ
		if(response.status == true){
			
			Swal.fire({
                title: '<strong>แจ้งเตือน</u></strong>',
                type: 'success',
                html: '',
                showCloseButton: false,
                showCancelButton: false,
                focusConfirm: false
			})
			
			setTimeout(function(){

				window.location.reload();
			}, '1000');
		}else{

			Swal.fire({
                title: '<strong>แจ้งเตือน</u></strong>',
                type: 'error',
                html: response.massage,
                showCloseButton: false,
                showCancelButton: false,
                focusConfirm: false
            })
		}
    }
</script>

<!-- End script -->