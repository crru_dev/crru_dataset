<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h4 class="text-themecolor">ข้อมูลส่วนตัว</h4>
			</div>
			<div class="col-md-7 align-self-center text-right">
				<div class="d-flex justify-content-end align-items-center">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active">ข้อมูลส่วนตัว</li>
					</ol>
					
				</div>
			</div>
		</div>

		<!-- Row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header bg-info">
						<h4 class="m-b-0 text-white">ข้อมูลส่วนตัว</h4>
					</div>
					<div class="card-body">
						<form method="post" action="<?php echo site_url('fontend/users/update_password')?>" name="form" id="form" enctype="multipart/form-data">
							<div class="form-body">
								<div class="form-group row">
									<label for="example-text-input" class="col-2 col-form-label">Username</label>
									<div class="col-10">
										<?php echo form_input('username', $user->username, ['placeholder' => 'username', 'class' => 'form-control', 'readonly' => 'readonly']); ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-2 col-form-label">รหัสผ่านเก่า</label>
									<div class="col-10">
									<?php echo form_input('password', '', ['placeholder' => 'รหัสผ่านเก่า', 'class' => 'form-control', 'required' => 'required']); ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-2 col-form-label">รหัส Password ใหม่</label>
									<div class="col-10">
									<?php echo form_input('newpass', '', ['placeholder' => 'รหัสผ่านใหม่', 'class' => 'form-control', 'required' => 'required']); ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-2 col-form-label">ยืนยัน Password ใหม่</label>
									<div class="col-10">
									<?php echo form_input('confpassword', '', ['placeholder' => 'รหัสผ่านใหม่', 'class' => 'form-control', 'required' => 'required']); ?>
									</div>
								</div>

								<div class="form-group row">
									<div class="form-actions">
										<button type="submit" id="btn-form" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
										<button type="button" onclick="save_data2()" class="btn btn-inverse">Cancel</button>
									</div>
								</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- Row -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- script -->
<script type="text/javascript">
$("#btn-form").click(function(e) { // เมื่อกดปุ่มบันทึกข้อมูล

		e.preventDefault();
        $('#btn-form').button('loading');
        $('#form').ajaxSubmit({ //ฟอร์มบันทึกข้อมูล
            dataType: 'json',
            success: reponseData // เมื่อทำงานเสร็จให้ไปทำ fucntion นี้ต่อ
        });
    });

    function reponseData(response) { // function เมื่อ ส่งค่าไปทำงานเสร็จ
		if(response.status == true){
			
			Swal.fire({
                title: '<strong>แจ้งเตือน</u></strong>',
                type: 'success',
                html: '',
                showCloseButton: false,
                showCancelButton: false,
                focusConfirm: false
			})
			
			setTimeout(function(){

				window.location.reload();
			}, '1000');
		}else{

			Swal.fire({
                title: '<strong>แจ้งเตือน</u></strong>',
                type: 'error',
                html: response.massage,
                showCloseButton: false,
                showCancelButton: false,
                focusConfirm: false
            })
		}
    }
</script>

<!-- End script -->