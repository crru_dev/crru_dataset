<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h4 class="text-themecolor">ข้อมูลส่วนตัว</h4>
			</div>
			<div class="col-md-7 align-self-center text-right">
				<div class="d-flex justify-content-end align-items-center">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active">ข้อมูลส่วนตัว</li>
					</ol>
				</div>
			</div>
		</div>

		<!-- Row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header bg-info">
						<h4 class="m-b-0 text-white">ข้อมูลส่วนตัว</h4>
					</div>
					<div class="card-body">
						<form method="post" action="" name="form-1" id="form-1" enctype="multipart/form-data">
							<div class="form-body">
								<div class="form-group row">
									<label for="example-text-input" class="col-2 col-form-label">Username</label>
									<div class="col-10">
										<?php echo form_input('username', $user->username, ['placeholder' => 'username', 'class' => 'form-control', 'readonly' => 'readonly']); ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-2 col-form-label">ชื่อ</label>
									<div class="col-10">
										<?php echo form_input('name', $user->name, ['placeholder' => 'ชื่อ', 'class' => 'form-control']); ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-text-input" class="col-2 col-form-label">นามสกุล</label>
									<div class="col-10">
										<?php echo form_input('surname', $user->surname, ['placeholder' => 'นามสกุล', 'class' => 'form-control']); ?>
									</div>
								</div>

								<div class="form-group row">
									<div class="form-actions">
										<button type="submit" id="btn-save-form-1" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
										<button type="button" onclick="save_data2()" class="btn btn-inverse">Cancel</button>
									</div>
								</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- Row -->

	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- script -->

<script type="text/javascript">

</script>

<!-- End script -->