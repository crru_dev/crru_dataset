<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">CRRU DATASET</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">รายงาน
                        </li>
                        <li class="breadcrumb-item active">ข้อมูลพื้นฐานครัวเรือน
                        </li>
                    </ol>
                </div>
            </div>
        </div>

        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">ระบบสารสนเทศฐานข้อมูลระดับบุคคลและครัวเรือน (ด้านข้อมูลพื้นฐานครัวเรื่อน)</h4>
                    </div>
                    <div class="card-body">
                        <form method="post" action="<?php echo site_url('fontend/report/report_information')?>" name="form-1" id="form-1"  enctype="multipart/form-data">
                            <div class="form-body">

                                <div class="row p-t-20">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">จังหวัด</label>
                                            <select class="select2 form-control custom-select" data-placeholder="Select"
                                                id="provinces" name="province_id" required onchange="get_amphure(this.value)">>
                                                <option value="">เลือกจังหวัด</option>
                                                <?php
                                                foreach ($provices as $provice) {
                                                    ?>
                                                <option value="<?php echo $provice['province_id']; ?>">
                                                    <?php echo $provice['province_name']; ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="amphure" id="sss" class="control-label">อำเภอ</label>

                                            <select name="amphur_id" id="amphures"
                                                class="select2 form-control custom-select"
                                                onchange="get_district(this.value)"> require>
                                                <option value="">เลือกอำเภอ</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="district" class="control-label">ตำบล</label>
                                            <select name="district_id" id="district"
                                                class="select2 form-control custom-select">
                                                <option value="">เลือกตำบล</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary" name="Search" id="Search">Search</button>
                      
                        </form>
                        <div class="row">
                        
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">เพศ</h4>
                                        <div>
                                            <canvas id="gender" height="150"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">สันชาติ</h4>
                                        <div>
                                            <canvas id="nationality" height="150"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">ศาสสนา</h4>
                                        <div>
                                            <canvas id="religion" height="150"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>            
                        
                        <div>











                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- script -->
<style>
    .bg-red{
        background-color: red;
        color:white;
    }

    .bg-greeen{
        background-color: #7ac606;
        color:white;
    }
</style>

<script src="http://eliteadmin.themedesigner.in/demos/bt4/eliteadmin/dist/js/pages/validation.js"></script>

<script type="text/javascript">  
function get_amphure(id_province) {

    $.ajax({
        type: "POST",
        url: "<?php echo site_url('index.php/fontend/form/get_province') ?>",
        data: {
            id: id_province,
            function: 'provinces'
        },
        success: function(data) {
            $('#amphures').html(data);
            $('#districts').html('');
        }
    });
}

function get_district(id_amphures) {
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('index.php/fontend/form/get_province') ?>",
        data: {
            id: id_amphures,
            function: 'amphures'
        },
        success: function(data) {
            $('#district').html(data);
        }
    });
}

/*<!-- Pie Chart -->*/
    /*<!-- ============================================================== -->*/
    
    $(function() {


        new Chart(document.getElementById("gender"),
        {
            "type":"pie",
            "data":{"labels":<?php echo $gender_JSON;?>,
            "datasets":[{
                "label":"เพศ",
                "data":<?php echo $count_gender_JSON;?>,
                "backgroundColor":<?php echo $rgb_gender_JSON;?>}
            ]}
        });

        new Chart(document.getElementById("nationality"),
        {
            "type":"pie",
            "data":{"labels":<?php echo $nationality_JSON;?>,
            "datasets":[{
                "label":"สัญชาติ",
                "data":<?php echo $count_nationality_JSON;?>,
                "backgroundColor":<?php echo $rgb_nationality_JSON;?>}
            ]}
        });

        new Chart(document.getElementById("religion"),
        {
            "type":"pie",
            "data":{"labels":<?php echo $religion_JSON;?>,
            "datasets":[{
                "label":"ศาสนา",
                "data":<?php echo $count_religion_JSON;?>,
                "backgroundColor":<?php echo $rgb_religion_JSON;?>}
            ]}
        });


    });
    /*<!-- ============================================================== -->*/
	
</script>

<!-- End script -->