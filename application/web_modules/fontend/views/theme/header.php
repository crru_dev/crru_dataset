<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="site_url" content="<?php echo site_url(); ?>">
    <meta name="base_url" content="<?php echo base_url(); ?>">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>asset/theme/eliteadmin/assets/images/favicon.png">
    <title>CRRU DATSET</title>
    <!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="<?php echo base_url(); ?>asset/theme/eliteadmin/assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <!--Toaster Popup message CSwS -->
    <link href="<?php echo base_url(); ?>asset/theme/eliteadmin/assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>asset/theme/eliteadmin/dist/css/style.css" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="<?php echo base_url(); ?>asset/theme/eliteadmin/dist/css/pages/dashboard1.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Trirong&display=swap" rel="stylesheet">
    <style media="screen">
        body, h1, h2, h3, h4, h5 {
            font-family: 'Trirong', serif;
        }
        .color-red{
            color:red;
        }
        .breadcrumb-item {
            font-size: 16px !important;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- All Jquery -->
<script src="<?php echo base_url(); ?>asset/theme/eliteadmin/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>asset/plugins/jquery-form/jquery.form.js"></script>
</head>

<body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label"><?php echo site_url(); ?></p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?php $this->load->view('fontend/theme/navbar.php'); ?>
        <?php $this->load->view('fontend/theme/left_sidebar.php'); ?>
