<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h4 class="text-themecolor">
            <i class="fa fa-database" aria-hidden="true"></i>
            ระบบฐานข้อมูลราชภัฏ โดย มหาวิทยาลัยราชภัฏเชียงราย (CRRU DATASET)
        </h4>
    </div>
    <div class="col-md-6 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <?php foreach ($breadcrumb as $key => $bc): ?>
                    <li class="breadcrumb-item active"><?php echo $bc; ?></li>
                <?php endforeach; ?>

            </ol>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
