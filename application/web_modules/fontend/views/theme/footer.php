        </div><!-- Wrapper -->

    <!-- Bootstrap popper Core JavaScript -->
    <script src="<?php echo base_url(); ?>asset/theme/eliteadmin/assets/node_modules/popper/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/theme/eliteadmin/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>asset/theme/eliteadmin/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>asset/theme/eliteadmin/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>asset/theme/eliteadmin/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>asset/theme/eliteadmin/dist/js/custom.min.js"></script>
    <!-- This page plugins -->
    <!--morris JavaScript -->
    <script src="<?php echo base_url(); ?>asset/theme/eliteadmin/assets/node_modules/raphael/raphael-min.js"></script>
    <script src="<?php echo base_url(); ?>asset/theme/eliteadmin/assets/node_modules/morrisjs/morris.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/theme/eliteadmin/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Popup message jquery -->
    <script src="<?php echo base_url(); ?>asset/theme/eliteadmin/assets/node_modules/toast-master/js/jquery.toast.js"></script>

    <!-- Plugins -->
    <script src="<?php echo base_url(); ?>appjs/global.js" charset="utf-8"></script>
    <?php
    if (isset($plugin)) {
        echo generate_plugin($plugin);
    }
    ?>

    <!-- AppJS-->
    <?php
    if (isset($appjs)) {
        echo generate_plugin($appjs);
    }
    ?>
    <script>
        $(".btn-delete").click(function(e) {

            if (confirm("ยืนยันการลบข้อมูล") == true) {

                var href = $(this).attr('href');
                $.get(href, function(response) {

                    if (response.status == true) {
                        
                        Swal.fire({
                            title: '<strong>แจ้งเตือน</u></strong>',
                            type: 'success',
                            html: '',
                            showCloseButton: false,
                            showCancelButton: false,
                            focusConfirm: false
                        })

                        setTimeout(function() {

                            window.location.reload();
                        }, '1000');
                    } else {

                    }
                }, 'json');
            }
        });
    </script>
</body>

</html>
