<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->

<?php $session = $this->session->userdata();?>

<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="user-pro"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                    <img src="<?php echo base_url(); ?>asset/theme/eliteadmin/assets/images/users/9.svg" alt="user-img" class="img-circle">
                    <span class="hide-menu"><?php echo $session['fullname'] ?? "" ?></span></a>
                    <ul aria-expanded="false" class="collapse">
                
                        <li>
                            <a onclick="return confirm('are you sure?')" href="<?php echo site_url('fontend/login/logout') ?>">
                                <i class="fa fa-caret-right"></i>
                                ออกจากระบบ
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a class="waves-effect waves-dark" href="<?php echo site_url('fontend/form/form_1') ?>"
                        aria-expanded="false">
                        <i class="  fas fa-edit"></i>
                        <span class="hide-menu">กรอกแบบสอบถาม</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="<?php echo site_url('fontend/home'); ?>"
                        aria-expanded="false">
                        <i class="far fa-list-alt text-danger"></i>
                        <span class="hide-menu">รายการแบบสอบถาม</span>
                    </a>
                </li>
               <!-- <li>
                        <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="fa fa-list-alt"></i>
                            <span class="hide-menu">รายงาน</span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li>
                                <a href="<?php echo site_url('fontend/report/house_information') ?>">
                                    <i class="fa fa-caret-right"></i>
                                    ข้อมูลพื้นฐานครัวเรือน
                                </a>
                            </li>
                            <li>

                                <a href="<?php echo site_url('fontend/report/economic_information') ?>">
                                    <i class="fa fa-caret-right"></i>
                                    ข้อมูลด้านเศรษฐกิจ
                                </a>
                            </li>
                        </ul>
                    </li>-->

                <?php if ($session['role'] == 'admin'): ?>
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="fa fa-user"></i>
                            <span class="hide-menu">ข้อมูลผู้ใช้งาน</span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li>
                                <a href="<?php echo site_url('fontend/users/create') ?>">
                                    <i class="fa fa-caret-right"></i>
                                    สร้างข้อมูลผู้ใช้
                                </a>
                            </li>
                            <li>

                                <a href="<?php echo site_url('fontend/users/index') ?>">
                                    <i class="fa fa-caret-right"></i>
                                    ข้อมูลผู้ใช้
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php endif;?>
                <li>
                    <a class="waves-effect waves-dark" onclick="return confirm('are you sure?')"
                        href="<?php echo site_url('fontend/login/logout') ?>" aria-expanded="false">
                        <i class="fas fa-sign-out-alt"></i>
                        <span class="hide-menu">ออกจากระบบ</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
