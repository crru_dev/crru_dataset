<div class="row login-register  justify-content-center" style="padding:3% 0 !important;background-image:url(<?php echo base_url('')?>asset/theme/eliteadmin/assets/images/background/login-register.jpg);">
	<div class="col-xs-12 col-md-7 col-lg-5">
		<div class="card" style="background: rgba(255, 255, 255, 0.7); ">
			<div class="card-body">
				<form class="form-horizontal form-material" id="loginform" method="post" action="<?php echo site_url('fontend/login/signin')?>">
					<div class="text-center">
						<img src="<?php echo base_url('asset/images/crru_logo.png'); ?>" width="130" alt="">
					</div>
					<h3 class="text-center m-b-20 mt-2">ระบบฐานข้อมูลราชภัฏ โดยมหาวิทยาลัยราชภัฏเชียงราย <br>(CRRU DATASET)</h3>
					<h4 class="text-center m-b-20">ลงชื่อเข้าใช้ระบบ</h4>
					<div class="form-group ">
						<div class="col-xs-12">
							<input class="form-control" type="text" name="username" required="" placeholder="Username">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input class="form-control" type="password" name="password" required="" placeholder="Password">
						</div>
					</div>
					<div class="form-group row text-center">
						<div class="col-md-12">
							<div class="">
								<?php if($msg_error):?>
									<div class="label label-md label-danger text-center" style="font-size: 100%;"><?php echo $msg_error;?></div>
								<?php endif;?>
								<!--<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="customCheck1">
									<label class="custom-control-label" for="customCheck1">Remember me</label>
								</div>
								<div class="ml-auto">
									<a href="javascript:void(0)" id="to-recover" class="text-muted"><i class="fas fa-lock m-r-5"></i> Forgot pwd?</a>
								</div>-->
							</div>
						</div>
					</div>
					<div class="form-group text-center">

						<div class="col-xs-12 p-b-20">
							<button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">เข้าสู่ระบบ</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
							<!--div class="social">
								<button class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fab fa-facebook-f"></i> </button>
								<button class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fab fa-google-plus-g"></i> </button>
							</div>-->
						</div>
					</div>
					<!--<div class="form-group m-b-0">
						<div class="col-sm-12 text-center">
							Don't have an account? <a href="pages-register.html" class="text-info m-l-5"><b>Sign Up</b></a>
						</div>
					</div>-->
				</form>

				<!--<form class="form-horizontal" id="recoverform" action="index.html">
					<div class="form-group ">
						<div class="col-xs-12">
							<h3>Recover Password</h3>
							<p class="text-muted">Enter your Email and instructions will be sent to you! </p>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-xs-12">
							<input class="form-control" type="text" required="" placeholder="Email">
						</div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
						</div>
					</div>
				</form>-->
			</div>
		</div>

	</div>
</div>
