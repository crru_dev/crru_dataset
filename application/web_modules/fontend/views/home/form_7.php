<?php $house_registration_id = $this->session->userdata('house_registration_id'); ?>
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Container fluid  -->
    <div class="container-fluid">
        <?php
        $community_id = $this->session->userdata('community_id');
        $house_information_recorded = $this->session->userdata('house_information_recorded');
        $breadcrumb = ['breadcrumb'=>[
            'หน้าหลัก',
            'เลขที่แบบสอบถาม '.$community_id,
            'บันทึกครั้งที่ '.$house_information_recorded,
            'ส่วนที่ 4 สิ่งแวดล้อมและธรรมชาติ'
            ]];
        $this->load->view('fontend/theme/breadcrumb', $breadcrumb); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">ฟอร์ม 7 : ส่วนที่ 4 สิ่งแวดล้อมและธรรมชาติ</h4>
                    </div>
                    <div class="card-body">

                        <form name="form_5" action="<?php echo site_url('fontend/save/form_7'); ?>" method="post" id="form_5" name="form_5">
                            <!-- 4.1 ข้อมูลครัวเรือน-->
                            <div class="row">
                                <div class="col-12">
                                    <div class="card border">
                                        <div class="card-header bg-info">
                                            <h4 class="m-b-0 text-white">4.1 ข้อมูลครัวเรือน</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <table class="table table-bordered table-hover">
                                                        <tbody>
                                                        <?php foreach ($q_environment as $key => $env):
                                                            $table ='answer_environment';
                                                            $cond = [
                                                                'house_registration_id'=>$house_registration_id,
                                                                'environment_id' => $env->id
                                                            ];
                                                            $answer_env = $this->db->get_where($table, $cond)->row();
                                                        ?>
                                                            <tr>
                                                                <th width="50"><?php echo $key+1; ?></th>
                                                                <th><h5><?php echo $env->name; ?><h5></th>
                                                                <td class="text-center">
                                                                    <input type="hidden" name="environment[<?php echo $env->id ?>][text]" value="<?php echo $env->name; ?>" required />
                                                                    <input type="hidden" name="environment[<?php echo $env->id ?>][environment_id]"value="<?php echo $env->id; ?>" required />

                                                                    <input type="radio" class="icheck-checkbox" <?php echo (!is_null($answer_env) && $answer_env->answer == 'true') ? 'checked':''; ?>
                                                                        name="environment[<?php echo $env->id ?>][answer]" value="true" required />
                                                                    <span style="font-size:16px;" class="mr-3 text-success"> มี</span>

                                                                    <input type="radio" class="icheck-checkbox" <?php echo (!is_null($answer_env) && $answer_env->answer == 'false') ? 'checked':''; ?>
                                                                        name="environment[<?php echo $env->id ?>][answer]" value="false" required />
                                                                    <span style="font-size:18px;" class="text-danger"> ไม่มี</span>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- 4.2 ข้อมูลด้านทรัพยากรธรรมชาติ-->
                            <div class="row">
                                <div class="col-12">
                                    <div class="card border">
                                        <div class="card-header bg-info">
                                            <h4 class="m-b-0 text-white">4.2 ข้อมูลด้านทรัพยากรธรรมชาติ <small>( ทรัพยากรธรรมชาติในชุมชนของท่าน )</small></h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <table class="table table-bordered table-hover">
                                                        <thead>
                                                            <tr class="bg-secondary">
                                                                <th class="text-center">ทรัพยากรธรรมชาติ </th>
                                                                <th class="text-center">มี</th>
                                                                <th class="text-center">ไม่มี</th>
                                                                <th class="text-center">โปรดระบุชื่อ/ลักษณะ/พิกัด/รายละเอียดเพิ่มเติม</th>
                                                            </tr>

                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($q_resource as $key => $resource):
                                                            $table ='answer_natural_resources';
                                                            $cond = [
                                                                'house_registration_id'=>$house_registration_id,
                                                                'natural_resources_id' => $resource->id
                                                            ];
                                                            $answer_nature = $this->db->get_where($table, $cond)->row();
                                                            // echo "<pre>";
                                                            // print_r($answer_nature);

                                                        ?>
                                                            <tr>
                                                                <th><h5><?php echo $resource->name; ?><h5></th>
                                                                <td class="text-center">
                                                                    <input type="radio" class="icheck-checkbox"
                                                                        <?php echo (!is_null($answer_nature) && $answer_nature->answer =='true') ? 'checked':''; ?>
                                                                        name="natural_resources[<?php echo $resource->id ?>][answer]"
                                                                        value="true" required/>
                                                                </td>
                                                                <td class="text-center">
                                                                    <input type="radio" class="icheck-checkbox"
                                                                    <?php echo (!is_null($answer_nature) && $answer_nature->answer =='false') ? 'checked':''; ?>
                                                                        name="natural_resources[<?php echo $resource->id ?>][answer]"
                                                                        value="false" required/>
                                                                </td>
                                                                <td class="text-center">
                                                                    <input type="text" class="form-control"
                                                                        name="natural_resources[<?php echo $resource->id ?>][note]"
                                                                        value="<?php echo (!is_null($answer_nature)) ? $answer_nature->natural_resources_name:''; ?>">
                                                                    <input type="hidden" name="natural_resources[<?php echo $resource->id ?>][natural_resources_id]" value="<?php echo $resource->id ?>">
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 text-center">
                                    <!--<button type="submit" class="btn btn-lg btn-info">
                                        ดำเนินการต่อไป <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                                    </button>-->
                                    <div class="col-md-offset-3 col-md-9">
                                            <a href="<?php echo site_url('fontend/form/form_6')?>" class="btn btn-primary"><i class="fas fa-step-backward"></i>ย้อนกลับ</a>
                                            <button type="submit" id="btn-save-form-5" class="btn btn-success"> <i
                                            class=" fas fa-step-forward"></i>  บันทึกต่อไป</button>
                                            <a href="<?php echo site_url('fontend/home')?>" class="btn btn-info"><i class="  fas fa-home"></i>กลับหน้าแรก</a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- main card -->
            </div>
        </div>
    </div><!-- End Container fluid  -->
</div><!-- End Page wrapper  -->
