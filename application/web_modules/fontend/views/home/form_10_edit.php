<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">CRRU DATASET</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">เลขที่แบบสอบถาม
                            <?php echo $this->session->userdata('community_id') ?> </li>
                        <li class="breadcrumb-item active">บันทึกครั้งที่
                            <?php echo $this->session->userdata('house_information_recorded') ?> </li>
                        <li class="breadcrumb-item active">การรวมกลุ่ม/องค์กรในชุมชนที่ท่านมีส่วนร่วมหรือเป็นสมาชิก
                        </li>

                    </ol>
                </div>
            </div>
        </div>


        <!-- Row -->
        <div class="row">

            <div class="col-12">

                <form method="post" action="<?php echo site_url('index.php/fontend/save/form_10') ?>"
                    onsubmit="return save_check() " enctype="multipart/form-data">



                    <div class="card">
                        <div class="card-header bg-info">
                            <h4 class="m-b-0 text-white"> ฟอร์ม 10 : การรวมกลุ่มในชุมชน
                            </h4>
                        </div>
                        <div class="card-body">

                            <div class="form-body">



                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">ประเภทการรวมกลุ่ม</label>
                                            <div class="col-md-8">

                                                <?php echo form_dropdown('group_type_id', $group_type, '', ['class' => 'select2 form-control custom-select']); ?>

                                                <input type="hidden" name="house_registration_id"
                                                    value="<?php echo $this->session->userdata('house_registration_id'); ?>">
                                                <input type="hidden" name="house_information_recorded"
                                                    value="<?php echo $this->session->userdata('house_information_recorded'); ?>">

                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">รูปแบบการรวมกลุ่ม</label>
                                            <div class="col-md-8">

                                                <?php echo form_dropdown('group_format_id', $group_format, '', ['class' => 'select2 form-control custom-select']); ?>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="row">



                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">ชื่อกลุ่ม</label>
                                            <div class="col-md-8">
                                                <?php echo form_input('group_name', '', ['placeholder' => 'ชื่อกลุ่ม', 'class' => 'form-control']); ?>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label
                                                class="control-label text-left col-md-4">หน่วยงาน/องค์กรที่รับผิดชอบดำเนินกิจกรรม</label>
                                            <div class="col-md-8">
                                                <?php echo form_textarea('organization_of_care', '', ['placeholder' => 'หน่วยงาน หรือ องค์กรที่รับผิดชอบ ', 'class' => 'form-control']); ?>
                                            </div>

                                        </div>
                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">กิจกรรมของกลุ่ม</label>
                                            <div class="col-md-8">
                                                <?php echo form_textarea('activity', '', ['placeholder' => 'กิจกรรมที่ทำ หรือเกี่ยวข้อง', 'class' => 'form-control']); ?>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">ผลการดำเนินกิจกรรม</label>
                                            <div class="col-md-8">
                                                <?php echo form_textarea('results', '', ['placeholder' => 'ผลการดำเนินกิจกรรม / ประโยชน์', 'class' => 'form-control']); ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit"
                                                                class="btn btn-success">Submit</button>
                                                            <button type="button"
                                                                class="btn btn-inverse">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </from>

                            </div>


                        </div>
                    </div>
                    <!-- Row -->

            </div>





            <?php
$house_registration_id = $this->session->userdata('house_registration_id');
$house_information_recorded = $this->session->userdata('house_information_recorded');
$sql = "SELECT
tb_community_integration.created_at,
tb_community_integration.updated_at,
tb_community_integration.created_by,
tb_community_integration.updated_by,
tb_community_integration.record_note,
tb_community_integration.id,

tb_community_integration.group_name,

tb_community_integration.organization_of_care,
tb_community_integration.activity,
tb_community_integration.results,
tb_group_type.`name` AS group_type_name ,
tb_group_format.`name` AS group_format
FROM
tb_community_integration
LEFT JOIN tb_group_type ON tb_community_integration.group_type_id = tb_group_type.id
LEFT JOIN tb_group_format ON tb_community_integration.group_format_id = tb_group_format.id
 WHERE   tb_community_integration.house_registration_id = '$house_registration_id' AND tb_community_integration.house_information_recorded = '$house_information_recorded'";
$num_rows = $this->db->query($sql)->num_rows();

if ($num_rows > 0) {
    $recData = $this->db->query($sql)->result_array();

    ?>
            <!-- column -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">กลุ่มในชุมชน</h4>

                        <div class="table-responsive">
                            <table class="display nowrap table table-hover table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>ประเภทการรวมกลุ่ม</th>
                                        <th>ชื่อกลุ่ม</th>
                                        <th>รูปแบบการรวมกลุ่ม</th>
                                        <th class="text-nowrap">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($recData as $row): ?>
                                    <tr>
                                        <td><?php echo $row['group_name'] ?></td>
                                        <td><?php echo $row['group_type_name'] ?></td>
                                        <td><?php echo $row['group_format'] ?></td>
                                        <td class="text-nowrap">
                                            <a href="javascript:void(0)" data-toggle="tooltip"
                                                data-original-title="Edit">แก้ใข <i
                                                    class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="javascript:void(0)" data-toggle="tooltip"
                                                data-original-title="Close"> ลบ <i class="fa fa-close text-danger"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- column -->

            <?php
}
?>




            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- script -->




        <!-- End script -->