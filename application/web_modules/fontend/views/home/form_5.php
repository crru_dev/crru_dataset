<?php
$house_registration_id = $this->session->userdata('house_registration_id');
// $house_registration_id = 8;
?>
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Container fluid  -->
    <div class="container-fluid">

        <?php
        $community_id = $this->session->userdata('community_id');
        $house_information_recorded = $this->session->userdata('house_information_recorded');
        $breadcrumb = ['breadcrumb'=>[
            'หน้าหลัก',
            'เลขที่แบบสอบถาม '.$community_id,
            'บันทึกครั้งที่ '.$house_information_recorded,
            'ส่วนที่ 2 ข้อมูลด้านสุขภาพ'
            ]];
        $this->load->view('fontend/theme/breadcrumb', $breadcrumb);
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">ฟอร์ม 5 : ส่วนที่ 2 ข้อมูลด้านสุขภาพ</h4>
                    </div>
                    <div class="card-body">
                        <form name="form_5" action="<?php echo site_url('fontend/save/form_5'); ?>" method="post" id="form_5" name="form_5">
                            <!-- 2.1 ข้อมูลพฤติกรรมเสี่ยงสมาชิกในครัวเรือน -->
                            <div class="row">
                                <div class="col-12">
                                    <div class="card border">
                                        <div class="card-header bg-info">
                                            <h4 class="m-b-0 text-white">2.1 ข้อมูลพฤติกรรมเสี่ยงสมาชิกในครัวเรือน</h4>
                                        </div>
                                        <div class="card-body">

                                        <?php 
                                         $x=1;
                                        foreach ($house_member_list as $key_member => $member):
                                               
                                        ?>
                                            <h4><i class="fa fa-user" aria-hidden="true"></i> สมาชิก คนที่  <?php echo $x." : ". $member->f_name.'  '.$member->l_name; ?></h4>
                                            <div class="row p-3 checkbox-group">
                                                <div class="col-12 message text-warning" style="display:none;"></div>
                                            <?php
                                            $table = 'answer_individual_risk_behavior';
                                            $cond = [
                                                    'house_registration_id' => $house_registration_id,
                                                    'house_member_id'=>$member->house_member_id
                                                    ];
                                            $this->db->select('individual_risk_behavior_id');
                                            $answer_list = $this->db->get_where($table, $cond)->result();

                                            foreach ($irb_list as $key_irb => $irb):
                                                    $checked = '';
                                                    foreach ($answer_list as $key => $answer) {
                                                        if ($irb->id  === $answer ->individual_risk_behavior_id ) {
                                                            $checked = 'checked';
                                                            break;
                                                        }

                                                    }//END CHECKED
                                            ?>
                                                <div class="col-lg-4 pb-2">
                                                    <input type="checkbox" class="icheck-checkbox" <?php echo $checked; ?>
                                                        name="individual_risk_behavior[member][<?php echo $member->house_member_id; ?>][answer][]" value="<?php echo $irb->id; ?>">
                                                    <input type="hidden" name="individual_risk_behavior[individual_risk_behavior_list][<?php echo $key_irb+1 ?>]" value="<?php echo $irb->name; ?>">
                                                    <span style="font-size:16px;"><?php echo $irb->name; ?></span>
                                                </div>
                                            <?php endforeach; ?>
                                            </div>
                                            <hr>
                                        <?php 
                                            $x++;
                                            endforeach; 
                                        ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- 2.2 ข้อมูลภาวะเสี่ยงจากการประกอบอาชีพของสมาชิกครัวเรือน -->
                            <div class="row">
                                <div class="col-12">
                                    <div class="card border">
                                        <div class="card-header bg-info">
                                            <h4 class="m-b-0 text-white">2.2 ข้อมูลภาวะเสี่ยงจากการประกอบอาชีพของสมาชิกครัวเรือน</h4>
                                        </div>
                                        <div class="card-body">
                                        <?php
                                        $x=1;
                                        foreach ($house_member_list as $key_member => $member): ?>
                                            <h4><i class="fa fa-user" aria-hidden="true"></i> สมาชิก คนที่  <?php echo $x." : ". $member->f_name.'  '.$member->l_name; ?></h4>
                                            <div class="row p-3 checkbox-group">
                                                <div class="col-12 message text-warning" style="display:none;"></div>
                                            <?php
                                            $table = 'answer_work_risk_behavior';
                                            $cond = [
                                                    'house_registration_id' => $house_registration_id,
                                                    'house_member_id'=>$member->house_member_id
                                                    ];
                                            $this->db->select('work_risk_behavior_id');
                                            $answer_list = $this->db->get_where($table, $cond)->result();
                                            foreach ($wrb_list as $key_wrb => $wrb):
                                                $checked = '';
                                                foreach ($answer_list as $key => $answer) {
                                                    if ($wrb->id  === $answer ->work_risk_behavior_id ) {
                                                        $checked = 'checked';
                                                        break;
                                                    }

                                                }//END CHECKED
                                            ?>
                                                <div class="col-lg-4">
                                                    <input type="checkbox" class="icheck-checkbox" <?php echo $checked; ?>
                                                        name="work_risk_behavior[member][<?php echo $member->house_member_id; ?>][answer][]" value="<?php echo $wrb->id; ?>">
                                                    <input type="hidden" name="work_risk_behavior[work_risk_behavior_list][<?php echo $key_wrb+1 ?>]" value="<?php echo $wrb->name; ?>">
                                                    <span style="font-size:16px;"><?php echo $wrb->name; ?></span>
                                                </div>
                                            <?php endforeach; ?>
                                            </div>
                                            <hr>
                                        <?php 
                                    $x++;
                                    endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- 2.3 ข้อมูลผู้ที่ต้องการได้รับการช่วยเหลือดูแลในครัวเรือน -->
                            <!-- <div class="row">
                                <div class="col-12">
                                    <div class="card border">
                                        <div class="card-header bg-info">
                                            <h4 class="m-b-0 text-white">2.3 ข้อมูลผู้ที่ต้องการได้รับการช่วยเหลือดูแลในครัวเรือน </h4>
                                        </div>
                                        <div class="card-body">
                                        <?php 
                                        $x=1;
                                        foreach ($house_member_list as $key_member => $member): ?>
                                            <h4><i class="fa fa-user" aria-hidden="true"></i> สมาชิก คนที่  <?php echo $x . " : ".$member->f_name.'  '.$member->l_name; ?></h4>
                                            <div class="row p-3 checkbox-group">
                                                <div class="col-12 message text-warning" style="display:none;"></div>
                                            <?php
                                            $table = 'answer_need_help';
                                            $cond = [
                                                    'house_registration_id' => $house_registration_id,
                                                    'house_member_id'=>$member->house_member_id
                                                    ];
                                            $this->db->select('need_help_id');
                                            $answer_list = $this->db->get_where($table, $cond)->result();
                                            foreach ($nh_list as $key_nh => $nh):
                                                $checked = '';
                                                foreach ($answer_list as $key => $answer) {
                                                    if ($nh->id  === $answer ->need_help_id ) {
                                                        $checked = 'checked';
                                                        break;
                                                    }

                                                }//END CHECKED
                                            ?>
                                                <div class="col-lg-4">
                                                    <input type="checkbox" class="icheck-checkbox" <?php echo $checked; ?>
                                                        name="need_help[member][<?php echo $member->house_member_id; ?>][answer][]" value="<?php echo $nh->id; ?>">
                                                    <input type="hidden" name="need_help[need_help_list][<?php echo $key_nh+1 ?>]" value="<?php echo $nh->name; ?>">

                                                    <span style="font-size:16px;"><?php echo $nh->name; ?></span>
                                                </div>
                                            <?php endforeach; ?>
                                            </div>
                                            <hr>
                                        <?php
                                    $x++;
                                    endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <!-- 2.4 ข้อมูลภาวะฉุกเฉินที่ต้องได้รับการรักษา  -->
                            <!-- <div class="row">
                                <div class="col-12">
                                    <div class="card border">
                                        <div class="card-header bg-info">
                                            <h4 class="m-b-0 text-white">2.4 ข้อมูลภาวะฉุกเฉินที่ต้องได้รับการรักษา </h4>
                                        </div>
                                        <div class="card-body">
                                        <?php
                                        $x=1;
                                        foreach ($house_member_list as $key => $member): ?>
                                            <h4><i class="fa fa-user" aria-hidden="true"></i> สมาชิก คนที่  <?php echo  $x ." : ".$member->f_name.'  '.$member->l_name; ?></h4>
                                            <div class="row p-3 checkbox-group">
                                                <div class="col-12 message text-warning" style="display:none;"></div>
                                            <?php
                                            $table = 'answer_emergency_assistance';
                                            $cond = [
                                                    'house_registration_id' => $house_registration_id,
                                                    'house_member_id'=>$member->house_member_id
                                                    ];
                                            $this->db->select('emergency_assistance_id');
                                            $answer_list = $this->db->get_where($table, $cond)->result();

                                            foreach ($amas_list as $key_amas => $amas):
                                                $checked = '';
                                                foreach ($answer_list as $key => $answer) {
                                                    if ($amas->id  === $answer ->emergency_assistance_id ) {
                                                        $checked = 'checked';
                                                        break;
                                                    }

                                                }//END CHECKED
                                            ?>
                                                <div class="col-lg-4">
                                                    <input type="checkbox" class="icheck-checkbox" <?php echo $checked; ?>
                                                        name="emergency_assistance[member][<?php echo $member->house_member_id; ?>][answer][]" value="<?php echo $amas->id; ?>">
                                                    <input type="hidden" name="emergency_assistance[emergency_assistance_list][<?php echo $key_amas+1 ?>]" value="<?php echo $amas->name; ?>">

                                                    <span style="font-size:16px;"><?php echo $amas->name; ?></span>
                                                </div>
                                            <?php endforeach; ?>
                                            </div>
                                            <hr>
                                        <?php 
                                    $x++;
                                    endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div> -->


                            <!-- 2.5 ข้อมูลการเจ็บป่วยเรื้อรัง การดูแลรักษาเมื่อมีภาวะการณ์ป่วย  -->
                              <!-- 11-01-2565 -->
                                <!-- แก้ เก็บข้อมูลเป็บแบบภาพรวม และหัวข้อเป็น2.3 -->
                            <div class="row">
                                <div class="col-12">
                                    <div class="card border">
                                        <div class="card-header bg-info">
                                            <h4 class="m-b-0 text-white">2.3 ข้อมูลการเจ็บป่วยเรื้อรัง การดูแลรักษาเมื่อมีภาวการณ์เจ็บป่วย ปัญหาและความต้องการในการดูแลรักษาสุขภาพ และสภาวะการเจ็บป่วยในปัจจุบัน  (ตอบเป็นภาพรวมของครัวเรือน)</h4>
                                        </div>
                                        <div class="card-body">
                                        <?php
                
                                        //foreach ($house_member_list as $key => $member): ?> 
                                            <h4><i class="fa fa-home" aria-hidden="true"></i> ตอบเป็นภาพรวมครัวเรือน</h4>
                                            <div class="row">
                                                <div class="col-lg-12 pt-3 pl-3">
                                                    <h5>ข้อมูลการเจ็บป่วยเรื้อรัง</h5>
                                                    <small class="color-red chronic_illness_list"></small>
                                                </div>
                                            </div>
                                            <div class="row p-3 checkbox-group">
                                                <div class="col-12 message text-warning" style="display:none;"></div>

                                            <?php
                                            $table = 'answer_chronic_illness';
                                            $cond = [
                                                    'house_registration_id' => $house_registration_id
                                                    //'house_member_id'=>$member->house_member_id
                                                    ];
                                            $this->db->select('chronic_illness_id');
                                            $answer_list = $this->db->get_where($table, $cond)->result();

                                            foreach ($chronic_illness_list as $key_c => $chronic_illness):
                                                $checked = '';
                                                foreach ($answer_list as $key => $answer) {
                                                    if ($chronic_illness->id  === $answer ->chronic_illness_id ) {
                                                        $checked = 'checked';
                                                        break;
                                                    }

                                                }//END CHECKED
                                            ?>
                                                <div class="col-lg-4">
                                                    <input type="checkbox" class="icheck-checkbox" <?php echo $checked; ?>
                                                        name="chronic_illness[member][<?php echo $member->house_member_id; ?>][answer][]" value="<?php echo $chronic_illness->id; ?>">
                                                    <input type="hidden" name="chronic_illness[chronic_illness_list][<?php echo $key_c+1 ?>]" value="<?php echo $chronic_illness->name; ?>">
                                                    <span style="font-size:16px;"><?php echo $chronic_illness->name; ?></span>
                                                </div>
                                            <?php endforeach; ?>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12 pt-3 pl-3">
                                                    <h5>ข้อมูลผู้ให้การดูแล </h5>
                                                </div>
                                            </div>
                                            <div class="row p-3 checkbox-group">
                                                <div class="col-12 message text-warning" style="display:none;"></div>

                                            <?php
                                            $table = 'answer_caretaker';
                                            $cond = [
                                                    'house_registration_id' => $house_registration_id
                                                    // 'house_member_id'=>$member->house_member_id
                                                    ];
                                            $this->db->select('caretaker_id');
                                            $answer_list = $this->db->get_where($table, $cond)->result();

                                            foreach ($caretaker_list as $key_caretaker => $caretaker):

                                                $checked = '';
                                                foreach ($answer_list as $key => $answer) {
                                                    if ($caretaker->id  === $answer ->caretaker_id ) {
                                                        $checked = 'checked';
                                                        break;
                                                    }

                                                }//END CHECKED
                                            ?>
                                                <div class="col-lg-4">
                                                    <input type="checkbox" class="icheck-checkbox" <?php echo $checked; ?>
                                                      name="caretaker[member][<?php echo $member->house_member_id; ?>][answer][]" value="<?php echo $caretaker->id; ?>">
                                                    <input type="hidden" name="caretaker[caretaker_list][<?php echo $key_caretaker+1 ?>]" value="<?php echo $caretaker->name; ?>">

                                                    <span style="font-size:16px;"><?php echo $caretaker->name; ?></span>
                                                </div>
                                            <?php endforeach; ?>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12 pt-3 pl-3">
                                                    <h5>ข้อมูลแนวทางการดูแลรักษาเมื่อมีภาวะเจ็บป่วย </h5>
                                                </div>
                                            </div>
                                            <div class="row p-3 checkbox-group">
                                                <div class="col-12 message text-warning" style="display:none;"></div>
                                            <?php
                                            $table = 'answer_guidelines_for_care';
                                            $cond = [
                                                    'house_registration_id' => $house_registration_id
                                                    // 'house_member_id'=>$member->house_member_id
                                                    ];
                                            $this->db->select('guidelines_for_care_id');
                                            $answer_list = $this->db->get_where($table, $cond)->result();

                                            foreach ($guidelines_for_care_list as $key_guidelines_for_care => $guidelines_for_care):
                                                $checked = '';
                                                foreach ($answer_list as $key => $answer) {
                                                    if ($guidelines_for_care->id  === $answer ->guidelines_for_care_id ) {
                                                        $checked = 'checked';
                                                        break;
                                                    }

                                                }//END CHECKED
                                            ?>
                                                <div class="col-lg-4">
                                                    <input type="checkbox" class="icheck-checkbox" <?php echo $checked; ?>
                                                        name="guidelines_for_care[member][<?php echo $member->house_member_id; ?>][answer][]"
                                                        value="<?php echo $guidelines_for_care->id; ?>">
                                                    <input type="hidden" name="guidelines_for_care[guidelines_for_care_list][<?php echo $key_guidelines_for_care+1 ?>]"
                                                        value="<?php echo $guidelines_for_care->name; ?>">

                                                    <span style="font-size:16px;"><?php echo $guidelines_for_care->name; ?></span>
                                                </div>
                                            <?php endforeach; ?>
                                            </div>
                                            <hr>
                                         <?php 
                                     //$x++;
                                   // endforeach; ?> 
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- 2.6 ข้อมูลปัญหาและสภาวะจากการเจ็บป่วยเรื้อรังในปัจจุบัน -->
                            <!-- <div class="row">
                                <div class="col-12">
                                    <div class="card border">
                                        <div class="card-header bg-info">
                                            <h4 class="m-b-0 text-white">2.6 ข้อมูลปัญหาและสภาวะจากการเจ็บป่วยเรื้อรังในปัจจุบัน </h4>
                                        </div>
                                        <div class="card-body">
                                            <?php
                                            $x=1;
                                            foreach ($house_member_list as $key => $member):
                                                $table = 'answer_chronic_illness_problems';
                                                $cond = [
                                                        'house_registration_id' => $house_registration_id,
                                                        'house_member_id'=>$member->house_member_id
                                                        ];
                                                $cip = $this->db->get_where($table, $cond)->row();

                                            ?>
                                            <h5><i class="fa fa-user" aria-hidden="true"></i> สมาชิก คนที่  <?php echo $x . " : ". $member->f_name.'  '.$member->l_name; ?></h5>
                                            <div class="form-group">
                                                <textarea name="chronic_illness_problems[<?php echo $member->house_member_id; ?>]"
                                                     class="form-control" rows="8"><?php echo (!is_null($cip)) ? $cip->answer:''; ?></textarea>
                                            </div>
                                            <?php 
                                         $x++;
                                        endforeach; ?>

                                        </div>
                                    </div>
                                </div>
                            </div> -->


                            <!-- 2.7  ข้อมูลสิทธิและสวัสดิการในการรักษาพยาบาล -->
                              <!-- 11-01-2565 -->
                                <!-- แก้ เก็บข้อมูลเป็บแบบภาพรวม และหัวข้อเป็น2.4 -->
                                 <!-- 
                                     เปลี่ยนเป็นแบบเลือก
                                     เพิ่มตาราง
                                
                                -->
                            <div class="row">
                                <div class="col-12">
                                    <div class="card border">
                                        <div class="card-header bg-info">
                                            <h4 class="m-b-0 text-white">2.4 ข้อมูลสิทธิและสวัสดิการในการรักษาพยาบาล (มีสิทธิใดบ้าง และมีการใช้สิทธิอย่างไร) </h4>
                                        </div>
                                        <div class="card-body">
                                            
                                        <h5><i class="fa fa-home" aria-hidden="true"></i>ตอบเป็นภาพรวมครัวเรือน </h5>
                                            
                                        <div class="row p-3 checkbox-group">
                                                <div class="col-12 message text-warning" style="display:none;"></div>
                                            


                                        <?php
                                            $table = 'answer_welfare';
                                            $cond = [
                                                    'house_registration_id' => $house_registration_id
                                                    // 'house_member_id'=>$member->house_member_id
                                                    ];
                                            $this->db->select('welfare_type_id');
                                            $answer_list = $this->db->get_where($table, $cond)->result();

                                            foreach ($welfare_type_list as $key_welfare_type => $welfare_type):
                                                $checked = '';
                                                foreach ($answer_list as $key => $answer) {
                                                    if ($welfare_type->id  === $answer ->welfare_type_id ) {
                                                        $checked = 'checked';
                                                        break;
                                                    }

                                                }//END CHECKED
                                            ?>
                                                <div class="col-lg-4">
                                                    <input type="checkbox" class="icheck-checkbox" <?php echo $checked; ?>
                                                        name="welfare_type[member][<?php echo $member->house_member_id; ?>][answer][]"
                                                        value="<?php echo $welfare_type->id; ?>">
                                                    <input type="hidden" name="welfare_type[welfare_type_list][<?php echo $key_welfare_type+1 ?>]"
                                                        value="<?php echo $welfare_type->name; ?>">

                                                    <span style="font-size:16px;"><?php echo $welfare_type->name; ?></span>
                                                </div>
                                            <?php endforeach; ?>
                                            



<hr>


                                            <?php
                                            // $x=1;
                                            // foreach ($house_member_list as $key => $member):
                                                $table = 'answer_welfare';
                                                $cond = [
                                                        'house_registration_id' => $house_registration_id
                                                        // 'house_member_id'=>$member->house_member_id
                                                        ];
                                                $welfare = $this->db->get_where($table, $cond)->row();

                                            ?>
                                           
                                            <div class="form-group">

                                            

                                            การใช้สิทธิ์ของท่าน
                                                <textarea name="welfare_detail"
                                                     class="form-control" rows="8"><?php echo (!is_null($welfare)) ? $welfare->answer:''; ?></textarea>
                                            </div>
                                            <?php 
                                        // $x++;
                                        // endforeach; ?>





                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                
                                <a href="<?php echo site_url('fontend/form/form_4')?>" class="btn btn-primary"><i class="fas fa-step-backward"></i>ย้อนกลับ</a>
                                <button type="submit" id="btn-save-form-5" class="btn btn-success"> <i
                                class=" fas fa-step-forward"></i>  บันทึกต่อไป</button>
                                <a href="<?php echo site_url('fontend/home')?>" class="btn btn-info"><i class="  fas fa-home"></i>กลับหน้าแรก</a>
                                                        

                            </div>
                        </div>


                    </div>
                </div><!-- main card -->
            </div>
        </div>
    </div><!-- End Container fluid  -->
</div><!-- End Page wrapper  -->
<script type="text/javascript">
  $("#btn-save-form-5").click(function(e) { // เมื่อกดปุ่มบันทึกข้อมูล
        e.preventDefault();
        var validate = true;
        var c = 0;
       /* $.each($('.checkbox-group'), function(index, el) {
            var element_length = $(el).find('input[type=checkbox]:checked').length;
            var el_msg = $(el).find('.message');
            if(element_length == 0){
                el_msg.html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> โปรดเลือกคำตอบอย่างน้อย 1 คำตอบ');
                el_msg.fadeIn('500');
                c++;
                if(c == 1){
                    $('html, body').animate({
                        scrollTop: $(el).offset().top -150
                    }, 1000)
                }


            } else {
                el_msg.fadeOut('500');
            }

        });//END EACH*/
        if(c == 0){
            $('#btn-save-form-5').button('loading');
            $('#form_5').submit();
        }

    });

</script>
