<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Container fluid  -->
    <div class="container-fluid">
    <?php
        $community_id = $this->session->userdata('community_id');
        $house_information_recorded = $this->session->userdata('house_information_recorded');
        $breadcrumb = ['breadcrumb'=>[
            'หน้าหลัก',
            'รายการข้อมูลชุมชน '
            ]];
        $this->load->view('fontend/theme/breadcrumb', $breadcrumb); ?>
    </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-info">
                    <h4 class="mb-0 text-white"><i class=" fas fa-address-card"></i> รายการข้อมูลชุมชน</h4>
                    </div>
                    <div class="card-body">
                        <form>

                        <div class="row">
                            <div class="col-md-5 ">
                                <div class="form-group">
                                    <label>รหัสแบบสอบถาม, ชื่อหมู่บ้าน, บ้านเลขที่, รหัสประจำบ้าน, หมู่ที่, </label>
                                    <?php echo form_input('key', $this->input->get('key'), ['placeholder' => 'ชื่อหมู่บ้าน', 'class' => 'form-control']); ?>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">จังหวัด</label>
                                    <select class="select2 form-control custom-select" data-placeholder="Select" id="provinces" name="province_id"  onchange="get_amphure(this.value)">>
                                        <option value="">ทั้งหมด</option>
                                        <?php
                                        foreach ($provices as $provice) {
                                             $selected =  $this->input->get('province_id') == $provice['province_id']  ?  'selected' : '';
                                        ?>
                                            <option value="<?php echo $provice['province_id']; ?>" <?php echo $selected;?>>
                                                <?php echo $provice['province_name']; ?></option>
                                        <?php
                                        }
                                        ?>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="amphure" id="sss" class="control-label">อำเภอ</label>

                                    <select name="amphur_id" id="amphures" class="select2 form-control custom-select" onchange="get_district(this.value)"> >
                                        <option value="">ทั้งหมด</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="district" class="control-label">ตำบล</label>
                                    <select name="district_id" id="district" class="select2 form-control custom-select">
                                        <option value="">ทั้งหมด</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 row">
                                <div class="form-group">
                                <button type="submit" class="btn btn-md btn-info">
                                    <span class=" fas fa-search"></span>
                                    ค้นหา
                                </button>
                                <button type="reset" class="btn btn-md btn-primary">
                                    <span class="  fas fa-undo-alt"></span>
                                    ยกเลิก
                                </button>
                                </div>
                            </div>
                            </hr>
                        </form>
                        <div class="table-responsive m-t-40">
                            <table class="table table-bordered table-striped table-responsive table-hover">
                                <thead>

                                    <tr class="table-success">
                                        <th class="text-center" width="10%">จัดการ</th>
                                        <th class="text-center">รหัสแบบสอบถาม</th>
                                        <th class="text-center">รหัสประจำบ้าน</th>
                                        <th class="text-center">จำนวนสมาชิก</th>
                                        <th class="text-center">วันที่เก็บข้อมูล</th>
                                        <th class="text-center">ที่อยู่</th>
                                    </tr>

                                </thead>
                                <tbody>

                                    <?php
                                    if(empty($recData))
                                    {
                                        ?>
                                        <tr class="bg-danger text-white">
                                            <td colspan="7">
                                            ไม่พบข้อมูล !!!
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    foreach ($recData as $row) :
                                    ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <button class="btn btn-warning" onclick="get_edit_list(<?php echo $row['house_registration_id']; ?>)">
                                                        <span class="ti-pencil-alt"></span>
                                                    </button>
                                                    <button type="button" onclick = "delete_house_registration(<?php echo $row['house_registration_id']; ?>)" class="btn btn-danger" >
                                                        <span class="ti-trash"></span>
                                                    </button>
                                                </div>

                                            </td>
                                            <td><?php echo $row['community_id'] ?></td>
                                            <td><?php echo $row['house_code'] ?></td>
                                            <td><?php echo $row['family_member'] ?></td>
                                            <td><?php echo DateThai($row['created_at']) ?></td>
                                            <td>
                                                <?php
                                                if ($row['house_number'] == "") :
                                                    echo "บ้านเลที่ข้างเคียง " . $row['house_number_nearby'] . " ชื่อหมู่บ้าน/ชื่อชุมชน " . $row['village_name'] . " / " . $row['community_name'] . " ตำบล " . $row['districts_name'] . " อำเภอ " . $row['amphures_name'] . " จังหวัด " . $row['province_name'];
                                                else :
                                                    echo "บ้านเลที่ " . $row['house_number'] . " ชื่อหมู่บ้าน/ชื่อชุมชน " . $row['village_name'] . " / " . $row['community_name'] . " ตำบล " . $row['districts_name'] . " อำเภอ " . $row['amphures_name'] . " จังหวัด " . $row['province_name'];
                                                endif;
                                                ?>
                                            </td>
                                        </tr>
                                    <?php
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>

                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <?php echo $recData = $this->pagination->create_links(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div><!-- End Container fluid  -->
</div><!-- End Page wrapper  -->
<div id="modal_edit_list" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" id="modal_body_edit_list">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" name="button" data-dismiss="modal" >
                    <i class="fas fa-times-circle" aria-hidden="true"></i> ปิด
                </button>
            </div>
        </div>

    </div>
</div>
<script>
<?php
if($this->input->get('amphur_id')){
    ?>
    get_district('<?php echo $this->input->get('amphur_id')?>');
    <?php
}


?>
$("#provinces").val('<?php echo $this->input->get('province_id')?>').trigger('change');
 setTimeout(function(){

    $("#amphures").val('<?php echo $this->input->get('amphur_id')?>');
    $("#district").val('<?php echo $this->input->get('district_id')?>').trigger('change');
}, '1000');
function get_amphure(id_province) {

$.ajax({
    type: "POST",
    url: "<?php echo site_url('index.php/fontend/form/get_province') ?>",
    data: {
        id: id_province,
        function: 'provinces'
    },
    success: function(data) {
        $('#amphures').html(data);
        $('#districts').html('');
    }
});
}

function get_district(id_amphures) {
$.ajax({
    type: "POST",
    url: "<?php echo site_url('index.php/fontend/form/get_province') ?>",
    data: {
        id: id_amphures,
        function: 'amphures'
    },
    success: function(data) {
        $('#district').html(data);
    }
});
}

function delete_house_registration(house_registration_id){

    if (confirm("ยืนยันการลบข้อมูล!") == false) {

        return false;
    }
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('index.php/fontend/save/delete_house_registration') ?>",
        data: {
            house_registration_id: house_registration_id 
        },
        success: function(data) {
            setTimeout(() => {

                window.location.reload();
            }, 1000);
            
        }
    });
}
</script>
