
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Container fluid  -->
    <div class="container-fluid">
        <?php
        $community_id = $this->session->userdata('community_id');
        $house_information_recorded = $this->session->userdata('house_information_recorded');
        $breadcrumb = ['breadcrumb'=>[
            'หน้าหลัก',
            'เลขที่แบบสอบถาม '.$community_id,
            'บันทึกครั้งที่ '.$house_information_recorded,
            'การป้องกันเชื้อไวรัสโคโรนา-2019'
            ]];
        $this->load->view('fontend/theme/breadcrumb', $breadcrumb); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">ข้อมูลพฤติกรรมการป้องกันโรคติดเชื้อไวรัสโคโรนา-2019 ในครอบครัว (ตอบได้มากกว่า 1 ข้อ)</h4>
                    </div>
                    <div class="card-body">

                        <form name="form_5" action="<?php echo site_url('fontend/save/form_covid'); ?>" method="post" id="form_5" name="form_5">
                            <!-- 4.1 มีการเข้าร่วมกิจกรรมด้านการจัดการชุมชนและด้านการเมืองการปกครอง ดังต่อไปนี้ โปรดระบุเครื่องหมาย √ ในช่องว่าง-->
                            <div class="row">
                                <div class="col-12">
                                    <h4>
                                        มีการเข้าร่วมกิจกรรมด้านการจัดการชุมชนและด้านการเมืองการปกครอง ดังต่อไปนี้ โปรดระบุเครื่องหมาย √ ในช่องว่าง
                                    </h4>
                                    <table class="table table-bordered table-hover">
                                        <tbody>
                                        <?php foreach ($q_covid_defense as $keypinfo => $pinfo):
                                            $checked = '';
                                            foreach ($answer_list as $key => $answer) {
                                                if($pinfo->id == $answer->covid_defense_id) {
                                                    $checked = 'checked';
                                                    break;
                                                }
                                            }
                                        ?>
                                            <tr>
                                                <th width="50"><?php echo $keypinfo+1; ?></th>
                                                <td>
                                                    <input type="checkbox" class="icheck-checkbox" <?php echo $checked; ?>
                                                        name="covid_defense[answer][]" value="<?php echo $pinfo->id; ?>">
                                                    <input type="hidden" name="covid_defense[covid_defense_name][<?php echo $keypinfo+1; ?>]" value="<?php echo $pinfo->name; ?>">
                                                    <span style="font-size:16px;"><?php echo $pinfo->name; ?></span>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 text-center">
                                    <a href="<?php echo site_url('fontend/form/form_9')?>" class="btn btn-primary"><i class="fas fa-step-backward"></i>ย้อนกลับ</a>
                                    <button type="submit" id="btn-save-form-5" class="btn btn-success"> <i
                                    class=" fas fa-step-forward"></i>  บันทึกต่อไป</button>
                                    
                                    <a href="<?php echo site_url('fontend/home')?>" class="btn btn-info"><i class="  fas fa-home"></i>กลับหน้าแรก</a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- main card -->
            </div>
        </div>
    </div><!-- End Container fluid  -->e4aa6g
</div><!-- End Page wrapper  -->
