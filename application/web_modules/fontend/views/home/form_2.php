<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">CRRU DATASET</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">เลขที่แบบสอบถาม
                            <?php echo $this->session->userdata('community_id') ?></li>
                            <li class="breadcrumb-item active">ครั้งที่บันทึก


                    </ol>

                </div>
            </div>
        </div>

        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white"> ฟอร์ม 2 ครั้งที่บันทึก</h4>
                    </div>
                    <div class="card-body">
                        <form method="post" action="<?php echo site_url('index.php/fontend/save/form_2') ?>"
                            onsubmit="return save_check() " enctype="multipart/form-data">
                            <div class="form-body">

                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">ครั้งที่บันทึก</label>
                                            <div class="col-md-9">
                                                <?php
                                                $options = array(
                                                    '1' => '1',
                                                 

                                                );

                                            echo form_dropdown('house_information_recorded', $options, $row['house_information_recorded'], ['class' => 'select2 form-control custom-select']);?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">วันที่สอบถาม</label>
                                            <div class="col-md-9">
                                                <input id="house_information_record_date"
                                                    name="house_information_record_date" type="date"
                                                    class="form-control" placeholder="dd/mm/yyyy" value="<?php if($row['house_information_record_date']!=""){echo date("Y-m-d", strtotime($row['house_information_record_date'])); }else{ echo date("Y-m-d");}?>">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">ผู้ให้ข้อมูล</label>
                                            <div class="col-md-9">
                                                <?php echo form_input('house_information_recorder', $row['house_information_recorder'], ['placeholder' => 'ชื่อ-สกุล', 'class' => 'form-control', 'required' => 'required', 'id' => 'house_information_recorder']); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label
                                                class="control-label text-right col-md-3">เบอร์โทรผู้ให้ข้อมูล</label>
                                            <div class="col-md-9">
                                                <?php echo form_input('house_information_recorder_tel', $row['house_information_recorder_tel'], ['placeholder' => 'เบอร์โทร', 'class' => 'form-control', 'required' => 'required', 'id' => 'house_information_recorder_tel']); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label
                                                class="control-label text-right col-md-3">ชื่อผู้กรอกข้อมูล</label>
                                            <div class="col-md-9">
                                                <?php echo form_input('recorder_name', $row['recorder_name'], ['placeholder' => 'ชื่อผู้กรอกข้อมูล', 'class' => 'form-control', 'required' => 'required', 'id' => 'recorder_name']); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                             <a href="<?php echo site_url('fontend/form/form_1_edit')?>" class="btn btn-primary"><i class="fas fa-step-backward"></i>ย้อนกลับ</a>
                                                <button type="submit" id="btn-save-form-1" class="btn btn-success"> <i
                                                class=" fas fa-step-forward"></i>  บันทึกต่อไป</button>
                                                <a href="<?php echo site_url('fontend/home')?>" class="btn btn-info"><i class="  fas fa-home"></i>กลับหน้าแรก</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6"> </div>
                                </div>
                            </div>
                            </from>
                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- script -->

    <script>
    function save_check() { // เมื่อกดปุ่มบันทึกข้อมูล
        var house_information_record_date = $("#house_information_record_date").val();
      //  var house_information_recorder = $("#house_information_recorder").val();
        var house_information_recorder_tel = $("#house_information_recorder_tel").val();


        if (house_information_record_date == "" || house_information_recorder_tel ==
            "") {
            swal("แจ้งเตือน", "กรอกข้อมูลให้ครบ", "error");
            return false;
        }

    }
    </script>


    <!-- End script -->
