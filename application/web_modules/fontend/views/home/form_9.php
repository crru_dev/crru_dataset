<?php
$house_registration_id = $this->session->userdata('house_registration_id');
// $house_registration_id = 8;
?>
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Container fluid  -->
    <div class="container-fluid">
        <?php
        $community_id = $this->session->userdata('community_id');
        $house_information_recorded = $this->session->userdata('house_information_recorded');
        $breadcrumb = ['breadcrumb'=>[
            'หน้าหลัก',
            'เลขที่แบบสอบถาม '.$community_id,
            'บันทึกครั้งที่ '.$house_information_recorded,
            'ส่วนที่ 6 ข้อมูลด้านสังคม วัฒนธรรม และการรวมกลุ่ม'
            ]];
        $this->load->view('fontend/theme/breadcrumb', $breadcrumb); ?>
        <div class="row ">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">ฟอร์ม 9 : ส่วนที่ 6 ข้อมูลด้านสังคม วัฒนธรรม และการรวมกลุ่ม</h4>
                    </div>
                    <div class="card-body">
                        <form name="form_5" action="<?php echo site_url('fontend/save/form_9'); ?>" method="post" id="form_5" name="form_5">
                            <!-- โปรดรุบุกิจกรรมทางบุญ ประเพณี ความเชื่อที่สำคัญ ที่ท่านมีส่วนร่วม-->
                            <div class="row pb-3">
                                <div class="col-12">
                                    <h4>
                                        โปรดระบุกิจกรรมทางบุญ ประเพณี ความเชื่อที่สำคัญ ที่ท่านมีส่วนร่วม
                                    </h4>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr class="bg-secondary">
                                                <th class="text-center" width="200"> ช่วงเวลา </th>
                                                <th class="text-center"> กิจกรรมทางบุญ ประเพณี ความเชื่อ </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($month_list as $key => $month): ?>
                                            <tr>
                                                <th width="50"><?php echo $month->name ?></th>
                                                <td>
                                                    <?php
                                                    $table = 'answer_monthly_activity';
                                                    $cond = ['house_registration_id'=>$house_registration_id, 'month_id'=>$month->id];
                                                    $answer_monthly = $this->db->get_where($table, $cond)->row();

                                                    ?>
                                                    <input type="text" class="form-control" name="activity[<?php echo $month->id ?>][activity]"
                                                        value="<?php echo (!is_null($answer_monthly)) ? $answer_monthly->activity:''; ?>">
                                                    <input type="hidden" name="activity[<?php echo $month->id ?>][month_name]" value="<?php echo $month->name; ?>">
                                                    <input type="hidden" name="activity[<?php echo $month->id ?>][month_id]" value="<?php echo $month->id; ?>">
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <?php
                                $cond = ['house_registration_id'=>$house_registration_id];
                                $table = 'answer_family_knowledge';
                                $knowledge = $this->db->get_where($table, $cond)->row();
                                ?>
                                <div class="col-12 pb-3">
                                    <h4>
                                        ภูมิปัญญาที่สำคัญ ที่ท่านได้รับการถ่ายทอดหรือสืบทอดในครอบครัว เครือญาติ และชุมชน
                                    </h4>
                                    <div class="pl-3">
                                        <input type="radio" class="icheck-checkbox" <?php echo (!is_null($knowledge) && $knowledge->choice == 'true') ? 'checked':''; ?>
                                            name="knowledge" value="true" required> <span class="mr-3" style="font-size:16px;"> มี</span>
                                        <input type="radio" class="icheck-checkbox" <?php echo (!is_null($knowledge) && $knowledge->choice == 'false') ? 'checked':''; ?>
                                            name="knowledge" value="false" required> <span style="font-size:16px;"> ไม่มี</span>
                                    </div>
                                </div>

                                <div class="col-12 pb-3 pl-4">
                                    <h5>ลักษณะการถ่ายทอดหรือสืบทอด</h5>
                                    <textarea class="form-control" name="knowledge_text"
                                     rows="8" cols="80"><?php echo (!is_null($knowledge)) ? $knowledge->answer:''; ?></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <!--<button type="submit" class="btn btn-lg btn-info">
                                        ดำเนินการต่อไป <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                                    </button>-->
                                    <a href="<?php echo site_url('fontend/form/form_8')?>" class="btn btn-primary"><i class="fas fa-step-backward"></i>ย้อนกลับ</a>
                                        <button type="submit" id="btn-save-form-5" class="btn btn-success"> <i
                                        class=" fas fa-step-forward"></i>  บันทึกต่อไป</button>
                                        <a href="<?php echo site_url('fontend/home')?>" class="btn btn-info"><i class="  fas fa-home"></i>กลับหน้าแรก</a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- main card -->
            </div>
        </div>
    </div><!-- End Container fluid  -->
</div><!-- End Page wrapper  -->
