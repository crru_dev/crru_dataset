<h4><?php
echo "บ้านเลที่ " . $house_info['house_number'] . " ชื่อหมู่บ้าน/ชื่อชุมชน " . $house_info['village_name'] ;?>
</h4>
<table class="table table-hover table-bordered">
    <tbody>
        <tr>
            <td>ส่วนที่ 1 ข้อมูลบ้าน</td>
            <td>
                <a class="btn btn-warning" href="<?php echo site_url('fontend/form/form_1_edit'); ?>">
                    <i class="ti-pencil-alt" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -ครั้งที่บันทึก</td>
            <td>
                <a class="btn btn-warning" href="<?php echo site_url('fontend/form/form_2'); ?>">
                    <i class="ti-pencil-alt" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -ข้อมูลสมาชิกในครัวเรือน</td>
            <td>
                <a class="btn btn-warning" href="<?php echo site_url('fontend/form/form_3'); ?>">
                    <i class="ti-pencil-alt" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -ข้อมูลการประกอบอาชีพของสมาชิกในครัวเรือน</td>
            <td>
                <a class="btn btn-warning" href="<?php echo site_url('fontend/form/form_4'); ?>">
                    <i class="ti-pencil-alt" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>ส่วนที่ 2 ข้อมูลด้านสุขภาพ</td>
            <td>
                <a class="btn btn-warning" href="<?php echo site_url('fontend/form/form_5'); ?>">
                    <i class="ti-pencil-alt" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>ส่วนที่ 3 รายรับรายจ่ายครัวเรือน</td>
            <td>
                <a class="btn btn-warning" href="<?php echo site_url('fontend/form/form_6'); ?>">
                    <i class="ti-pencil-alt" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>ส่วนที่ 4 สิ่งแวดล้อมและธรรมชาติ</td>
            <td>
                <a class="btn btn-warning" href="<?php echo site_url('fontend/form/form_7'); ?>">
                    <i class="ti-pencil-alt" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>ส่วนที่ 5 ข้อมูลด้านการเมืองการปกครอง</td>
            <td>
                <a class="btn btn-warning" href="<?php echo site_url('fontend/form/form_8'); ?>">
                    <i class="ti-pencil-alt" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>ส่วนที่ 6 ข้อมูลด้านสังคม วัฒนธรรม และการรวมกลุ่ม</td>
            <td>
                <a class="btn btn-warning" href="<?php echo site_url('fontend/form/form_9'); ?>">
                    <i class="ti-pencil-alt" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - การรวมกลุ่ม/องค์กรในชุมชนที่ท่านมีส่วนร่วมหรือเป็นสมาชิก</td>
            <td>
                <a class="btn btn-warning" href="<?php echo site_url('fontend/form/form_10'); ?>">
                    <i class="ti-pencil-alt" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    </tbody>

</table>
