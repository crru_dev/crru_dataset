<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <?php
        $community_id = $this->session->userdata('community_id');
        $house_information_recorded = $this->session->userdata('house_information_recorded');
        $breadcrumb = ['breadcrumb'=>[
            'หน้าหลัก',
            'เลขที่แบบสอบถาม '.$community_id,
            'บันทึกครั้งที่ '.$house_information_recorded,
            'การรวมกลุ่ม/องค์กรในชุมชนที่ท่านมีส่วนร่วมหรือเป็นสมาชิก'
            ]];
        $this->load->view('fontend/theme/breadcrumb', $breadcrumb); ?>

        <!-- Row -->
        <div class="row">
            <div class="col-12">
                <form method="post" action="<?php echo site_url('fontend/save/form_10') ?>"  enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-header bg-info">
                            <h4 class="m-b-0 text-white">ฟอร์ม 10 : การรวมกลุ่มในชุมชน
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">ประเภทการรวมกลุ่ม</label>
                                            <div class="col-md-8">
                                                <?php echo form_dropdown('group_type_id', $group_type, '', ['class' => 'select2 form-control custom-select']); ?>
                                                <input type="hidden" name="house_registration_id" value="<?php echo $this->session->userdata('house_registration_id'); ?>">
                                                <input type="hidden" name="house_information_recorded" value="<?php echo $this->session->userdata('house_information_recorded'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">รูปแบบการรวมกลุ่ม</label>
                                            <div class="col-md-8">
                                                <?php echo form_dropdown('group_format_id', $group_format, '', ['class' => 'select2 form-control custom-select']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">ชื่อกลุ่ม</label>
                                            <div class="col-md-8">
                                                <?php echo form_input('group_name', '', ['placeholder' => 'ชื่อกลุ่ม', 'class' => 'form-control', 'required' => 'required']); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">หน่วยงาน/องค์กรที่รับผิดชอบดำเนินกิจกรรม</label>
                                            <div class="col-md-8">
                                                <?php echo form_textarea('organization_of_care', '', ['placeholder' => 'หน่วยงาน หรือ องค์กรที่รับผิดชอบ ', 'class' => 'form-control', 'required' => 'required']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group   row">
                                            <label class="control-label text-left col-md-4">กิจกรรมของกลุ่ม</label>
                                            <div class="col-md-8">
                                                <?php echo form_textarea('activity', '', ['placeholder' => 'กิจกรรมที่ทำ หรือเกี่ยวข้อง', 'class' => 'form-control', 'required' => 'required']); ?>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">ผลการดำเนินกิจกรรม</label>
                                            <div class="col-md-8">
                                                <?php echo form_textarea('results', '', ['placeholder' => 'ผลการดำเนินกิจกรรม / ประโยชน์', 'class' => 'form-control', 'required' => 'required']); ?>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">ข้อเสนอแนะเพิ่มเติม</label>
                                            <div class="col-md-8">
                                                <?php echo form_textarea('record_note', '', ['placeholder' => 'ข้อเสนอแนะเพิ่มเติม', 'class' => 'form-control']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"><i class="fas fa-plus "></i> add</button>
                                                            <button type="button" class="btn btn-inverse">Cancel</button>
                                                            <a class="btn btn-info btn-lg" href="<?php echo site_url('home'); ?>">
                                    <i class="fas fa-check-circle" aria-hidden="true"></i> กรณีไม่มีกิจกรรมในชุมชน  จบการทำงาน
                                </a>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </from>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
            </div>

            <?php
            $house_registration_id = $this->session->userdata('house_registration_id');
            $house_information_recorded = $this->session->userdata('house_information_recorded');
            $sql = "SELECT
                tb_community_integration.created_at,
                tb_community_integration.updated_at,
                tb_community_integration.created_by,
                tb_community_integration.updated_by,
                tb_community_integration.record_note,
                tb_community_integration.id,

                tb_community_integration.group_name,

                tb_community_integration.organization_of_care,
                tb_community_integration.activity,
                tb_community_integration.results,
                tb_group_type.`name` AS group_type_name ,
                tb_group_format.`name` AS group_format
                FROM
                tb_community_integration
                LEFT JOIN tb_group_type ON tb_community_integration.group_type_id = tb_group_type.id
                LEFT JOIN tb_group_format ON tb_community_integration.group_format_id = tb_group_format.id
                WHERE   tb_community_integration.house_registration_id = '$house_registration_id' AND tb_community_integration.house_information_recorded = '$house_information_recorded'";
            $num_rows = $this->db->query($sql)->num_rows();

            if ($num_rows > 0) {
                $recData = $this->db->query($sql)->result_array();
            ?>
                <!-- column -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">กลุ่มในชุมชน</h4>

                            <div class="table-responsive">
                                <table class="display nowrap table table-hover table-striped table-bordered">
                                    <thead>
                                        <tr class="table-success">
                                            <th>ประเภทการรวมกลุ่ม</th>
                                            <th>ชื่อกลุ่ม</th>
                                            <th>รูปแบบการรวมกลุ่ม</th>
                                            <th>หน่วยงาน/องค์กรที่รับผิดชอบดำเนินกิจกรรม</th>
                                            <th>กิจกรรมของกลุ่ม</th>
                                            <th>ผลการดำเนินกิจกรรม</th>
                                            <th>ข้อเสนอแนะเพิ่มเติม</th>
                                            <th class="text-nowrap" width="5%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($recData as $row) : ?>
                                            <tr>
                                                <td><?php echo $row['group_type_name'] ?></td>
                                                <td><?php echo $row['group_name'] ?></td>
                                                <td><?php echo $row['group_format'] ?></td>
                                                <td><?php echo $row['organization_of_care'] ?></td>
                                                <td><?php echo $row['activity'] ?></td>
                                                <td><?php echo $row['results'] ?></td>
                                                <td><?php echo $row['record_note'] ?></td>
                                                <td class="text-nowrap">
                                                <button type="button" href="<?php echo site_url('fontend/save/delete_form_10/'.$row['id']) ?>" class="btn btn-sm btn-danger btn-delete" >
                                                    <span class="ti-trash"></span>
                                                </button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>

                                    </tbody>
                                </table>
                            </div>

                            <div class="text-center">
                                <a class="btn btn-info btn-lg" href="<?php echo site_url('home'); ?>">
                                    <i class="fas fa-check-circle" aria-hidden="true"></i> เสร็จสิ้น
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- column -->
            <?php
            }
            ?>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
