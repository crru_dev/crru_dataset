<?php 

$house_registration_id = $this->session->userdata('house_registration_id');



/*echo "<pre>";
print_r($row);
*/?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">CRRU DATASET</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">เลขที่แบบสอบถาม
                            <?php echo $this->session->userdata('community_id') ?> </li>
                        <li class="breadcrumb-item active">บันทึกครั้งที่
                            <?php echo $this->session->userdata('house_information_recorded') ?> </li>
                        <li class="breadcrumb-item active">ข้อมูลด้านเศรษฐกิจ
                        </li>
                        <li class="breadcrumb-item active">รายรับรายจ่ายครัวเรือน
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        

        <!-- Row -->
        <div class="row">

            <div class="col-12">

                <form method="post" action="<?php echo site_url('index.php/fontend/save/form_6') ?>"
                    onsubmit="return save_check() " enctype="multipart/form-data">

                    <div class="card">
                        <div class="card-header bg-info">
                            <h4 class="m-b-0 text-white">ฟอร์ม 6 : รายรับครัวเรือน
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group  row">
                                            <label
                                                class="control-label text-left col-md-3">รายได้ต่อเดือนโดยประมาณ</label>
                                            <div class="col-md-4">
                                                <div class="input-group mb-3">

                                                    <input type="number" name="monthly_income" class=""
                                                        value="<?php echo $house_information['monthly_income']; ?>" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2">บาท</span>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row pb-3">
                                            <div class="col-12">
                                                <h4>ประเภทของรายได้หลักของครัวเรือน กรุณาเรียงลำดับ 1-3
                                                    (1=รายรับส่วนที่มากที่สุด)</h4>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group  row">
                                            <strong
                                                class="control-label text-left col-md-4">ประเภทรายรับหลักของครัวเรือน</strong>
                                            <div class="col-md-8">
                                                <?php
                                                    foreach ($income_type_data as $row_income_type) :
                                                ?>
                                                <div class="form-group  row">
                                                    <div class="col-md-4">
                                                        <?php echo $row_income_type['name']; ?>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="hidden"
                                                            name="income[<?php echo $row_income_type['id']; ?>][income_type_id]"
                                                            value="<?php echo $row_income_type['id']; ?>">
                                                        <?php
                                                         $sql ="SELECT * FROM answer_income WHERE house_registration_id = '$house_registration_id' AND income_type_id = '$row_income_type[id]' ";
                                                         $q = $this->db->query($sql);
                                                         $row = $q->row_array();
                                                 
                                                         if ($q->num_rows() > 0) {
                                                             $r = $q->row_array();
                                                             //มีการบันทึก
                                                             ?>
                                                        <select class="custom-select"
                                                            name="income[<?php echo $row_income_type['id']; ?>][income_amont]">
                                                            <option selected>เลือกลำดับ</option>
                                                            <option value="1"
                                                                <?php if ($r['income_amont']== "1"){ echo "selected";} ?>>
                                                                1</option>
                                                            <option value="2"
                                                                <?php if ($r['income_amont']== "2"){ echo "selected";} ?>>
                                                                2</option>
                                                            <option value="3"
                                                                <?php if ($r['income_amont']== "3"){ echo "selected";} ?>>
                                                                3</option>
                                                        </select>

                                                        <?php
                                                         }else{
                                                             ?>
                                                        <select class="custom-select"
                                                            name="income[<?php echo $row_income_type['id']; ?>][income_amont]">
                                                            <option selected>เลือกลำดับ</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                        </select>

                                                        <?php
                                                         }
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php
                                                    endforeach;
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row pb-3">
                                            <div class="col-12">
                                                <h4>ค่าใช้จ่ายต่อเดือน ส่วนใหญ่เป็นการใช้จ่ายประเภทใด กรุณาเรียงลำดับ
                                                    1-3 (1=รายจ่ายส่วนที่มากที่สุด)</h4>
                                            </div>
                                        </div>
                                        <div class="form-group  row">
                                            <strong
                                                class="control-label text-left col-md-4">ประเภทรายจ่ายของครัวเรือน</strong>
                                            <div class="col-md-8">
                                                <?php
                                                foreach ($cost_type_data as $row_cost_type) {
                                                ?>
                                                <div class="form-group  row">
                                                    <div class="col-md-4">
                                                        <?php echo $row_cost_type['name']; ?>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="hidden"
                                                            name="cost[<?php echo $row_cost_type['id']; ?>][cost_type_id]"
                                                            value="<?php echo $row_cost_type['id']; ?>">

                                                        <?php
                                                         $sql ="SELECT * FROM answer_cost WHERE house_registration_id = '$house_registration_id' AND cost_type_id = '$row_cost_type[id]' ";
                                                        // echo $sql ."<hr>";
                                                         $q = $this->db->query($sql);
                                                         $row = $q->row_array();
                                                 
                                                         if ($q->num_rows() > 0) {
                                                             $r = $q->row_array();
                                                             ?>

                                                        <select class="custom-select"
                                                            name="cost[<?php echo $row_cost_type['id']; ?>][cost_amont]">
                                                            <option selected>เลือกลำดับ</option>
                                                            <option value="1"
                                                                <?php if ($r['cost_amont']== "1"){ echo "selected";} ?>>
                                                                1</option>
                                                            <option value="2"
                                                                <?php if ($r['cost_amont']== "2"){ echo "selected";} ?>>
                                                                2</option>
                                                            <option value="3"
                                                                <?php if ($r['cost_amont']== "3"){ echo "selected";} ?>>
                                                                3</option>
                                                        </select>

                                                        <?php
                                                         }else{
                                                             ?>

                                                        <select class="custom-select"
                                                            name="cost[<?php echo $row_cost_type['id']; ?>][cost_amont]">
                                                            <option selected>เลือกลำดับ</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                        </select>
                                                        <?php
                                                         }
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <br>
                                <!--==================================end loop====================== -->
                            </div>
                        </div>
                    </div>

                    <!-----6.1------>
             
                    <div class="card">
                        <div class="card-header bg-info">
                            <h4 class="m-b-0 text-white">ฟอร์ม 6.1 : ภาระหนี้สิน การออม และปัญหาเศรษฐกิจของครัวเรือน
                            </h4>
                        </div>
                                              
                        <!---- card body --->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group  row">
                                        <label class="control-label text-left col-md-3">หนี้สินของครัวเรือน</label>
                                        <div class="col-md-6">
                                            <div class="form-check form-check-inline">
                                               
                                                <input type="radio" class="" <?php echo $house_information['hourse_debt'] =='Y'  ?  'checked' : '' ?>
                                                    name="hourse_debt" value="Y"  />
                                                <span style="" class="mr-3 "> มี</span>

                                                   
                                                <input type="radio" class="" <?php echo $house_information['hourse_debt'] =='N'  ?  'checked' : '' ?>
                                                    name="hourse_debt" value="N"  />
                                                <span style="" class=" "> ไม่มี</span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group  row">
                                        <label class="control-label text-left col-md-3">การออมของครัวเรือน</label>
                                        <div class="col-md-6">
                                            <div class="form-check form-check-inline">
                                                <input type="radio" class="" <?php echo $house_information['hourse_saveing'] =='Y'  ?  'checked' : '' ?>
                                                    name="hourse_saveing" value="Y"  />
                                                <span style="" class="mr-3 "> มี</span>

                                                
                                                <input type="radio" class="" <?php echo $house_information['hourse_saveing'] =='N'  ?  'checked' : '' ?>
                                                    name="hourse_saveing" value="N"  />
                                                <span style="" class=" "> ไม่มี</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group  row">
                                        <label class="control-label text-left col-md-12">
                                            <h4>ปัญหาทางเศรษฐกิจของครัวเรือน (สามารถระบุได้มากกว่า 1 ปัญหา)</h4>
                                        </label>

                                        <div class="col-md-8">
                                            <table class="table table-bordered table-hover">
                                                <tbody>
                                                    <?php foreach ($q_economic_problems as $keypinfo => $pinfo):
                                            $checked = '';
                                            foreach ($answer_list as $key => $answer) {
                                                if($pinfo->id == $answer->economic_problems_id) {
                                                    $checked = 'checked';
                                                    break;
                                                }
                                            }
                                        ?>
                                                    <tr>
                                                        <th width="50"><?php echo $keypinfo+1; ?></th>
                                                        <td>
                                                            <input type="checkbox" class="icheck-checkbox"
                                                                <?php echo $checked; ?>
                                                                name="economic_problems[answer][]"
                                                                value="<?php echo $pinfo->id; ?>">
                                                            <input type="hidden"
                                                                name="economic_problems[economic_problems_name][<?php echo $keypinfo+1; ?>]"
                                                                value="<?php echo $pinfo->name; ?>">
                                                            <span
                                                                style="font-size:16px;"><?php echo $pinfo->name; ?></span>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <a href="<?php echo site_url('fontend/form/form_5')?>"
                                                    class="btn btn-primary"><i
                                                        class="fas fa-step-backward"></i>ย้อนกลับ</a>
                                                <button type="submit" id="btn-save-form-5" class="btn btn-success"> <i
                                                        class=" fas fa-step-forward"></i> บันทึกต่อไป</button>
                                                <a href="<?php echo site_url('fontend/home')?>" class="btn btn-info"><i
                                                        class="  fas fa-home"></i>กลับหน้าแรก</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    </from>
            </div>
        </div>
    </div>
    <!-- Row -->



</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- script -->




<!-- End script -->