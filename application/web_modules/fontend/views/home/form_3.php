<?php 
$house_registration_id = $this->session->userdata('house_registration_id');
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">CRRU DATASET</h4>
            </div>
            <div class="col-md-7 align-self-center text-left">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">เลขที่แบบสอบถาม
                            <?php echo $this->session->userdata('community_id') ?> </li>
                        <li class="breadcrumb-item active">บันทึกครั้งที่
                            <?php echo $this->session->userdata('house_information_recorded') ?> </li>
                        <li class="breadcrumb-item active">ข้อมูลสมาชิกในครัวเรือน
                        </li>
                    </ol>

                </div>
            </div>
        </div>


        <!-- Row -->
        <div class="row">

            <div class="col-12">
                <form method="post" action="<?php echo site_url('index.php/fontend/save/form_3') ?>"
                    onsubmit="return save_check() " enctype="multipart/form-data">
                    <?php
                        $x = 0;
                            foreach ($member_data as $row) {
                                $x++;
                            //echo $row['house_member'] . "<br>";
                    ?>

                    <div class="card">
                        <div class="card-header bg-dark">
                            <h4 class="m-b-0 text-white">
                                ฟอร์ม 3 ข้อมูลสมาชิกในครัวเรือน
                            </h4>
                           
                        </div>
                        <div class="card-header bg-info">
                            <h4 class="m-b-0 text-white">
                                ข้อมูลสมาชิกคนที่  <?php echo $x." ". $row['f_name']." ".$row['l_name']; ?>
                            </h4>
                           
                        </div>
     
                        <div class="card-body">
                            <div class="form-body">
                                <h4 class="card-title"> ข้อมูลส่วนตัว</h4>
                                <hr class="m-t-0 m-b-40">
                                <input type="hidden" name="house_member_id[]"
                                    value="<?php echo $row['house_member_id']; ?>">
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">เพศ</label>
                                            <div class="col-md-9">
                                                <?php
                                                    $gender_name = array(
                                                    '' => 'เลือกเพศ',
                                                    '1' => 'ชาย',
                                                    '2' => 'หญิง',
                                                    );
                                                echo form_dropdown('gender[]', $gender_name, $row['gender'], ['class' => 'select2 form-control custom-select']);?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">คำนำหน้า</label>
                                            <div class="col-md-9">
                                                <?php echo form_dropdown('prefix_id[]', ['0' => 'เลือกคำนำหน้า'] + $p_name, $row['prefix_id'], ['class' => 'select2 form-control custom-select']); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">ชื่อ</label>
                                            <div class="col-md-9">
                                                <?php echo form_input('f_name[]', $row['f_name'], ['placeholder' => 'ชื่อ', 'class' => 'form-control']); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-3">สกุล</label>
                                            <div class="col-md-9">
                                                <?php echo form_input('l_name[]', $row['l_name'], ['placeholder' => 'สกุล', 'class' => 'form-control']); ?>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">วันเกิด</label>
                                            <div class="col-md-9">
                                                <input name="birthday[]" type="date" class="form-control"
                                                    placeholder="dd/mm/yyyy"  value="<?php if($row['birthday']!=""){echo date("Y-m-d", strtotime($row['birthday'])); }else{}?>"> 

                                            </div>
                                        </div>
                                    </div>
                                    <div class=" col-md-4">
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">อายุ</label>
                                            <div class="col-md-9">
                                                <?php echo form_input([ 'type' => 'number', 'name' => 'aeg[]',  'value' => $row['aeg']  , 'placeholder' => 'อายุ', 'class' => 'form-control' ] ); ?>                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">สูง(ซม.)</label>
                                            <div class="col-md-9">                                        
                                                <?php echo form_input([ 'type' => 'number', 'step'=>'0.01', 'name' => 'height[]',  'value' => $row['height']  , 'placeholder' => 'อาส่วนสูง เซนติเมตรยุ', 'class' => 'form-control' ] ); ?>          
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">น้ำหนัก(กก.)</label>
                                            <div class="col-md-9">                                              
                                                <?php echo form_input([ 'type' => 'number','step'=>'0.01', 'name' => 'weight[]',  'value' => $row['weight']  , 'placeholder' => 'น้ำหนัก กิโลกรัม', 'class' => 'form-control' ] ); ?> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">สัญชาติ</label>
                                            <div class="col-md-9">
                                                <?php echo form_dropdown('nationality_id[]', ['' => 'เลือกสัญชาติ'] + $nationality, $row['nationality_id'], ['class' => 'select2 form-control custom-select']); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label class="control-label text-left col-md-3">ศาสนา</label>
                                            <div class="col-md-9">
                                                <?php echo form_dropdown('religion_id[]', ['' => 'เลือกศาสนา'] + $religion, $row['religion_id'], ['class' => 'select2 form-control custom-select']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <!-- <h4 class="card-title"> สถานะในครัวเรือน</h4>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label
                                                class="control-label text-left col-md-4">ความเกี่ยวข้องกับหัวหน้าครัวเรือน</label>
                                            <div class="col-md-8">
                                                <?php echo form_dropdown('relatin_head_id[]', ['' => 'ความเกี่ยวข้องกับหัวหน้าครัวเรือน'] + $relatin_head, $row['relatin_head_id'], ['class' => 'select2 form-control custom-select']); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <label
                                                class="control-label text-left col-md-4">การอยู่อาศัยในครัวเรือน</label>
                                            <div class="col-md-8">
                                                <?php echo form_dropdown('living_status_id[]', ['' => 'สถานะการอยู่อาศัยในครัวเรือน'] + $living_status, $row['living_status_id'], ['class' => 'select2 form-control custom-select']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br><br> -->

                                <h4 class="card-title"> ความสามารถเฉพาะ ตอบได้มากกว่า 1 ข้อ</h4>
                                <hr>
                                <div class="row">
                                    <?php
                                       
                                    foreach ($skill as $row_skill) {
                                        $sql ="SELECT * FROM answer_member_skill 
                                        WHERE house_registration_id = '$house_registration_id' 
                                        AND member_id = '$row[house_member_id]'
                                        AND skill_id = '$row_skill[skill_id]' ";
                                        $num_row_array = $this->db->query($sql)->row();                                                                                    
                                        if ($num_row_array) {
                                            $checked = "checked='checked'";
                                        }else{
                                            $checked = "";
                                        }
                                    ?>
                                    <div class="col-md-3 col-sm-3">
                                        <div class="form-group  row">
                                            <div class="col-md-12">
                                                <?php echo form_checkbox('skill_id[' . $row['house_member_id'] . '][]', $row_skill['skill_id'], $checked, ['class' => '  ']) ?><label><?php echo $row_skill['skill_name'] ?></label>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    }
                                    ?>
                                </div>
                                <br>
                                <br>


                                <h4 class="card-title"> สถานภาพสมรส </h4>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">สถานภาพสมรส</label>
                                            <div class="col-md-8">
                                                <?php echo form_dropdown('marry_status_id[]', ['0' => 'เลือก'] + $marry, $row['marry_status_id'], ['class' => 'select2 form-control custom-select']); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br><br>


                                <h4 class="card-title"> การศึกษา</h4>
                                
                                <div class="row">
                                <!-- <div class="col-md-4">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">สถานะการศึกษา</label>
                                            <div class="col-md-8">
                                                <?php echo form_dropdown('edu_status_id[]', ['' => 'สถานะการศึกษา'] + $edu_status, $row['edu_status_id'], ['class' => 'select2 form-control custom-select']); ?>

                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-md-4">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">ระดับการศึกษาสูงสุด</label>
                                            <div class="col-md-8">
                                                <?php echo form_dropdown('edu_level_id[]', ['' => 'ระดับการศึกษา'] + $edu_level, $row['edu_level_id'], ['class' => 'select2 form-control custom-select']); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <h4 class="card-title"> ความสามารถในการอ่านออกเขียนได้</h4>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">การอ่าน</label>
                                            <div class="col-md-8">
                                                <?php echo form_dropdown('reading_level_id[]', ['' => 'ทักษะการอ่าน'] + $reading_level, $row['reading_level_id'], ['class' => 'select2 form-control custom-select']); ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">การเขียน</label>
                                            <div class="col-md-8">
                                                <?php echo form_dropdown('writing_level_id[]', ['' => 'ทักษะการเขียน'] + $writing_level, $row['writing_level_id'], ['class' => 'select2 form-control custom-select']); ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group  row">
                                            <label class="control-label text-left col-md-4">การคิด</label>
                                            <div class="col-md-8">
                                                <?php echo form_dropdown('cal_level_id[]', ['0' => 'ทักษะการคิด'] + $cal_level, $row['cal_level_id'], ['class' => 'select2 form-control custom-select']); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                          
                                <br><br>
                            
                                <h4 class="card-title"> ภาษาพูด ภาษาที่สอง ตอบได้มากกว่า 1 ข้อ</h4>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                    </div>
                                    <?php
                                    foreach ($second_lang as $row_second_lang) {
                                        $sql ="SELECT * FROM answer_member_second_lang 
                                        WHERE house_registration_id = '$house_registration_id' 
                                        AND member_id = '$row[house_member_id]'
                                        AND second_lang_id = '$row_second_lang[second_lang_id]' ";
                                     
                                        $num_row_array = $this->db->query($sql)->row();                                                                                    
                                        if ($num_row_array) {
                                            $checked = "checked='checked'";
                                        }else{
                                            $checked = "";
                                        }


                                    ?>
                                    <div class="col-md-3 col-sm-3">
                                        <div class="form-group  row">
                                            <div class="col-md-12">
                                                <?php echo form_checkbox('second_lang_id[' . $row['house_member_id'] . '][]', $row_second_lang['second_lang_id'], $checked, ['class' => '  ']) ?><label><?php echo $row_second_lang['second_lang_name'] ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>

                    </div>
                    <?php
                   
                    }
                    ?>
                    <div class="card">
                        <div class="card-body">

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                            
                                             <a href="<?php echo site_url('fontend/form/form_2')?>" class="btn btn-primary"><i class="fas fa-step-backward"></i>ย้อนกลับ</a>
                                            <button type="submit" id="btn-save-form-1" class="btn btn-success"> <i
                                            class=" fas fa-step-forward"></i>  บันทึกต่อไป</button>
                                             <a href="<?php echo site_url('fontend/home')?>" class="btn btn-info"><i class="  fas fa-home"></i>กลับหน้าแรก</a>
                                          
                                                
                                                
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    </from>

            </div>

        </div>
        <!-- Row -->



    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- script -->




<!-- End script -->