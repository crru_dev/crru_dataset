<?php 
$house_registration_id = $this->session->userdata('house_registration_id');
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">CRRU DATASET</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">เลขที่แบบสอบถาม
                            <?php echo $this->session->userdata('community_id') ?> </li>
                        <li class="breadcrumb-item active">บันทึกครั้งที่
                            <?php echo $this->session->userdata('house_information_recorded') ?> </li>
                        <li class="breadcrumb-item active">ข้อมูลการประกอบอาชีพของสมาชิกในครัวเรือน
                        </li>
                    </ol>
                </div>
            </div>
        </div>


        <!-- Row -->
        <div class="row">

            <div class="col-12">

                <form method="post" action="<?php echo site_url('index.php/fontend/save/form_4') ?>"  enctype="multipart/form-data">

                    <?php
                    $x = 0;
                    foreach ($member_data as $row) {
                    ?>

                        <div class="card">
                            <div class="card-header bg-dark">
                                <h4 class="m-b-0 text-white">
                                    ฟอร์ม 4 ข้อมูลการประกอบอาชีพของสมาชิกในครัวเรือน
                                </h4>
                            
                            </div>
                            <div class="card-header bg-info">
                                <h4 class="m-b-0 text-white"> สมาชิก คนที่  <?php echo $x+1 ." : ". $row['f_name'] . " " . $row['l_name']; ?>
                                </h4>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    <input type="hidden" name="house_member_id[<?php echo $x; ?>]" value="<?php echo $row['house_member_id']; ?>">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="card-title"> อาชีพ
                                            </h4>
                                            <hr>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group  row">
                                                <label class="control-label text-left col-md-4"><h5>อาชีพหลัก</h5></label>
                                                <div class="col-md-8">
                                                    <?php echo form_dropdown('occup_id[]', ['0' => 'อาชีพหลัก'] + $occup_data, $row['occup_id'], ['class' => 'select2 form-control custom-select']); ?>
                                                    <?php echo form_input('occup_detail[]', $row['occup_detail'], ['placeholder' => 'รายละเอียดอาชีพหลัก', 'class' => 'form-control']); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <hr>
                                    <label class="control-label text-left col-md-4"><h5>อาชีพเสริม ตอบได้มากกว่า 1 ข้อ</h5></label>
                                   
                                    <div class="row">

                                        <?php
                                      
                                        foreach ($occup_sub_data as $row_occup_sub):
                                                
                                            $occup_sub_id = $row_occup_sub['occup_sub_id']; 
                            
                                            $house_member_id  = $row['house_member_id'];
                                            $checked = "";
                                            
                                            $sql = "SELECT
                                                tb_occup_sub.occup_sub_id,
                                                tb_occup_sub.occup_sub_name
                                            FROM
                                                answer_occup_sub
                                            JOIN    tb_occup_sub
                                            ON  answer_occup_sub.occup_sub_id =  tb_occup_sub.occup_sub_id
                                            WHERE answer_occup_sub.house_registration_id = '$house_registration_id' AND house_member_id = ".$house_member_id." AND tb_occup_sub.occup_sub_id = '$occup_sub_id' ";
                                            
                                        $num_row_array = $this->db->query($sql)->row();
                                        
                                        if ($num_row_array) {
                                            $checked = "checked='checked'";
                                        }else{
                                            $checked = "";
                                        }

                                        ?>
                                            <div class="col-md-4">
                                                <div class="form-group  row">
                                                    <div class="col-md-12">

                                                        <?php echo form_checkbox('occup_sub_id['.$row['house_member_id'].'][]', $row_occup_sub['occup_sub_id'], $checked, ['class' => '  ']) ?><label><?php echo $row_occup_sub['occup_sub_name'] ?></label>

                                                    </div>

                                                </div>
                                            </div>

                                        <?php endforeach;?>

                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <hr>
                                    <label class="control-label text-left col-md-4"><h5>พื้นที่การทำงานของอาชีพหลัก
                                        ตอบได้มากกว่า 1 ข้อ</h5></label>
                                  
                                    <div class="row">

                                        <?php
                                       
                                        foreach ($area_occup_data as $row_area_occup) {

                                            $area_occup_id = $row_area_occup['id']; 
                                            $house_member_id  = $row['house_member_id'];
                                            $checked = "";

                                            $sql = "SELECT
                                                    tb_occup_area.id,
                                                    tb_occup_area.`name`,
                                                    answer_area_occup.area_occup_id
                                            FROM
                                                    tb_occup_area
                                            LEFT JOIN   answer_area_occup
                                            ON  tb_occup_area.id =   answer_area_occup.area_occup_id
                                            WHERE answer_area_occup.house_registration_id = '$house_registration_id' AND house_member_id = ".$house_member_id." AND area_occup_id = '$area_occup_id' ";
                                           
                                           $num_row_array = $this->db->query($sql)->row();
                                            
                                            if ($num_row_array) {
                                                $checked = "checked='checked'";
                                            }else{
                                                $checked = "";
                                            }
                                        ?>
                                            <div class="col-md-4">
                                                <div class="form-group  row">
                                                    <div class="col-md-12">

                                                        <?php echo form_checkbox('area_occup_id['.$row['house_member_id'].'][]', $row_area_occup['id'], $checked, ['class' => '  ']) ?><label><?php echo $row_area_occup['name'] ?></label>

                                                    </div>

                                                </div>
                                            </div>

                                        <?php
                                        }
                                        ?>

                                    </div>

                                

                                   


                                </div>
                            </div>

                        </div>
                    <?php
                        $x++;
                    }

                    ?>
                    <div class="card">
                        <div class="card-body">

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                            <a href="<?php echo site_url('fontend/form/form_3')?>" class="btn btn-primary"><i class="fas fa-step-backward"></i>ย้อนกลับ</a>
                                            <button type="submit" id="btn-save-form-1" class="btn btn-success"> <i
                                            class=" fas fa-step-forward"></i>  บันทึกต่อไป</button>
                                             <a href="<?php echo site_url('fontend/home')?>" class="btn btn-info"><i class="  fas fa-home"></i>กลับหน้าแรก</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    </from>

            </div>


        </div>
    </div>
    <!-- Row -->



</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- script -->




<!-- End script -->