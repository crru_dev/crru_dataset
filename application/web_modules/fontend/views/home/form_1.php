<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">CRRU DATASET</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">กรอกแบบสอบถาม
                        </li>
                        <li class="breadcrumb-item active">ข้อมูลบ้าน
                        </li>
                    </ol>
                </div>
            </div>
        </div>

        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">ฟอร์ม 1 ข้อมูลบ้าน</h4>
                    </div>
                    <div class="card-body">
                        <form method="post" action="<?php echo site_url('fontend/save/form_1') ?>" name="form-1" id="form-1"  enctype="multipart/form-data">
                            <div class="form-body">

                                <div class="row p-t-20">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">จังหวัด</label>
                                            <select class="select2 form-control custom-select" data-placeholder="Select"
                                                id="provinces" name="province_id" required onchange="get_amphure(this.value)">>
                                                <option value="">เลือกจังหวัด</option>

                                                <?php
                                                foreach ($provices as $provice) {
                                                    ?>
                                                <option value="<?php echo $provice['province_id']; ?>">
                                                    <?php echo $provice['province_name']; ?></option>
                                                <?php
                                                }
                                                ?>

                                            </select>


                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="amphure" id="sss" class="control-label">อำเภอ</label>

                                            <select name="amphur_id" id="amphures"
                                                class="select2 form-control custom-select"
                                                onchange="get_district(this.value)"> require>
                                                <option value="">เลือกอำเภอ</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="district" class="control-label">ตำบล</label>
                                            <select name="district_id" id="district"
                                                class="select2 form-control custom-select">
                                                <option value="">เลือกตำบล</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-1 ">
                                        <div class="form-group">
                                            <label>หมู่ที่</label>
                                            <?php

                                                $data = array(
                                                    'name' => 'moo',
                                                    'id' => 'moo',
                                                    'class' => 'form-control',
                                                    'type' => 'number', 
                                                    'placeholder' => 'หมู่ที่',
                                                    'class' => 'form-control',
                                                    'min' => '1',
                                                    'value' => '',
                                                );

                                                echo form_input($data);?>

                                        </div>
                                    </div>
                                    <div class="col-md-4 ">
                                        <div class="form-group">
                                            <label>ชื่อหมู่บ้าน</label>
                                            <?php echo form_input('village_name', '', ['placeholder' => 'ชื่อหมู่บ้าน', 'class' => 'form-control', 'required' => 'required']); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4 ">
                                        <div class=       form-group">
                                            <label>ชื่อชุมชน</label>
                                            <?php echo form_input('community_name', '', ['placeholder' => 'ชื่อชุมชน', 'class' => 'form-control']); ?>
                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-md-2 ">
                                        <div class="form-group">
                                            <label>บ้านเลขที่</label>
                                            <?php echo form_input('house_number', '', ['placeholder' => 'บ้านเลขที่', 'class' => 'form-control']); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-3 ">
                                        <div class="form-group">
                                            <label>รหัสประจำบ้าน</label>
                                            <?php

                                            $data = array(
                                                'name' => 'house_code',
                                                'id' => 'house_code',
                                                'maxlength' => '11',
                                                'class' => 'form-control',
                                                'type' => 'text',
                                                'placeholder' => 'รหัสประจำบ้าน',
                                                'class' => 'form-control',
                                                'value' => '',

                                            );

                                            echo form_input($data);?>
                                        </div>
                                    </div>
                                    <div class="col-md-3 ">
                                        <div class="form-group">
                                            <label>กรณีไม่มีบ้านเลขที่ ให้ใส่บ้านเลขที่ไกล้เคียง</label>
                                            <?php echo form_input('house_number_nearby', '', ['placeholder' => 'บ้านเลขที่ไกล้เคียง', 'class' => 'form-control']); ?>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-2 ">
                                        <div class="form-group">
                                            <label>จำนวนสมาชิกในครัวเรือน</label>
                                            <?php
                                                $data = array(
                                                    'name' => 'family_member',
                                                    'id' => 'family_member',
                                                    'class' => 'form-control',
                                                    'type' => 'number',
                                                    'placeholder' => 'จำนวนสมาชิก/คน',
                                                    'class' => 'form-control',
                                                    'value' => '1',
                                                    'min'=>"1",
                                                    'max'=>"15"
                                                );

                                                echo form_input($data);?>
                                  
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5 ">
                                        <div class="form-group">
                                            <label>รูปบ้าน ที่ 1 </label><small style="color:red">( ขนาดรูปควรไม่เกิน 2 MB )</small>
                                            <input id="file" type="file" name="pic" >
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5 ">
                                        <div class="form-group">
                                            <label>รูปบ้าน ที่ 2 </label><small style="color:red">( ขนาดรูปควรไม่เกิน 2 MB )</small>
                                            <input id="file2" type="file" name="pic2" >
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3" class="col-sm-2 control-label">สถานที่ตั้งชุมชน</label>
                                        <div class="col-sm-4">
                                            <div id="map_canvas"></div>

                                            <input name="zoom_value" type="hidden" id="zoom_value" value="0" size="5" />
                                            <?php echo form_input('Latitude', '', ['placeholder' => 'ละติจูด', 'class' => 'form-control', 'id' => 'Latitude']); ?>
                                            <?php echo form_input('Longitude', '', ['placeholder' => 'ลองจิจูด', 'class' => 'form-control', 'id' => 'Longitude']); ?>
                                            <p id="error_position"></p>
                                            <button type="button" class="btn btn-primary" onclick="getLocation();"><i
                                                    class="fa fa-location-arrow"></i> ใช้ตำแหน่งปัจจุบัน</button>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="form-actions">
                                    <button type="submit" id="btn-save-form-1" class="btn btn-success"> <i
                                            class=" fas fa-step-forward"></i>  บันทึกต่อไป</button>
                                    <a href="<?php echo site_url('fontend/home')?>" class="btn btn-info"><i class="  fas fa-home"></i>กลับหน้าแรก</a>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- script -->
<style>
    .bg-red{
        background-color: red;
        color:white;
    }

    .bg-greeen{
        background-color: #7ac606;
        color:white;
    }
</style>

<script src="http://eliteadmin.themedesigner.in/demos/bt4/eliteadmin/dist/js/pages/validation.js"></script>

<script type="text/javascript">

$("#btn-save-form-1").click(function(e) { // เมื่อกดปุ่มบันทึกข้อมูล
    
});

// $(document).ready(function() {
   


//     $("#house_code").blur(function(){
//         var house_code = $(this).val();
//         if(house_code){
//             checkHouse_code(house_code);
//         }
        
//     })
// });//END Ready


// function checkHouse_code(house_code){
//     $.get('<?php echo site_url("fontend/form/checkHouse_code")?>', { house_code : house_code }, function(responese){
//         if(responese.status == false){

//             swal("แจ้งเตือน", responese.message, "error");
//             $("#house_code").addClass( "bg-red" );
//             $("#house_code").removeClass( "bg-greeen" );
//             $("#btn-save-form-1").prop("disabled", true);
//         }else{

//             swal("แจ้งเตือน", responese.message, "success");
//             $("#house_code").addClass( "bg-greeen" );
//             $("#house_code").removeClass( "bg-red" );
//             $("#btn-save-form-1").prop("disabled", false);
//         }
//     }, 'json');
// }


var x = document.getElementById("error_position");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {

    document.getElementById("Latitude").value = position.coords.latitude;
    document.getElementById("Longitude").value = position.coords.longitude;
    GGM = new Object(google
        .maps); // เก็บตัวแปร google.maps Object ไว้ในตัวแปร GGM
    // กำหนดจุดเริ่มต้นของแผนที่
    var my_Latlng = new GGM.LatLng(position.coords.latitude, position.coords
        .longitude);
    var my_mapTypeId = GGM.MapTypeId.ROADMAP; // กำหนดรูปแบบแผนที่ที่แสดง
    // กำหนด DOM object ที่จะเอาแผนที่ไปแสดง ที่นี้คือ div id=map_canvas
    var my_DivObj = $("#map_canvas")[0];
    // กำหนด Option ของแผนที่
    var myOptions = {
        zoom: 15, // กำหนดขนาดการ zoom
        center: my_Latlng, // กำหนดจุดกึ่งกลาง
        mapTypeId: my_mapTypeId // กำหนดรูปแบบแผนที่
    };
    map = new GGM.Map(my_DivObj,
        myOptions); // สร้างแผนที่และเก็บตัวแปรไว้ในชื่อ map

    var my_Marker = new GGM.Marker({ // สร้างตัว marker
        position: my_Latlng, // กำหนดไว้ที่เดียวกับจุดกึ่งกลาง
        map: map, // กำหนดว่า marker นี้ใช้กับแผนที่ชื่อ instance ว่า map
        draggable: true, // กำหนดให้สามารถลากตัว marker นี้ได้
        title: "คลิกลากเพื่อหาตำแหน่งจุดที่ต้องการ!" // แสดง title เมื่อเอาเมาส์มาอยู่เหนือ
    });

}

function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred."
            break;
    }
}

function get_amphure(id_province) {

    $.ajax({
        type: "POST",
        url: "<?php echo site_url('index.php/fontend/form/get_province') ?>",
        data: {
            id: id_province,
            function: 'provinces'
        },
        success: function(data) {
            $('#amphures').html(data);
            $('#districts').html('');
        }
    });
}

function get_district(id_amphures) {
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('index.php/fontend/form/get_province') ?>",
        data: {
            id: id_amphures,
            function: 'amphures'
        },
        success: function(data) {
            $('#district').html(data);
        }
    });
}

function save_check() { // เมื่อกดปุ่มบันทึกข้อมูล
    var province_id = $("#province").val();
    var district_id = $("#district").val();
    var amphur_id = $("#amphur").val();
    var Latitude = $("#Latitude").val();
    var Longitude = $("#Longitude").val();
    var family_member = $("#family_member").val();
    var moo = $("#moo").val();


    // var tel = $("#tel").val();

    if (province_id == "" || district_id == "" || amphur_id == "" || Latitude == "" || Longitude == "" ||
        family_member == "0" || family_member == "" || moo == "") {
        swal("แจ้งเตือน", "กรอกข้อมูลให้ครบ", "error");
        return false;
    }
    // var house_code = $("#house_code").val();
    // var id = "0";
    // $.ajax({
    //     type: "POST",
    //     url: "<?php echo site_url('index.php/fontend/save/check_house_code') ?>",
    //     data: {
    //         house_code: house_code
    //     },
    //     success: function(data) {
    //         if (data != "0") {
    //             // alert(data);
    //             swal("แจ้งเตือน", "รหัสบ้านนี้มีแล้ว", "error");
    //             return false;
    //             //id = "1";
    //         }
    //     }

    // });
    //if (id == "1") {
    //  swal("แจ้งเตือน", "รหัสบ้านนี้มีแล้ว", "error");
    //   return false;
    //  }

}
	
</script>

<!-- End script -->